<?php

class JournalsController extends \BaseController {

	/**
	 * Display a listing of journals
	 *
	 * @return Response
	 */
	public function index()
	{
		$journals = Journal::all();

		return View::make('journals.index', compact('journals'));
	}

	/**
	 * Show the form for creating a new journal
	 *
	 * @return Response
	 */
	public function create()
	{
		$particulars = Particular::all();
		foreach ($particulars as $key => $particular) {
				if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32') {
						unset($particulars[$key]);
				}
		}
/*$accounts = Account::all();
return View::make('journals.create', compact('accounts','particulars'));*/

		$members = Member::all();
		return View::make('journals.create', compact('particulars', 'members'));
	}

	/**
	 * Store a newly created journal in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Journal::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}



       $particular = Particular::findOrFail(Input::get('particular'));

       $data = array(
            'date' => Input::get('date'),
            'debit_account' => $particular->debitaccount_id,
            'credit_account' => $particular->creditaccount_id,
            'description' => Input::get('description'),
            'amount' => Input::get('amount'),
            'initiated_by' => Input::get('user'),
            'particulars_id' => Input::get('particular'),
            'bank_details' => Input::get('bank_reference'),

            'narration' => Input::get('narration')
        );
			//return	$this->savingtransactions();

			//	return $data1;
        $journal = new Journal;

        $journal->journal_entry($data);

       /** if (Input::has('expense')) {
            return Redirect::to('budget/expenses');
        } elseif (Input::has('income')) {
            return Redirect::to('budget/incomes');
        } else {
            return Redirect::route('journals.index');
        }**/
          return Redirect::to('accounts');

	}

	/**
	 * Display the specified journal.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$journal = Journal::findOrFail($id);

		return View::make('journals.show', compact('journal'));
	}

	/**
	 * Show the form for editing the specified journal.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$journal = Journal::find($id);

		return View::make('journals.edit', compact('journal'));
	}

	/**
	 * Update the specified journal in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$journal = Journal::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Journal::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$branch = Branch::findOrFail(Input::get('branch_id'));
		$account = Account::findOrFail(Input::get('account_id'));


		$journal->branch()->associate($branch);
		$journal->account()->associate($account);

		$journal->date = Input::get('date');
		$journal->trans_no = Input::get('trans_no');
		$journal->initiated_by = Input::get('initiated_by');
		$journal->amount = Input::get('amount');
		$journal->type = Input::get('type');
		$journal->description = Input::get('description');
		$journal->update();

		return Redirect::route('journals.index');
	}

	/**
	 * Remove the specified journal from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$journal = Journal::findOrFail($id);

		$journal->void = TRUE;
		$journal->update();

		return Redirect::route('journals.index');
	}
public function savingtransactions(){


	$savingtransactions=Savingtransaction::all();
return	$savingaccount_id=Savingtransaction::where('savingaccount_id','>','0')->pluck('savingaccount_id');
	$credit_account=Savingposting::where('credit_account','=',$savingaccount_id)->get();
	$debit_account=Savingposting::where('debit_account','=',$savingaccount_id)->get();
foreach ($savingtransactions as $savingtransaction) {

	$data1 = array(
			 'date' => Input::get('date'),
			 // 'debit_account' => 9,
			 // 'credit_account' => 5,
		 	 'debit_account' => $debit_account,
			 'credit_account' => $credit_account,
			 'description' => $savingtransaction->type,
			 'amount' =>$savingtransaction->amount,
			 'initiated_by' => Input::get('user'),
			 'particulars_id' =>$savingtransaction->id,
			 'narration' => Input::get('narration')
				);
}
    return $data1;
		}

}
