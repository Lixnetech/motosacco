<?php

class AccountsController extends \BaseController {

	/**
	 * Display a listing of accounts
	 *
	 * @return Response
	 */
	public function index()
	{
		$accounts = DB::table('accounts')->orderBy('code', 'asc')->get();

		return View::make('accounts.index', compact('accounts'));
	}

	/**
	 * Show the form for creating a new account
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('accounts.create');
	}

	/**
	 * Store a newly created account in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Account::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}


		// check if code exists
		$code = Input::get('code');
		$code_exists = DB::table('accounts')->where('code', '=', $code)->count();

		if($code_exists >= 1){

			return Redirect::back()->withErrors(array('error'=>'The GL code already exists'))->withInput();
		}
		else {


		$account = new Account;


		$account->category = Input::get('category');
		$account->name = Input::get('name');
		$account->code = Input::get('code');
		if(Input::get('active')){
			$account->active = TRUE;
		}
		else {
			$account->active = FALSE;
		}
		$account->save();

		}



		return Redirect::route('accounts.index');
	}

	/**
	 * Display the specified account.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$account = Account::findOrFail($id);

		return View::make('accounts.show', compact('account'));
	}

	/**
	 * Show the form for editing the specified account.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$account = Account::find($id);

		return View::make('accounts.edit', compact('account'));
	}

	/**
	 * Update the specified account in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$account = Account::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Account::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$code = Input::get('code');
		$original_code = DB::table('accounts')->where('id', '=', $account->id)->pluck('code');

		if($code != $original_code) {

			$code_exists = DB::table('accounts')->where('code', '=', $code)->count();

		if($code_exists >= 1){

			return Redirect::back()->withErrors(array('error'=>'The GL code already exists'))->withInput();
		}


		else {




		$account->category = Input::get('category');
		$account->name = Input::get('name');
		$account->code = Input::get('code');
		if(Input::get('active')){
			$account->active = TRUE;
		}
		else {
			$account->active = FALSE;
		}

		$account->update();

		}

		} else {

		$account->category = Input::get('category');
		$account->name = Input::get('name');
		$account->code = Input::get('code');
		$account->active = Input::get('active');
		$account->update();

		}



		return Redirect::route('accounts.index');
	}

	/**
	 * Remove the specified account from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Account::destroy($id);

		return Redirect::route('accounts.index');
	}

    public function proposalInterests()
    {
        $name = 'Interest';
        $types = DB::table('proposal_categories')->where('type', 'INTEREST')->get();
        return View::make('accounts.budget', compact('types', 'name'));
    }

    public function proposalOtherIncome()
    {
        $name = 'Other Income';
        $types = DB::table('proposal_categories')->where('type', 'OTHER INCOME')->get();
        return View::make('accounts.budget', compact('types', 'name'));
    }

    public function proposalExpenditure()
    {
        $name = 'Expenditure';
        $types = DB::table('proposal_categories')->where('type', 'Expenditure')->get();
        return View::make('accounts.budget', compact('types', 'name'));
    }

    public function createProposal()
    {
        return View::make('accounts.create_proposal_category');
    }

    public function storeProposal()
    {
        $validator = Validator::make($data = Input::all(), array(
            'type' => 'required|in:INTEREST,OTHER INCOME,EXPENDITURE',
            'name' => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::table('proposal_categories')->insert(array(
            'type' => Input::get('type'),
            'name' => Input::get('name')
        ));

        switch (Input::get('type')) {
            default:
            case 'INTEREST':
                return Redirect::to('budget/interests');

            case 'OTHER INCOME':
                return Redirect::to('budget/income');

            case 'EXPENDITURE':
                return Redirect::to('budget/expenditure');

        }
    }

    public function projections()
    {
        $set_year = Input::get('year');
        if ($set_year == null || empty($set_year))
            $set_year = date("Y");

        $year = (int)date("Y");
        $years = range($year - 100, $year + 100);

        $projections = array(
            'Interest' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
                ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
                ->where('proposal_entries.year', '=', $set_year)
                ->where('proposal_categories.type', '=', 'INTEREST')
                ->get(),
            'Income' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
                ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
                ->where('proposal_entries.year', '=', $set_year)
                ->where('proposal_categories.type', '=', 'OTHER INCOME')
                ->get(),
            'Expenditure' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
                ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
                ->where('proposal_entries.year', '=', $set_year)
                ->where('proposal_categories.type', '=', 'EXPENDITURE')
                ->get()
        );
        return View::make('accounts.projections', compact('set_year', 'years', 'projections'));
    }

    public function createProjection()
    {
        $year = (int)date("Y");
        $years = range($year - 100, $year + 100);
        $projections = array(
            'Interest' => DB::table('proposal_categories')->where('type', '=', 'INTEREST')->get(),
            'Income' => DB::table('proposal_categories')->where('type', '=', 'OTHER INCOME')->get(),
            'Expenditure' => DB::table('proposal_categories')->where('type', '=', 'EXPENDITURE')->get()
        );

        return View::make('accounts.create_projection', compact('year', 'years', 'projections'));
    }

    public function storeProjection()
    {
        $rules = array(
            'year' => 'required|integer'
        );
        $projections = array(
            'Interest' => DB::table('proposal_categories')->where('type', '=', 'INTEREST')->get(),
            'Income' => DB::table('proposal_categories')->where('type', '=', 'OTHER INCOME')->get(),
            'Expenditure' => DB::table('proposal_categories')->where('type', '=', 'EXPENDITURE')->get()
        );
        foreach ($projections as $title => $projection) {
            foreach ($projection as $category) {
                foreach (range(1, 4) as $value) {
                    $rules[$title . '.' . $category->name . '.' . $value] = 'required|integer';
                }
            }
        }

        $validator = Validator::make($date = Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        foreach ($projections as $title => $projection) {
            foreach ($projection as $category) {
                DB::table('proposal_entries')->insert(array(
                    'proposal_category_id' => $category->id,
                    'year' => Input::get('year'),
                    'first_quarter' => Input::get($title)[$category->name][1],
                    'second_quarter' => Input::get($title)[$category->name][2],
                    'third_quarter' => Input::get($title)[$category->name][3],
                    'fourth_quarter' => Input::get($title)[$category->name][4],
                ));
            }
        }

        return Redirect::to('budget/projections');
    }

    public function showExpenses()
    {   
        $expenseAccounts = Account::select('id')->where('category', 'EXPENSE')->get()->toArray();
        $expenses = Journal::whereIn('account_id', $expenseAccounts)->orderBy('date','DESC')->get();
        return View::make('accounts.expenses', compact('expenses'));
    }

    public function createExpenses()
    {
        $expenseAccounts = Account::select('id')->where('category', 'EXPENSE')->get()->toArray();
        $particulars = Particular::whereIn('debitaccount_id',$expenseAccounts)->get();
        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32') {
                unset($particulars[$key]);
            }
        }
        $members = Member::all();
        return View::make('accounts.create_expense', compact('particulars', 'members'));
    }

    public function storeExpenses()
    {
        $types = DB::table('proposal_categories')->select('name')->where('type', 'Expenditure')->get();
        $types_string = implode(",", array_map(function ($element) {
            return $element->name;
        }, $types));

        $rules = array(
            'type' => 'required|in:' . $types_string,
            'amount' => 'required|numeric',
            'description' => 'required',
            'date' => 'required|date'
        );

        $validator = Validator::make($data = Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DB::table('expenses')->insert(array(
            'type' => Input::get('type'),
            'amount' => Input::get('amount'),
            'description' => Input::get('description'),
            'date' => date('Y-m-d', strtotime(Input::get('date')))
        ));

        return Redirect::to('budget/expenses');
    }
    public function showIncomes(){
     //$from=date('Y-m')."-01";

 $incomeAccounts  = Account::select('id')->where('category', 'INCOME')->get()->toArray();
        $incomes = Journal::whereIn('account_id',$incomeAccounts)->whereNotNull('particulars_id')->where('particulars_id','>',0)->orderBy('date','DESC')->get();
         return View::make('accounts.income', compact('incomes'));
    /**
        $from=date('Y-m')."-01";
        $to=date('Y-m-t');

        $incomeAccounts = Account::select('id')->where('category', 'INCOME')->get()->toArray();
               
        $incomes = Journal::whereIn('account_id', $incomeAccounts)->whereNotNull('particulars_id')->whereBetween('date',array($from,$to))->orderBy('date','DESC')->get();
        $incomeSums = array();
       
        
        foreach ($incomes as $income){
           if(isset($income->particular->name)){

            $particular = $income->particular->name;
            if(key_exists($particular, $incomeSums)) {
                $incomeSums[$particular]['amount'] += $income->amount;
            }else{
                $incomeSums[$particular]['amount'] = $income->amount;
                $incomeSums[$particular]['income'] = $income;
            }
        }
   }

        return View::make('accounts.income', compact('incomeSums'));
     **/
    }
    public function createIncomes(){
       $incomeAccounts = Account::select('id')->where('category', 'INCOME')->get()->toArray();
        $particulars = Particular::whereIn('creditaccount_id',$incomeAccounts)->get();

        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32' ) {
                unset($particulars[$key]);
            }
        }
        $members = Member::all();
        return View::make('accounts.create_income', compact('particulars', 'members'));
    }

		public function savereceipt(){
	$data = Input::all();
	//credit cash account and debit bank account
	if($data['type'] == 'deposit'){
		$credit_account = Account::where('name', 'like', '%'.'Cash Account'.'%')->pluck('id');
		$debit_account = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
		$particulars = Particular::where('name', 'like', '%'.'bank deposits'.'%')->first();
		if(empty($particulars)){
			$particulars = new Particular;
			$particulars->name='Bank Deposits';
			$particulars->creditaccount_id =$credit_account;
			$particulars->debitaccount_id =$debit_account;
			$particulars->save();

		}

	}//else debit cash/expense account and credit bank account
	elseif ($data['type'] == 'withdrawal') {
		$debit_account = Account::where('name', 'like', '%'.'Cash Account'.'%')->pluck('id');
		$credit_account = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
		$particulars = Particular::where('name', 'like', '%'.'bank withdrawals'.'%')->first();

		if(empty($particulars)){
			$particulars = new Particular;
			$particulars->name = 'Bank withdrawals';
			$particulars->creditaccount_id = $credit_account;
			$particulars->debitaccount_id = $debit_account;
			$particulars->save();
		}
	}

	//return $particulars;

	$data = array(
		'date' => $data['date'],
		'description' => $data['description'],
		'amount' => $data['amount'],
		'debit_account' => $debit_account,
		'credit_account' => $credit_account,
		'initiated_by' => Confide::user()->username,
		'particulars_id' => $particulars->id,
                 'type' => 'deposit',
                 'bank_account' => '',

                 'payment_form' => $data['payment_form'],
		'batch_transaction_no' => $data['receiptno'],
                 'bank_details' => $data['receiptno'],

		'narration' => 0
	);

	$journal = new Journal;
	$journal->journal_entry($data);
         $accounttransaction= new AccountTransaction;
	 $accounttransaction->createTransaction($data);

	// AccountTransaction::createTransaction($data);
	//return Redirect::back()->withFlashMessage('Receipt captured successfully.');
        return Redirect::back()->with('success','Bank deposit transaction captured successfully.');

}
public function addBankTransaction(){
    $data = Input::all();
    //credit cash account and debit bank account
    if($data['type'] == 'payment'){
        $credit_account = Account::where('name', 'like', '%'.'Cash Account'.'%')->pluck('id');
        $debit_account = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
        $particulars = Particular::where('name', 'like', '%'.'bank deposits'.'%')->first();
        $type='deposit';
        if(empty($particulars)){
            $particulars = new Particular;
            $particulars->name='Bank Deposits';
            $particulars->creditaccount_id =$credit_account;
            $particulars->debitaccount_id =$debit_account;
            $particulars->save();

        }

    }//else debit cash/expense account and credit bank account
    elseif ($data['type'] == 'disbursal') {
        $debit_account = Account::where('name', 'like', '%'.'Cash Account'.'%')->pluck('id');
        $credit_account = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
        $particulars = Particular::where('name', 'like', '%'.'bank withdrawals'.'%')->first();
        $type="withdraw";
        if(empty($particulars)){
            $particulars = new Particular;
            $particulars->name = 'Bank withdrawals';
            $particulars->creditaccount_id = $credit_account;
            $particulars->debitaccount_id = $debit_account;
            $particulars->save();
        }
    }

    //return $particulars;

    $data = array(
        'date' => $data['date'],
        'description' => $data['description'],
        'amount' => $data['amount'],
        'debit_account' => $debit_account,
        'credit_account' => $credit_account,
        'initiated_by' => Confide::user()->username,
        'particulars_id' => $particulars->id,
        'batch_transaction_no' => $data['bankrefno'],
        'bank_account' => $data['bankAcc'],
        'payment_form' => $data['payment_form'],
        'type' => $type, 
        'narration' => 0
    );  
   
   //$journal = new Journal;
    //$journal->journal_entry($data);
      $accounttransaction= new AccountTransaction;
      $accounttransaction->createTransaction($data);
      
    // AccountTransaction::createTransaction($data);
    return Redirect::back()->with('success','Transaction captured successfully.');
}
public function importexpenseView()
    {
        return View::make('accounts.import');
    }
public function createTemplate()
    {
        return Excel::create('expenses', function ($excel) {

            require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


            $excel->sheet('expenses', function ($sheet) {


                $sheet->row(1, array(
                   'DATE', 'PARTICULAR', 'DESCRIPTION','AMOUNT',

                ));

                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 60,
                    'C' => 30,
                    'D' => 30,
                ));

                /*$sheet->getStyle('A2:A100')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');*/

               /** $sheet->setColumnFormat(array(
                    "A" => "yyyy-mm-dd",
                ));**/

                $row = 2;
                $particulars = Particular::where('creditaccount_id', '6')->get();
                if (ob_get_level() > 0) {
                    ob_end_clean();
                }

                for ($i = 0; $i < count($particulars); $i++) {
                   if(!empty($particulars[$i]))
                     {
             
                   $sheet->SetCellValue("Y" . $row, $particulars[$i]->name . ": " . $particulars[$i]->id);
                    $row++;
                    }
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'particulars', $sheet, 'Y2:Y' . (count($particulars) + 1)
                    )
                );


                for ($i = 2; $i <= 100; $i++) {

                    $objValidation = $sheet->getCell('B' . $i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('particulars'); //note this!
                }

            });

        })->export('xlsx');
    }
     public function importExpense()
    {
        if (Input::hasFile('expenses')) {
            $destination = public_path() . '/migrations/';
            $filename = str_random(12);
            $ext = Input::file('expenses')->getClientOriginalExtension();
             /* use csv format for correct date format since excel resuilts to errorneous date format for THIS VERSION
          if ($ext === 'csv') */
           if ($ext === 'xls' || $ext === 'xlsx')
          
              {
                $file = $filename . '.' . $ext;
                Input::file('expenses')->move($destination, $file);
                Excel::selectSheetsByIndex(0)->load(public_path() . '/migrations/' . $file, function ($reader) {
                    $results = $reader->get();
                    foreach ($results as $result) {
                        if ($result->particular != null && !empty($result->particular)
                            && $result->date != null && !empty($result->date)
                            && $result->amount != null && !empty($result->amount)
                            ) {

                            $particular = Particular::where('id', trim(explode(':', $result->particular)[1]))->first();
                            /*Record Transaction*/
                                                                                    $data = array(
                                'credit_account' =>$particular->creditaccount_id  ,
                                'debit_account' =>$particular->debitaccount_id ,
                                'date' => date('Y-m-d', strtotime($result->date)),
                                'amount' => $result->amount,
                                'initiated_by' => 'system',
                                'description' => $result->description,
                                'particulars_id' => $particular->id,
                                'narration' => 0
                            );
                            $journal = new Journal;
                            $journal->journal_entry($data);

                                                    }
                    }
                });
                return Redirect::back()->with('notice', 'Expenses have been successfully imported');
            }
            return Redirect::back()->with('warning', 'File Not Accepted. Kindly upload Excel Files only');
        }
    }
public function importincomeView()
    {
        return View::make('accounts.importincome');
    }
public function createIncomeTemplate()
    {
        return Excel::create('Income', function ($excel) {

            require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


            $excel->sheet('Income', function ($sheet) {


                $sheet->row(1, array(
                   'DATE', 'PARTICULAR', 'DESCRIPTION','AMOUNT',

                ));

                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 60,
                    'C' => 30,
                    'D' => 30,
                ));

                /*$sheet->getStyle('A2:A100')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');*/

               /** $sheet->setColumnFormat(array(
                    "A" => "yyyy-mm-dd",
                ));**/

                $row = 2;
                $particulars = Particular::where('debitaccount_id', '6')->get();
                if (ob_get_level() > 0) {
                    ob_end_clean();
                }

                for ($i = 0; $i < count($particulars); $i++) {
                   if(!empty($particulars[$i]))
                     {
             
                   $sheet->SetCellValue("Y" . $row, $particulars[$i]->name . ": " . $particulars[$i]->id);
                    $row++;
                    }
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'particulars', $sheet, 'Y2:Y' . (count($particulars) + 1)
                    )
                );


                for ($i = 2; $i <= 100; $i++) {

                    $objValidation = $sheet->getCell('B' . $i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('particulars'); //note this!
                }

            });

        })->export('xlsx');
    }

public function importincome()
    {
        if (Input::hasFile('income')) {
            $destination = public_path() . '/migrations/';
            $filename = str_random(12);
            $ext = Input::file('income')->getClientOriginalExtension();
             /* use csv format for correct date format since excel resuilts to errorneous date format for THIS VERSION
          if ($ext === 'csv') */
           if ($ext === 'xls' || $ext === 'xlsx')
          
              {
                $file = $filename . '.' . $ext;
                Input::file('income')->move($destination, $file);
                Excel::selectSheetsByIndex(0)->load(public_path() . '/migrations/' . $file, function ($reader) {
                    $results = $reader->get();
                    foreach ($results as $result) {
                        if ($result->particular != null && !empty($result->particular)
                            && $result->date != null && !empty($result->date)
                            && $result->amount != null && !empty($result->amount)
                            ) {

                            $particular = Particular::where('id', trim(explode(':', $result->particular)[1]))->first();
                            /*Record Transaction*/
                                                                                    $data = array(
                                'credit_account' =>$particular->creditaccount_id  ,
                                'debit_account' =>$particular->debitaccount_id ,
                                'date' => date('Y-m-d', strtotime($result->date)),
                                'amount' => $result->amount,
                                'initiated_by' => 'system',
                                'description' => $result->description,
                                'particulars_id' => $particular->id,
                                'narration' => 0
                            );
                            $journal = new Journal;
                            $journal->journal_entry($data);

                                                    }
                    }
                });
                return Redirect::back()->with('notice', 'Incomes have been successfully imported');
            }
            return Redirect::back()->with('warning', 'File Not Accepted. Kindly upload Excel Files only');
        }
    }



}
