<?php

class SavingaccountsController extends \BaseController {

	/**
	 * Display a listing of savingaccounts
	 *
	 * @return Response
	 */
	public function index()
	{
		$savingaccounts = Savingaccount::all();

		return View::make('savingaccounts.index', compact('savingaccounts'));
	}

	/**
	 * Show the form for creating a new savingaccount
	 *
	 * @return Response
	 */
	public function create($id)
	{

		$member = Member::findOrFail($id);
		$savingproducts = Savingproduct::all(); 
		return View::make('savingaccounts.create', compact('member', 'savingproducts'));
	}

	/**
	 * Store a newly created savingaccount in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Savingaccount::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}


		$member = Member::findOrFail(Input::get('member_id'));
		$savingproduct = Savingproduct::findOrFail(Input::get('savingproduct_id'));

		$acc_no = $savingproduct->shortname.'000000'.$member->membership_no;

		$savingaccount = new Savingaccount;
		$savingaccount->member()->associate($member);
		$savingaccount->savingproduct()->associate($savingproduct);
		$savingaccount->account_number = $acc_no;
		$savingaccount->save();

		return Redirect::route('savingaccounts.index');
	}

	/**
	 * Display the specified savingaccount.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$savingaccount = Savingaccount::findOrFail($id);

		return View::make('savingaccounts.show', compact('savingaccount'));
	}

	/**
	 * Show the form for editing the specified savingaccount.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$savingaccount = Savingaccount::find($id);

		return View::make('savingaccounts.edit', compact('savingaccount'));
	}

	/**
	 * Update the specified savingaccount in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$savingaccount = Savingaccount::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Savingaccount::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$savingaccount->update($data);

		return Redirect::route('savingaccounts.index');
	}

	/**
	 * Remove the specified savingaccount from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Savingaccount::destroy($id);

		return Redirect::route('savingaccounts.index');
	}



	public function memberaccounts($id){


		$member = Member::findOrFail($id);



		return View::make('savingaccounts.memberaccounts', compact('member'));
	}

	public function refund(){
		$amount = Input::get("refundAmount");  $form=Input::get("form"); 
		$refundable=Input::get("refundable");  $member_id=Input::get("member_id");
		$member=Member::findorfail($member_id); 

		if($amount>$refundable){
			return Redirect::back()->with('error','Cannot refund more than refundable amount.');		
		}
 
		if($refundable<1){
			return Redirect::back()->with('error','No amount to refund');		
		} 
		Savingaccount::withdrawSavings($member->id,$amount,$form);
		$savingsBal=Savingaccount::getUserSavingsBalance($member->id);
		Savingaccount::withdrawSavings($member->id,$savingsBal);
		$member_loans=Loanaccount::where("member_id",$member->id)->get();
		foreach($member_loans as $loan){
			Loanrepayment::clearLoan($loan);
		} 
		if($form=="Cash" || $form=="Mpesa"){
			$moneyAcc=1;
		}else{$moneyAcc=6;} 
		$data = array(
			'credit_account' =>$moneyAcc,
			'debit_account' =>'118', 
			'date' => date('Y-m-d'),
			'amount' => $amount,
			'initiated_by' => 'system',
			'description' => 'loanRefund',
			'particulars_id' => '0',
			'narration' => $member_id
			);
		
		$journal = new Journal;
		$journal->journal_entry($data);

		return Redirect::back()->with('refundStatus','Amount refunded');
		//return redirect('portal/refund/$member->id')->with('refundStatus','Amount refunded');
		//return View::make('savingaccounts.memberaccounts', compact('member'));
	}

}
