<?php

class ReportsController extends \BaseController
{



  public function members()
  {
    return View::make('members.members_select');
  }

  public function membersListing()
  {
    $validator = Validator::make(Input::all(), array(
      'type' => 'required|in:all,active,inactive'
    ));

    if ($validator->fails()) {
      return Redirect()->back()->withErrors($validator)->withInput();
    }

    $type = Input::get('type');
    switch ($type) {
      case 'all':
        $members = Member::all();
        break;

      case 'active':
        $members = Member::where('is_active', 1)->get();
        break;

      case 'inactive':
        $members = Member::where('is_active', 0)->get();
        break;

      default:
        $members = Member::all();
        break;
    }

    $organization = Organization::find(1);

    //return $members;
    if (Input::get('format') == 'pdf') {
      $pdf = PDF::loadView('pdf.memberlist', compact('members', 'organization'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('MemberList.pdf');
    } elseif (Input::get('format') == 'excel') {

      return Excel::create('Member List', function ($excel) use ($members) {

        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


        $excel->sheet('Member List ', function ($sheet) use ($members) {

          $sheet->setAllBorders('thin');

          $sheet->setWidth(array(
            'A' => 15,
            'B' => 40,
            'C' => 15,
            'D' => 20,
          ));

          $sheet->mergeCells('A1:D1');

          $sheet->row(1, array("MOTOSACCO SAVINGS REPORTS"));

          $sheet->cells('A1:D1', function ($cells) {
            $cells->setAlignment('center');
            // $cells->setBackground('#777777');
            $cells->setFont(array(
              'family' => 'Calibri',
              'bold' => true
            ));
          });

          $sheet->mergeCells('A2:D2');


          $sheet->row(3, array(
            "Member Number", "Member Name", "Member Group", "Member Branch"
          ));

          $sheet->cells('A3:D3', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 4;

          foreach ($members as $member) {
            $sheet->cell('A' . $row, function ($cell) use ($member) {
              $cell->setValue($member->membership_no);
            });
            $sheet->cell('B' . $row, function ($cell) use ($member) {
              $cell->setValue($member->name);
            });
            $sheet->cell('C' . $row, function ($cell) use ($member) {
              if ($member->group != null)
                $cell->setValue($member->group->name);
              else
                $cell->setValue("");
            });
            $sheet->cell('D' . $row, function ($cell) use ($member) {
              if ($member->branch != null)
                $cell->setValue($member->branch->name);
              else
                $cell->setValue("");
            });

            $row++;
          }
        });
      })->export('xlsx');
    }
  }



  public function remittance()
  {

    //$members = DB::table('members')->where('is_active', '=', '1')->get();

    $members = Member::all();
    $organization = Organization::find(1);

    $savingproducts = Savingproduct::all();

    $loanproducts = Loanproduct::all();

    $pdf = PDF::loadView('pdf.remittance', compact('members', 'organization', 'loanproducts', 'savingproducts'))->setPaper('a4')->setOrientation('landscape');

    return $pdf->stream('Remittance.pdf');
  }



  public function template()
  {

    $members = Member::all();

    $organization = Organization::find(1);

    $pdf = PDF::loadView('pdf.blank', compact('members', 'organization'))->setPaper('a4')->setOrientation('landscape');

    return $pdf->stream('Template.pdf');
  }



  public function loanlisting($data)
  {
    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
      $to = $data['date'];
    }

    if ($data['period'] == 'As at date') {
      $loans = Loanaccount::where('is_disbursed', true)->where('date_disbursed', '<=', $data['date'])->get();
    } else {
      $loans = Loanaccount::where('is_disbursed', true)->whereBetween('date_disbursed', array($from, $to))->get();
    }

    $organization = Organization::find(1);

    if ($data['format'] == 'excel') {
      return Excel::create('Loanlisting', function ($excel) use ($loans, $period, $to) {
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
        $excel->sheet('DisbursedLoans', function ($sheet) use ($loans, $period, $to) {
          $sheet->setAllBorders('thin');

          $sheet->setWidth(array("A" => 30, "B" => 50, "C" => 25, "D" => 20, "E" => 20, "F" => 20));
          $sheet->mergeCells('A1:E1');
          $sheet->row(1, array("Loan Listing Report " . $period));

          $sheet->cells('A1:F1', function ($cells) {
            $cells->setFont(array('bold' => true));
            $cells->setAlignment('center');
          });

          $sheet->row(2, array("Membership No", "Member name", "Loan product", "Loan Number", "Loan Amount", "Loan Balance"));

          $sheet->cells('A2:F2', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 3;
          $total = 0;
          $totalBal = 0;

          foreach ($loans as $loan) {
            if (Loantransaction::getLoanBalance($loan) > 5) {
              $sheet->cell('A' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->member->membership_no);
              });

              $sheet->cell('B' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->member->name);
              });

              $sheet->cell('C' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->loanproduct->name);
              });

              $sheet->cell('D' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->account_number);
              });

              $sheet->cell('E' . $row, function ($cell) use ($loan) {
                $cell->setValue(number_format(Loanaccount::getActualAmount($loan), 2));
              });

              $sheet->cell('F' . $row, function ($cell) use ($loan, $to) {
                $cell->setValue(number_format(Loantransaction::getLoanBalanceAt($loan, $to), 2));
              });

              $row++;
              $total += Loanaccount::getActualAmount($loan);
              $totalBal += Loantransaction::getLoanBalanceAt($loan, $to);
            }
          }

          $sheet->mergeCells('A' . $row . ':D' . $row);
          $sheet->cells('A' . $row . ':D' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('E' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });

          $sheet->cell('F' . $row, function ($cell) use ($totalBal) {
            $cell->setValue(number_format($totalBal, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    } else {
      $pdf = PDF::loadView('pdf.loanreports.loanbalances', compact('loans', 'organization', 'to', 'period'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('Loan Listing.pdf');
    }
  }


  public static function loanproductReport($data)
  {

    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
      $to = $data['date'];
    }

    $loanproduct = Loanproduct::find($data['loanproduct_id']);

    if ($data['period'] == 'As at date') {
      $loans = Loanaccount::where('loanproduct_id', $loanproduct->id)->where('is_disbursed', true)->where('date_disbursed', '<=', $data['date'])->get();
    } else {
      $loans = Loanaccount::where('loanproduct_id', $loanproduct->id)->where('is_disbursed', true)->whereBetween('date_disbursed', array($from, $to))->get();
    }

    $organization = Organization::find(1);

    if ($data['format'] == 'excel') {
      return Excel::create($loanproduct->name, function ($excel) use ($loans, $period, $to, $loanproduct) {
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
        $excel->sheet('DisbursedLoans', function ($sheet) use ($loans, $period, $to, $loanproduct) {
          $sheet->setAllBorders('thin');

          $sheet->setWidth(array("A" => 30, "B" => 50, "C" => 25, "D" => 20, "E" => 20, "F" => 20));
          $sheet->mergeCells('A1:E1');
          $sheet->row(1, array($loanproduct->name . " Report " . $period));

          $sheet->cells('A1:F1', function ($cells) {
            $cells->setFont(array('bold' => true));
            $cells->setAlignment('center');
          });

          $sheet->row(2, array("Membership No", "Member name", "Loan product", "Loan Number", "Loan Amount", "Loan Balance"));

          $sheet->cells('A2:F2', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 3;
          $total = 0;
          $totalBal = 0;

          foreach ($loans as $loan) {
            if (Loantransaction::getLoanBalance($loan) > 5) {
              $sheet->cell('A' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->member->membership_no);
              });

              $sheet->cell('B' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->member->name);
              });

              $sheet->cell('C' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->loanproduct->name);
              });

              $sheet->cell('D' . $row, function ($cell) use ($loan) {
                $cell->setValue($loan->account_number);
              });

              $sheet->cell('E' . $row, function ($cell) use ($loan) {
                $cell->setValue(number_format(Loanaccount::getActualAmount($loan), 2));
              });

              $sheet->cell('F' . $row, function ($cell) use ($loan, $to) {
                $cell->setValue(number_format(Loantransaction::getLoanBalanceAt($loan, $to), 2));
              });

              $row++;
              $total += Loanaccount::getActualAmount($loan);
              $totalBal += Loantransaction::getLoanBalanceAt($loan, $to);
            }
          }

          $sheet->mergeCells('A' . $row . ':D' . $row);
          $sheet->cells('A' . $row . ':D' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('E' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });

          $sheet->cell('F' . $row, function ($cell) use ($totalBal) {
            $cell->setValue(number_format($totalBal, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    } else {
      $pdf = PDF::loadView('pdf.loanreports.loanproducts', compact('loans', 'loanproduct', 'organization', 'to', 'period'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('Loan Product Listing.pdf');
    }
  }

  public function intercept()
  {
    $data = Input::all();


    if ($data['loanproduct_id'] == 'all') {
      if ($data['type'] == 'interest') {
        return self::allInterestReport($data);
      } elseif ($data['type'] == 'savings') {
        return self::allsavingsReport($data);
      } else {
        return self::loanlisting($data);
      }
    } else {
      if ($data['type'] == 'interest') {
        return self::interestReport($data);
      } elseif ($data['type'] == 'savings') {
        return self::savingsReport($data);
      } elseif ($data['type'] == 'financials') {
        return self::financials($data);
      } else {
        return self::loanproductReport($data);
      }
    }
  }

  public function interest($id)
  {
    $type = 'interest';
    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public function loanproduct($id)
  {
    $type = 'loans';

    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public static function interestReport($data)
  {

    //prepare date range for year and month selection
    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
    }

    $loanproduct = Loanproduct::find($data['loanproduct_id']);

    $loans = Loanaccount::where('loanproduct_id', $loanproduct->id)->where('is_disbursed', true)->lists('id');

    if ($data['period'] == 'As at date') {
      $interests = Loanrepayment::whereIn('loanaccount_id', $loans)->where('date', '<=', $data['date'])->get();
    } else {
      $interests = Loanrepayment::whereIn('loanaccount_id', $loans)->whereBetween('date', array($from, $to))->get();
    }
    $intArray = array();

    foreach ($interests as $interest) {
      // code...
      $member = $interest->loanaccount->member;
      if (!key_exists($member->id, $intArray)) {
        $intArray[$member->id] = array(
          'member_no' => $member->membership_no,
          'member_name' => $member->name,
          'loan_number' => $interest->loanaccount->account_number,
          'totalInt' => $interest->interest_paid
        );
      } else {

        $intArray[$member->id]['totalInt'] += $interest->interest_paid;
      }
    }

    //return $intArray;
    $organization = Organization::find(1);

    if ($data['format'] == 'excel') {
      return Excel::create($loanproduct->name, function ($excel) use ($intArray, $period, $loanproduct) {
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
        $excel->sheet('Interests', function ($sheet) use ($intArray, $period, $loanproduct) {
          $sheet->setAllBorders('thin');

          $sheet->setWidth(array("A" => 30, "B" => 50, "C" => 20));
          $sheet->mergeCells('A1:C1');
          $sheet->row(1, array($loanproduct->name . " Interest Report " . $period));

          $sheet->cells('A1:C1', function ($cells) {
            $cells->setFont(array('bold' => true));
            $cells->setAlignment('center');
          });

          $sheet->row(2, array("Membership No", "Member name", "Interest Amount"));

          $sheet->cells('A2:D2', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 3;
          $total = 0;

          foreach ($intArray as $interest) {
            $sheet->cell('A' . $row, function ($cell) use ($interest) {
              $cell->setValue($interest['member_no']);
            });

            $sheet->cell('B' . $row, function ($cell) use ($interest) {
              $cell->setValue($interest['member_name']);
            });

            $sheet->cell('C' . $row, function ($cell) use ($interest) {
              $cell->setValue(number_format($interest['totalInt'], 2));
            });

            $row++;
            $total += $interest['totalInt'];
          }

          $sheet->mergeCells('A' . $row . ':B' . $row);
          $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('C' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    } else {
      $pdf = PDF::loadView('pdf.loanreports.interest', compact('intArray', 'loanproduct', 'organization', 'period'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream($loanproduct->name . '_interest.pdf');
    }
  }

  public function totalInterest()
  {
    $id = 'all';
    $type = 'interest';
    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public function allListing()
  {
    $id = 'all';
    $type = 'loan';
    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public function allInterestReport($data)
  {

    $all = true;

    //prepare date range for year and month selection
    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
    }

    if ($data['period'] == 'As at date') {
      $interests = Loanrepayment::where('date', '<=', $data['date'])->get();
    } else {
      $interests = Loanrepayment::whereBetween('date', array($from, $to))->get();
    }

    $intArray = array();

    foreach ($interests as $interest) {
      // code...
      if (isset($interest)) {
        $member = $interest->loanaccount->member;

        if (!key_exists($member->id, $intArray)) {
          $intArray[$member->id] = array(
            'member_no' => $member->membership_no,
            'member_name' => $member->name,
            'totalInt' => $interest->interest_paid
          );
        } else {

          $intArray[$member->id]['totalInt'] += $interest->interest_paid;
        }
      }
    }

    //return $intArray;

    $organization = Organization::find(1);


    if ($data['format'] == 'excel') {
      return Excel::create('total_interests', function ($excel) use ($intArray, $period) {
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
        $excel->sheet('Interests', function ($sheet) use ($intArray, $period) {
          $sheet->setAllBorders('thin');

          $sheet->setWidth(array("A" => 30, "B" => 50, "C" => 20));
          $sheet->mergeCells('A1:C1');
          $sheet->row(1, array("Interest Report " . $period));

          $sheet->cells('A1:C1', function ($cells) {
            $cells->setFont(array('bold' => true));
            $cells->setAlignment('center');
          });

          $sheet->row(2, array("Membership No", "Member name", "Interest Amount"));

          $sheet->cells('A2:D2', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 3;
          $total = 0;

          foreach ($intArray as $interest) {
            $sheet->cell('A' . $row, function ($cell) use ($interest) {
              $cell->setValue($interest['member_no']);
            });

            $sheet->cell('B' . $row, function ($cell) use ($interest) {
              $cell->setValue($interest['member_name']);
            });

            $sheet->cell('C' . $row, function ($cell) use ($interest) {
              $cell->setValue(number_format($interest['totalInt'], 2));
            });

            $row++;
            $total += $interest['totalInt'];
          }

          $sheet->mergeCells('A' . $row . ':B' . $row);
          $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('C' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    } else {
      $pdf = PDF::loadView('pdf.loanreports.interest', compact('intArray', 'all', 'organization', 'period'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('Total_interest.pdf');
    }
  }

  public function loanrepayments()
  {

    $data = Input::all();

    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
    }

    if ($data['loanproduct'] == 'All') {
      $loans = Loanaccount::where('is_disbursed', 1)->lists('id');

      if ($data['period'] == 'As at date') {
        $loantransactions = Loanrepayment::whereIn('loanaccount_id', $loans)->where('date', '<=', $data['date'])->get();
      } else {
        $loantransactions = Loanrepayment::whereIn('loanaccount_id', $loans)->whereBetween('date', array($from, $to))->get();
      }
      $all = true;
    } else {
      $loans = Loanaccount::where('loanproduct_id', $data['loanproduct'])->where('is_disbursed', 1)->lists('id');
      if ($data['period'] == 'As at date') {
        $loantransactions = Loanrepayment::whereIn('loanaccount_id', $loans)->where('date', '<=', $data['date'])->get();
      } else {
        $loantransactions = Loanrepayment::whereIn('loanaccount_id', $loans)->whereBetween('date', array($from, $to))->get();
      }
      $all = false;

      $loanproduct = Loanproduct::find($data['loanproduct']);
    }

    $loantrans = array();
    //return $loantransactions;

    foreach ($loantransactions as $trans) {
      if (!key_exists($trans->loanaccount->account_number, $loantrans)) {

        $loantrans[$trans->loanaccount->account_number] = array(
          'member_no' => $trans->loanaccount->member->membership_no,
          'member_name' => $trans->loanaccount->member->name,
          'total' => $trans->principal_paid
        );
      } else {
        $loantrans[$trans->loanaccount->account_number]['total'] += $trans->principal_paid;
      }
    }
    $loanproduct = Loanproduct::find($data['loanproduct']);


    $organization = Organization::find(1);

    $pdf = PDF::loadView('pdf.loanreports.repayments', compact('organization', 'period', 'loantrans', 'all', 'loanproduct'))->setPaper('a4')->setOrientation('potrait');

    return $pdf->stream('Repayments.pdf');
  }


  public function savinglisting()
  {
    $id = 'all';
    $type = 'savings';

    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public static function allsavingsReport($data)
  {

    $savingaccounts = Savingaccount::all();

    //prepare date range for year and month selection
    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
    }

    $savings = array();

    foreach ($savingaccounts as $savingaccount) {
      // code...
      if ($data['period'] == 'As at date') {
        $balance = Savingaccount::getSavingsAsAt($savingaccount, $data['date']);
      } else {
        $balance = Savingaccount::getSavingsBetween($savingaccount, $from, $to);
      }
      $arr = array(
        'member_no' => $savingaccount->member->membership_no,
        'member_name' => $savingaccount->member->name,
        'product_name' => $savingaccount->savingproduct->name,
        'account_no' => $savingaccount->account_number,
        'balance' => $balance
      );
      array_push($savings, $arr);
    }
    $organization = Organization::find(1);

    if ($data['format'] == 'pdf') {

      $pdf = PDF::loadView('pdf.savingreports.savingbalances', compact('savings', 'organization', 'period'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('Savings Listing.pdf');
    } else { //Displays in Excel format

      return Excel::create('Savings Report', function ($excel) use ($savings, $period) {

        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


        $excel->sheet('Savings Report', function ($sheet) use ($savings, $period) {

          $sheet->setAllBorders('thin');

          $sheet->setWidth(array(
            'A' => 15,
            'B' => 40,
            'C' => 15,
            'D' => 20,
            'E' => 15
          ));

          $sheet->mergeCells('A1:E1');

          $sheet->row(1, array("Savings Listing Report " . $period));

          $sheet->cells('A1:E1', function ($cells) {
            $cells->setAlignment('center');
            // $cells->setBackground('#777777');
            $cells->setFont(array(
              'family' => 'Calibri',
              'bold' => true
            ));
          });

          $sheet->mergeCells('A2:E2');


          $sheet->row(3, array(
            "Member", "Member Name", "Saving Product", "Account Number", "Account Balance"
          ));

          $sheet->cells('A3:E3', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 4;
          $total = 0;
          foreach ($savings as $saving) {
            $sheet->cell('A' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['member_no']);
            });
            $sheet->cell('B' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['member_name']);
            });
            $sheet->cell('C' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['product_name']);
            });
            $sheet->cell('D' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['account_no']);
            });
            $sheet->cell('E' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['balance']);
            });

            $row++;
            $total += $saving['balance'];
          }

          $sheet->mergeCells('A' . $row . ':D' . $row);
          $sheet->cells('A' . $row . ':D' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('E' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    }
  }



  public function savingproduct($id)
  {
    $type = 'savings';
    return View::make('pdf.selectperiod', compact('id', 'type'));
  }

  public static function savingsReport($data)
  {

    $savingproduct = Savingproduct::find($data['savingaccount_id']);

    $savingaccounts = $savingproduct->savingaccounts;
    //prepare date range for year and month selection
    if ($data['period'] == 'month') {
      $month = $data['month'];
      $from = date('Y-m-01', strtotime('01-' . $month));
      $to = date('Y-m-t', strtotime($from));
      $period = 'for ' . date('F Y', strtotime($from));
    } elseif ($data['period'] == 'year') {
      $year = $data['year'];
      $from = $year . '-01-01';
      $to = $year . '-12-31';
      $period = 'for ' . $year;
    } elseif ($data['period'] == 'custom') {
      $from = $data['from'];
      $to = $data['to'];
      $period = 'for ' . date('d-M-Y', strtotime($from)) . ' to ' . date('d-M-Y', strtotime($to));
    } else {
      $period = 'as at ' . $data['date'];
    }

    $savings = array();

    foreach ($savingaccounts as $savingaccount) {
      // code...
      if ($data['period'] == 'As at date') {
        $balance = Savingaccount::getSavingsAsAt($savingaccount, $data['date']);
      } else {
        $balance = Savingaccount::getSavingsBetween($savingaccount, $from, $to);
      }

      /** $arr = array('member_no' => $savingaccount->member->membership_no,
                    'member_name' => $savingaccount->member->name,
                    'product_name' => $savingaccount->savingproduct->name,
                    'account_no' => $savingaccount->account_number,
                    'balance' => $balance
                  );

      array_push($savings, $arr);
    }**/
      $arr = array(
        'member_no' => $savingaccount->member->membership_no,
        'member_name' => $savingaccount->member->name,
        'product_name' => $savingaccount->savingproduct->name,
        'account_no' => $savingaccount->account_number,
        'balance' => $balance,
        'interest_amount' => $balance * ($savingaccount->savingproduct->Interest_Rate) / 100,
      );
      array_push($savings, $arr);
    }
    $organization = Organization::find(1);
    $producttype = $savingproduct->name;
    $interestrate = $savingproduct->Interest_Rate;

    $title = ucwords(strtolower($savingproduct->name)) . ' Report';
    if ($data['format'] == 'pdf') {
      $pdf = PDF::loadView('pdf.savingreports.savingproducts', compact('savings', 'interestrate', 'organization', 'period', 'title'))->setPaper('a4')->setOrientation('potrait');

      return $pdf->stream('Saving Product Deposit.pdf');
    } else {
      return Excel::create($title, function ($excel) use ($savings, $title, $period) {

        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


        $excel->sheet('Savings Deposits Report', function ($sheet) use ($savings, $title, $period) {

          $sheet->setAllBorders('thin');

          $sheet->setWidth(array(
            'A' => 15,
            'B' => 50,
            'C' => 15,
            'D' => 50,
            'E' => 15
          ));

          $sheet->mergeCells('A1:E1');

          $sheet->row(1, array($title . ' ' . $period));

          $sheet->cells('A1:E1', function ($cells) {
            $cells->setAlignment('center');
            // $cells->setBackground('#777777');
            $cells->setFont(array(
              'family' => 'Calibri',
              'bold' => true
            ));
          });

          $sheet->mergeCells('A2:E2');


          $sheet->row(3, array(
            "Member", "Member Name", "Saving Product", "Account Number", "Account Balance"
          ));

          $sheet->cells('A3:E3', function ($cells) {
            $cells->setFont(array(
              'bold' => true
            ));
          });

          if (ob_get_level() > 0) {
            ob_end_clean();
          }

          $row = 4;
          $total = 0;

          foreach ($savings as $saving) {
            $sheet->cell('A' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['member_no']);
            });
            $sheet->cell('B' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['member_name']);
            });
            $sheet->cell('C' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['product_name']);
            });
            $sheet->cell('D' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['account_no']);
            });
            $sheet->cell('E' . $row, function ($cell) use ($saving) {
              $cell->setValue($saving['balance']);
            });

            $total += $saving['balance'];
            $row++;
          }

          $sheet->mergeCells('A' . $row . ':D' . $row);
          $sheet->cells('A' . $row . ':D' . $row, function ($cells) {
            $cells->setFont(array('bold' => true));
          });

          $sheet->row($row, array('Total:'));
          $sheet->cell('E' . $row, function ($cell) use ($total) {
            $cell->setValue(number_format($total, 2));
            $cell->setFont(array('bold' => true));
          });
        });
      })->export('xlsx');
    }
  }

  public function monthlyrepayments()
  {

    $date = Input::get('date');
    $loanid = Input::get('member');
    if ($date != null && $loanid != null) {
      $scrapdate = Loanrepayment::where('loanaccount_id', '=', $loanid)
        ->get();


      $organization = Organization::find(1);
      $pdf = PDF::loadView('pdf.monthlyrepayments', compact('date', 'scrapdate', 'organization'))->setPaper('a4')->setOrientation('potrait');
      return $pdf->stream('Monthly Repayment Report.pdf');
    } else {
      return Redirect::back()
        ->withAlarm('Please select the repayment duration and the respective member');
    }
  }

  public function creditappraisal($id, $loanid)
  {
    $member = Member::where('id', '=', $id)->get()->first();
    $loans = Loanaccount::where('member_id', '=', $id)
      ->where('is_disbursed', '=', 1)
      ->get();
    $currentloan = Loanaccount::where('id', '=', $loanid)
      ->get()->first();
    $savingaccount = DB::table('savingaccounts')
      ->where('member_id', '=', $id)->pluck('account_number');
    $savings = DB::table('savingtransactions')
      ->join('savingaccounts', 'savingtransactions.savingaccount_id', '=', 'savingaccounts.id')
      ->where('savingaccounts.member_id', '=', $id)
      ->where('savingtransactions.type', '=', 'credit')
      ->sum('savingtransactions.amount');
    $shareaccount = DB::table('shareaccounts')
      ->where('member_id', '=', $id)->pluck('account_number');
    $shares = DB::table('sharetransactions')
      ->join('shareaccounts', 'sharetransactions.shareaccount_id', '=', 'shareaccounts.id')
      ->where('shareaccounts.member_id', '=', $id)
      ->where('sharetransactions.type', '=', 'credit')
      ->sum('sharetransactions.amount');
    $pdf = PDF::loadView('pdf.loanreports.creditappraisal', compact('member', 'loans', 'savings', 'savingaccount', 'shares', 'shareaccount', 'currentloan'))->setPaper('a4')->setOrientation('portrait');
    return $pdf->stream('Member Credit Appraisal Report.pdf');
  }


  public function financials()
  {

    $data = Input::all();
    $report = $data['report_type'];
    $date = $data['date'];

    $accounts = Account::all();

    $organization = Organization::find(1);

    $from = $data['from'];
    $to = $data['to'];
    $period = $data['period'];
    $incomeParticular = $data['income-particulars'];
    $expenseParticular = $data['expense-particulars'];
    $arr = array();
    $openingbalance = 0.0;
    foreach ($accounts as $account) {
      $date2 = date('Y', strtotime('2020-01-01'));
      $openingbalance = Tbopeningbalance::where('account_id', '=', $account->id)->where('opening_bal_for', '=', $date2)->pluck('account_balance');

      // code...
      if (!key_exists($account->category, $arr)) {
        $arr[$account->category] = [];
      }



      if ($period != 'custom') {
        $bal = array(
          'name' => $account->name,
          'balance' => Tbopeningbalance::getTrialBalanceAsAt($account, $openingbalance, $date)
        );
        //'balance'=> Account::getAccountBalanceAtDate($account, $date));

        array_push($arr[$account->category], $bal);
      } else {
        $bal = array(
          'name' => $account->name,
          'balance' => Tbopeningbalance::getTrialBalance($account, $openingbalance, $from, $to)
        );

        //'balance'=> Account::getAccountBalanceBetween($account, $from, $to));
        array_push($arr[$account->category], $bal);
      }
    }

    if ($report == 'balancesheet') {
      $accounts = $arr;

      if ($data['format'] == 'excel') {
        return Excel::create('balancesheet', function ($excel) use ($accounts, $period, $date, $to, $from) {

          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
          $excel->sheet('BalanceSheet', function ($sheet) use ($accounts, $period, $date, $to, $from) {

            $sheet->setAllBorders('thin');

            $sheet->setWidth(array(
              'A' => 60,
              'B' => 15,
            ));
            $sheet->mergeCells('A1:B1');
            if ($period = 'As at date') {
              $sheet->row(1, array("BALANCESHEET REPORT FOR " .  $date));
            } else {
              $sheet->row(1, array("BALANCESHEET REPORT FROM " . $from . " TO " . $to));
            }
            $sheet->cells('A1:B1', function ($cells) {
              $cells->setAlignment('center');
              // $cells->setBackground('#777777');
              $cells->setFont(array(
                'bold' => true
              ));
            });
            $sheet->row(2, array(
              "ACCOUNT DESCRIPTION", "AMOUNT",
            ));
            $sheet->cells('A2:B2', function ($cells) {
              $cells->setFont(array(
                'bold' => true
              ));
            });

            if (ob_get_level() > 0) {
              ob_end_clean();
            }

            $sheet->mergeCells('A3:B3');


            $row = 4;
            $total_assets = 0;
            $total_liabilities = 0;
            $total_equity = 0;
            $total_income = 0;
            $total_expense = 0;
            $totals = array();
            foreach ($accounts as $key => $value) {

              $sheet->mergeCells('A' . $row . ':B' . $row);
              $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
                $cells->setFont(array(
                  'bold' => true
                ));
              });
              $sheet->row($row, array($key));
              $row++;
              $totals[$key] = 0;
              foreach ($value as $account) {
                $sheet->cell('A' . $row, function ($cell) use ($account, $date, $period) {
                  $cell->setValue($account['name']);
                });

                $sheet->cell('B' . $row, function ($cell) use ($account, $date, $period) {
                  $cell->setValue(number_format($account['balance'], 2));
                });


                $totals[$key] += $account['balance'];

                $row++;
              }


              $sheet->cell('A' . $row, function ($cell) use ($key) {
                $cell->setFont(array(
                  'bold' => true
                ));
                $cell->setValue("TOTALS " . $key);
              });

              $sheet->cell('B' . $row, function ($cell) use ($totals, $key) {
                $cell->setValue(number_format($totals[$key], 2));
                $cell->setFont(array(
                  'bold' => true
                ));
              });
              $row++;
            }

            $row += 2;
            $sheet->mergeCells('A' . $row . ':B' . $row);

            $sheet->row($row, array("Printed on: " . date('D M j, Y') . " by " . Confide::user()->username));

            $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
              $cells->setAlignment('center');
              $cells->setFont(array(
                'bold' => true
              ));
            });
          });
        })->export('xlsx');
      } else {
        $pdf = PDF::loadView('pdf.financials.balancesheet', compact('accounts', 'to', 'from', 'date', 'organization'))->setPaper('a4')->setOrientation('potrait');

        return $pdf->stream('Balance Sheet.pdf');
      }
    }


    if ($report == 'income') {

      //return $incomedata;
      if ($data['format'] == 'excel') {
        $incomedata = array('INCOME' => array(), 'EXPENSE' => array());

        foreach ($accounts as $account) {
          $date2 = date('2020');
          $openingbalance = Tbopeningbalance::where('account_id', '=', $account->id)->where('opening_bal_for', '=', $date2)->pluck('account_balance');
          if ($account->category == 'INCOME') {
            $data1["name"] = $account->name;
            //changes made for the the purpose of including openbalance figures in the trial balance
            $data1['balance'] = Tbopeningbalance::getTrialBalance($account, $openingbalance, $from, $to);
            // $data1['balance'] = Account::getAccountBalanceAtDate($account, $date);
            array_push($incomedata['INCOME'], $data1);
          } elseif ($account->category == 'EXPENSE') {
            $data1["name"] = $account->name;
            $data1['balance'] = Tbopeningbalance::getTrialBalance($account, $openingbalance, $from, $to);
            //$data1['balance'] = Account::getAccountBalanceAtDate($account, $date);
            array_push($incomedata['EXPENSE'], $data1);
          }
        }

        return Excel::create('INCOME STATEMENT', function ($excel) use ($incomedata, $period, $date, $to, $from) {

          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
          $excel->sheet('INCOME STATEMENT', function ($sheet) use ($incomedata, $period, $date, $to, $from) {

            $sheet->setAllBorders('thin');

            $sheet->setWidth(array(
              'A' => 60,
              'B' => 15,
            ));
            $sheet->mergeCells('A1:B1');
            if ($period = 'As at date') {
              $sheet->row(1, array("INCOME STATEMENT REPORT FOR " .  $date));
            } else {
              $sheet->row(1, array("INCOME STATEMENT REPORT FROM " . $from . " TO " . $to));
            }
            $sheet->cells('A1:B1', function ($cells) {
              $cells->setAlignment('center');
              // $cells->setBackground('#777777');
              $cells->setFont(array(
                'bold' => true
              ));
            });
            $sheet->row(2, array(
              "ACCOUNT DESCRIPTION", "AMOUNT",
            ));
            $sheet->cells('A2:B2', function ($cells) {
              $cells->setFont(array(
                'bold' => true
              ));
            });

            if (ob_get_level() > 0) {
              ob_end_clean();
            }

            $sheet->mergeCells('A3:B3');


            $row = 4;
            $total_assets = 0;
            $total_liabilities = 0;
            $total_equity = 0;
            $total_income = 0;
            $total_expense = 0;
            $totals = array();
            foreach ($incomedata as $key => $value) {

              $sheet->mergeCells('A' . $row . ':B' . $row);
              $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
                $cells->setFont(array(
                  'bold' => true
                ));
              });
              $sheet->row($row, array($key));
              $row++;
              $totals[$key] = 0;
              foreach ($value as $account) {
                $sheet->cell('A' . $row, function ($cell) use ($account, $date, $period) {
                  $cell->setValue($account['name']);
                });

                $sheet->cell('B' . $row, function ($cell) use ($account, $date, $period) {
                  $cell->setValue(number_format($account['balance'], 2));
                });


                $totals[$key] += $account['balance'];

                $row++;
              }


              $sheet->cell('A' . $row, function ($cell) use ($key) {
                $cell->setFont(array(
                  'bold' => true
                ));
                $cell->setValue("TOTALS " . $key);
              });

              $sheet->cell('B' . $row, function ($cell) use ($totals, $key) {
                $cell->setValue(number_format($totals[$key], 2));
                $cell->setFont(array(
                  'bold' => true
                ));
              });
              $row += 2;
            }


            $sheet->cell('A' . $row, function ($cell) {
              $cell->setFont(array(
                'bold' => true
              ));
              $cell->setValue("TOTALS INCOME");
            });

            $sheet->cell('B' . $row, function ($cell) use ($totals) {
              $cell->setValue(number_format($totals["INCOME"] - $totals['EXPENSE'], 2));
              $cell->setFont(array(
                'bold' => true
              ));
            });
            $row++;

            $row += 2;
            $sheet->mergeCells('A' . $row . ':B' . $row);

            $sheet->row($row, array("Printed on: " . date('D M j, Y') . " by " . Confide::user()->username));

            $sheet->cells('A' . $row . ':B' . $row, function ($cells) {
              $cells->setAlignment('center');
              $cells->setFont(array(
                'bold' => true
              ));
            });
          });
        })->export('xlsx');
      } else {
        $pdf = PDF::loadView('pdf.financials.incomestatement', compact('accounts', 'date', 'period', 'from', 'to', 'organization'))->setPaper('a4')->setOrientation('potrait');

        return $pdf->stream('Income Statement.pdf');
      }
    }


    if ($report == 'trialbalance') {


      if ($data['format'] == 'excel') {
        return Excel::create('trialbalance', function ($excel) use ($accounts, $openingbalance, $period, $date, $to, $from) {

          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
          require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
          $excel->sheet('Trial Balance', function ($sheet) use ($accounts, $period, $date, $to, $from) {

            $sheet->setAllBorders('thin');

            $sheet->setWidth(array(
              'A' => 60,
              'B' => 15,
              'C' => 15,
            ));
            $sheet->mergeCells('A1:C1');
            if ($period = 'As at date') {
              $sheet->row(1, array("TRIALSHEET REPORT FOR " .  $date));
            } else {
              $sheet->row(1, array("TRIALSHEET REPORT FROM " . $from . " TO " . $to));
            }
            $sheet->cells('A1:C1', function ($cells) {
              $cells->setAlignment('center');
              // $cells->setBackground('#777777');
              $cells->setFont(array(
                'bold' => true
              ));
            });
            $sheet->row(2, array(
              "ACCOUNT DESCRIPTION", "CREDIT", "DEBIT"
            ));
            $sheet->cells('A2:C2', function ($cells) {
              $cells->setFont(array(
                'bold' => true
              ));
            });

            if (ob_get_level() > 0) {
              ob_end_clean();
            }

            $sheet->mergeCells('A3:C3');


            $row = 4;
            $total_credits = 0;
            $total_debits = 0;
            foreach ($accounts as $account) {
              $date2 = date('Y', strtotime('2020-01-01'));
              $openingbalance = Tbopeningbalance::where('account_id', '=', $account->id)->where('opening_bal_for', '=', $date2)->pluck('account_balance');


              if (Account::getAccountBalanceAtDate($account, $date) != 0) {
                $sheet->cell('A' . $row, function ($cell) use ($account, $date, $period) {
                  $cell->setValue($account['name']);
                });
                if ($account->category == 'ASSET' || $account->category == 'EXPENSE') {
                  $sheet->cell('B' . $row, function ($cell) use ($account, $openingbalance, $date, $period) {
                    $cell->setValue(number_format(Tbopeningbalance::getTrialBalanceAsAt($account, $openingbalance, $date), 0));

                    //$cell->setValue(number_format(Account::getAccountBalanceAtDate($account, $date),0));
                  });

                  $sheet->cell('C' . $row, function ($cell) use ($account, $date, $period) {
                    $cell->setValue(number_format(0, 0));
                  });
                  $total_credits += Tbopeningbalance::getTrialBalanceAsAt($account, $openingbalance, $date);


                  //$total_credits += Account::getAccountBalanceAtDate($account, $date);

                } else {
                  $sheet->cell('B' . $row, function ($cell) use ($account, $date, $period) {
                    $cell->setValue(number_format(0, 0));
                  });

                  $sheet->cell('C' . $row, function ($cell) use ($account, $openingbalance, $date, $period) {
                    $cell->setValue(number_format(Tbopeningbalance::getTrialBalanceAsAt($account, $openingbalance, $date), 0));
                    //$cell->setValue(number_format(Account::getAccountBalanceAtDate($account, $date),0));
                  });

                  $total_debits += Tbopeningbalance::getTrialBalanceAsAt($account, $openingbalance, $date);
                  //$total_debits += Account::getAccountBalanceAtDate($account, $date);

                }




                $row++;
              }
            }


            $sheet->cell('A' . $row, function ($cell) {
              $cell->setFont(array(
                'bold' => true
              ));
              $cell->setValue("TOTALS ");
            });

            $sheet->cell('B' . $row, function ($cell) use ($total_credits) {
              $cell->setValue(number_format($total_credits, 2));
              $cell->setFont(array(
                'bold' => true
              ));
            });
            $sheet->cell('C' . $row, function ($cell) use ($total_debits) {
              $cell->setValue(number_format($total_debits, 2));
              $cell->setFont(array(
                'bold' => true
              ));
            });

            $row += 2;
            $sheet->mergeCells('A' . $row . ':C' . $row);

            $sheet->row($row, array("Printed on: " . date('D M j, Y') . " by " . Confide::user()->username));

            $sheet->cells('A' . $row . ':C' . $row, function ($cells) {
              $cells->setAlignment('center');
              $cells->setFont(array(
                'bold' => true
              ));
            });
          });
        })->export('xlsx');
      } else {
        $pdf = PDF::loadView('pdf.financials.trialbalance', compact('accounts', 'from', 'to', 'period', 'date', 'organization'))->setPaper('a4')->setOrientation('potrait');

        return $pdf->stream('Trial Balance.pdf');
      }
    }

    if ($report == 'cashbook') {
      return View::make('pdf.financials.cashbookperiod');
    }

    if ($report == 'budget') {
      $set_year = date('Y', strtotime($date));
      $previous_year = $set_year - 1;

      $projections = array(
        'Interest' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
          ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
          ->where('proposal_entries.year', '=', $set_year)
          ->where('proposal_categories.type', '=', 'INTEREST')
          ->get(),
        'Income' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
          ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
          ->where('proposal_entries.year', '=', $set_year)
          ->where('proposal_categories.type', '=', 'OTHER INCOME')
          ->get(),
        'Expenditure' => DB::table('proposal_entries')->select('proposal_entries.year', 'proposal_entries.first_quarter', 'proposal_entries.second_quarter', 'proposal_entries.third_quarter', 'proposal_entries.fourth_quarter', 'proposal_categories.type', 'proposal_categories.name')
          ->join('proposal_categories', 'proposal_entries.proposal_category_id', '=', 'proposal_categories.id')
          ->where('proposal_entries.year', '=', $set_year)
          ->where('proposal_categories.type', '=', 'EXPENDITURE')
          ->get()
      );

      $pdf = PDF::loadView('pdf.budget_report', compact('set_year', 'previous_year', 'projections'))
        ->setPaper('a4')->setOrientation('landscape');
      return $pdf->stream($set_year . '_budget_report.pdf');
    }

    if ($report == 'income_reports') {
      $incomeAccounts = Account::select('id')->where('category', 'INCOME')->get()->toArray();
      $particulars = Particular::select('id')->whereIn('creditaccount_id', $incomeAccounts)->get();

      if ($incomeParticular != '0') {
        $particular = Particular::findOrFail($incomeParticular);
        foreach ($incomeAccounts as $key => $incomeAccount) {
          if ($incomeAccount['id'] != $particular->creditaccount_id) {
            unset($incomeAccounts[$key]);
          }
        }

        if ($period == 'As at date') {
          $incomeSums = Journal::whereIn('account_id', $incomeAccounts)
            ->where('particulars_id', $incomeParticular)
            ->where('date', $date)
            ->where('void', false)
            ->orderBy('date')
            ->get();
        } else {
          $incomeSums = Journal::whereIn('account_id', $incomeAccounts)
            ->where('particulars_id', $incomeParticular)
            ->whereBetween('date', array($from, $to))
            ->where('void', false)
            ->orderBy('date')
            ->get();
        }
      } else {

        if ($period == 'As at date') {
          $incomeSums = Journal::whereIn('account_id', $incomeAccounts)
            ->whereNotNull('particulars_id')
            ->where('date', $date)
            ->where('void', false)
            ->orderBy('date')
            ->get();
        } else {
          $incomeSums = Journal::whereIn('account_id', $incomeAccounts)
            ->whereNotNull('particulars_id')
            ->where('void', false)
            ->whereBetween('date', array($from, $to))
            ->orderBy('date')
            ->get();
        }
      }

      $incomes = array();

      foreach ($incomeSums as $income) {
        $particular = $income->particular->name;
        if (key_exists($particular, $incomes)) {
          $incomes[$particular]['amount'] += $income->amount;
        } else {
          $incomes[$particular]['amount'] = $income->amount;
          $incomes[$particular]['income'] = $income;
        }
      }

      $pdf = PDF::loadView('pdf.financials.income_reports', compact('incomeAccounts', 'date', 'organization', 'from', 'to', 'period', 'particulars', 'incomeSums', 'incomes'))->setPaper('a4')->setOrientation('potrait');
      return $pdf->stream('Income Report.pdf');
    }

    if ($report == 'expenses_reports') {
      $expenseAccounts = Account::select('id')->where('category', 'EXPENSE')->get()->toArray();
      $particulars = Particular::whereIn('debitaccount_id', $expenseAccounts)->get();

      if ($expenseParticular != '0') {
        $particular = Particular::findOrFail($expenseParticular);
        foreach ($expenseAccounts as $key => $expenseAccount) {
          if ($expenseAccount['id'] != $particular->debitaccount_id) {
            unset($expenseAccounts[$key]);
          }
        }

        if ($period == 'As at date') {
          $expenses = Journal::whereIn('account_id', $expenseAccounts)
            ->where('particulars_id', $expenseParticular)
            ->where('date', $date)
            ->where('void', false)
            ->orderBy('date')
            ->get();
        } else {
          $expenses = Journal::whereIn('account_id', $expenseAccounts)
            ->where('particulars_id', $expenseParticular)
            ->whereBetween('date', array($from, $to))
            ->where('void', false)
            ->orderBy('date')
            ->get();
        }
      } else {

        if ($period == 'As at date') {
          $expenses = Journal::whereIn('account_id', $expenseAccounts)
            ->whereNotNull('particulars_id')
            ->where('date', $date)
            ->where('void', false)
            ->orderBy('date')
            ->get();
        } else {
          $expenses = Journal::whereIn('account_id', $expenseAccounts)
            ->whereNotNull('particulars_id')
            ->whereBetween('date', array($from, $to))
            ->orderBy('date')
            ->where('void', false)
            ->get();
        }
      }

      $expe = array();

      foreach ($expenses as $expense) {
        $particular = $expense->particular->name;
        if (key_exists($particular, $expe)) {
          $expe[$particular]['amount'] += $expense->amount;
        } else {
          $expe[$particular]['amount'] = $expense->amount;
          $expe[$particular]['expense'] = $expense;
        }
      }


      $pdf = PDF::loadView('pdf.financials.expenses_reports', compact('expenseAccounts', 'date', 'organization', 'from', 'to', 'period', 'particulars', 'expenses', 'expe'))->setPaper('a4')->setOrientation('potrait');
      return $pdf->stream('Expenses Report.pdf');
    }
  }

  /**
   * GENERATE BANK RECONCILIATION REPORT
   */
  public function displayRecOptions()
  {
    $bankAccounts = DB::table('bank_accounts')
      ->get();

    $bookAccounts = DB::table('accounts')
      ->where('category', 'ASSET')
      ->get();

    return View::make('banking.recOptions', compact('bankAccounts', 'bookAccounts'));
  }

  public function showRecReport()
  {
    $bankAcID = Input::get('bank_account');
    $bookAcID = Input::get('book_account');
    $recMonth = Input::get('rec_month');

    //get statement id
    $bnkStmtID = DB::table('bank_statements')
      ->where('stmt_month', $recMonth)
      ->pluck('id');

    $bnkStmtBal = DB::table('bank_statements')
      ->where('bank_account_id', $bankAcID)
      ->where('stmt_month', $recMonth)
      ->select('bal_bd')
      ->first();

    $acTransaction = DB::table('account_transactions')
      ->where('status', '=', 'RECONCILED')
      ->where('bank_statement_id', $bnkStmtID)
      ->whereMonth('transaction_date', '=', substr($recMonth, 0, 2))
      ->whereYear('transaction_date', '=', substr($recMonth, 3, 6))
      ->select('id', 'account_credited', 'account_debited', 'transaction_amount')
      ->get();

    $bkTotal = 0;
    foreach ($acTransaction as $acnt) {
      if ($acnt->account_debited == $bookAcID) {
        $bkTotal += $acnt->transaction_amount;
      } else if ($acnt->account_credited == $bookAcID) {
        $bkTotal -= $acnt->transaction_amount;
      }
    }

    $additions = DB::table('account_transactions')
      ->where('status', '=', 'RECONCILED')
      ->where('bank_statement_id', $bnkStmtID)
      ->whereMonth('transaction_date', '<>', substr($recMonth, 0, 2))
      ->whereYear('transaction_date', '=', substr($recMonth, 3, 6))
      ->select('id', 'description', 'account_credited', 'account_debited', 'transaction_amount')
      ->get();

    $add = [];
    $less = [];
    foreach ($additions as $additions) {
      if ($additions->account_debited == $bookAcID) {
        array_push($add, $additions);
      } else if ($additions->account_credited == $bookAcID) {
        array_push($less, $additions);
      }
    }

    $organization = Organization::find(1);

    $pdf = PDF::loadView('banking.bankReconciliationReport', compact('recMonth', 'organization', 'bnkStmtBal', 'bkTotal', 'add', 'less'))->setPaper('a4')->setOrientation('potrait');
    return $pdf->stream('Reconciliation Reports');
    /*if(count($bnkStmtBal) == 0 || $bkTotal == 0 || count($additions) == 0 ){
					return "Error";
					//return View::make('erpreports.bankReconciliationReport')->with('error','Cannot generate report for this Reconciliation! Please check paremeters!');
			} else{
					return "Success";*/
    return View::make('banking.bankReconciliationReport', compact('recMonth', 'organization', 'bnkStmtBal', 'bkTotal', 'add', 'less'));
    //}
  }
  public function selecttransactionPeriod()
  {

    $transaction = AccountTransaction::all();
    return View::make('reports.selecttransactionperiod', compact('transaction'));
  }
  public function transaction()
  {
    $from = Input::get('from');
    $to = Input::get('to');
    $data = Input::all();
    $accounts = Account::where('name', 'like', '%' . 'Bank Account' . '%')->pluck('id');
    //get deposits
    $type = AccountTransaction::where('account_debited', $accounts)->pluck('description');
    $transaction = AccountTransaction::whereBetween('transaction_date', array(Input::get('from'), Input::get('to')))->where('description', $type)->get();

    //get withdrawals
    $type1 = AccountTransaction::where('account_credited', $accounts)->pluck('description');
    $transaction1 = AccountTransaction::whereBetween('transaction_date', array(Input::get('from'), Input::get('to')))->where('description', $type1)->get();


    $organization = Organization::find(1);
    //  $transaction=AccountTransaction::whereBetween('transaction_date', array(Input::get('from'),Input::get('to')))->where('description',$type)->get();
    $pdf = PDF::loadView('reports.transactionReport', compact('transaction', 'transaction1', 'organization', 'from', 'type', 'to'))->setPaper('a4')->setOrientation('potrait');
    return $pdf->stream('Transactions Reports');
  }

  public function loan_arrears()
  {
    $loans = Loanaccount::select('loanproduct_id', 'amount_applied')->groupBy('loanproduct_id')->get()->toArray();
    return View::make('pdf.loan_arrears', compact('loans'));
  }
  public function loan_arrears_pdf()
  {
    $organization = Organization::find(1); //where("active",1)->get();
    $product_id = Input::get('product');
    $format = Input::get('format');
    $loan_product = Loanproduct::find($product_id);
    $loan_accs = Loanaccount::where("loanproduct_id", $product_id)->where("is_disbursed", 1)->get();
    //  $transaction=AccountTransaction::whereBetween('transaction_date', array(Input::get('from'),Input::get('to')))->where('description',$type)->get();
    $pdf = PDF::loadView('reports.loan_arrears_pdf', compact('loan_accs', 'loan_product', 'organization'))->setPaper('a4')->setOrientation('potrait');
    return $pdf->stream('Loan_arrears Reports');
  }
}
