<?php

class SavingtransactionsController extends \BaseController
{

    /**
     * Display a listing of savingtransactions
     *
     * @return Response
     */
    public function index()
    {
        $savingtransactions = Savingtransaction::all();

        return View::make('savingtransactions.index', compact('savingtransactions'));
    }

    /**
     * Show the form for creating a new savingtransaction
     *
     * @return Response
     */
    public function create($id)
    {

        $savingaccount = Savingaccount::findOrFail($id);

        $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->sum('amount');
        $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $credit - $debit;

        $member = $savingaccount->member;


        if (Confide::user()->user_type == 'member') {
            return View::make('css.membersavings', compact('savingaccount', 'member', 'balance'));
        } else {
            return View::make('savingtransactions.create', compact('savingaccount', 'member', 'balance'));
        }

    }

    /**
     * Store a newly created savingtransaction in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $date = Input::get('date');
        $transAmount = Input::get('amount');
        $bank = Input::get('bank_reference');

        $savingaccount = Savingaccount::findOrFail(Input::get('account_id'));
        $date = Input::get('date');
        $amount = Input::get('amount');
        $type = Input::get('type');
        $description = Input::get('description');
        $transacted_by = Input::get('transacted_by');
        $method = Input::get('pay_method');
        $member = Member::findOrFail($savingaccount->member_id);


        Savingtransaction::transact($date, $savingaccount, $amount, $type, $description, $transacted_by, $member,$bank,$method);


        return Redirect::to('savingtransactions/show/' . $savingaccount->id);
    }

    /**
     * Display the specified savingtransaction.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {            $account = Savingaccount::findOrFail($id);
               $interest=DB::table('savingproducts')->where('id', '=', $account->savingproduct_id)->pluck('Interest_Rate');
        $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
        $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $credit - $debit;

        return View::make('savingtransactions.show', compact('account','interest', 'balance'));
    }

    /**
     * Show the form for editing the specified savingtransaction.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $savingtransaction = Savingtransaction::find($id);

        return View::make('savingtransactions.edit', compact('savingtransaction'));
    }

    /**
     * Update the specified savingtransaction in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $savingtransaction = Savingtransaction::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $savingtransaction->update($data);

        return Redirect::route('savingtransactions.index');
    }

    /**
     * Remove the specified savingtransaction from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Savingtransaction::destroy($id);

        return Redirect::route('savingtransactions.index');
    }


    public function receipt($id)
    {

        $transaction = Savingtransaction::findOrFail($id);

        $organization = Organization::findOrFail(1);

        $pdf = PDF::loadView('pdf.receipt', compact('transaction', 'organization'))->setPaper('a6')->setOrientation('potrait');;

        return $pdf->stream('receipt.pdf');


    }


    public function statement($id)
    {

        $account = Savingaccount::findOrFail($id);
        $interest=DB::table('savingproducts')->where('id', '=', $account->savingproduct_id)->pluck('Interest_Rate');

        $transactions = $account->transactions()->orderBy('date')->get();


        $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
        $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $credit - $debit;

        $organization = Organization::findOrFail(1);

        $pdf = PDF::loadView('pdf.statement', compact('transactions', 'organization','interest', 'account', 'balance'))->setPaper('a4')->setOrientation('potrait');;

        return $pdf->stream('statement.pdf');


    }


    public function import()
    {

        if (Input::hasFile('saving')) {

            $destination = public_path() . '/uploads/savings/';

            $filename = date('Y-m-d');

            $ext = Input::file('saving')->getClientOriginalExtension();
            $photo = $filename . '.csv';


            $file = Input::file('saving')->move($destination, $photo);

            //$file = public_path().'/uploads/savings/'.$filename;


            $row = 1;

            $saving = array();

            if (($handle = fopen(public_path() . '/uploads/savings/' . $photo, "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    echo '<pre>';

                    $saving[] = array('date' => $data[0], 'member' => $data[1], 'account' => $data[2], 'amount' => $data[3],'bank' => $data[4], 'pay_method' => $data[5]);
                }

                $i = 1;

                for ($i = 1; $i < count($saving); $i++) {

                    $member = $saving[$i]['member'];
                    $account = $saving[$i]['account'];
                    $amount = $saving[$i]['amount'];
                    $date = $saving[$i]['date'];
                      $bank = $saving[$i]['bank'];
                      $method = $saving[$i]['pay_method'];

                    $member_no = DB::table('members')->where('membership_no', '=', $member)->get();

                    if (empty($member_no)) {

                        return Redirect::to('import')->with('error', 'The member does not exist');
                    }

                    $account_no = DB::table('savingaccounts')->where('account_number', '=', $account)->get();


                    if (empty($account_no)) {

                        return Redirect::to('import')->with('error', 'The saving account does not exist');
                    }


                    Savingtransaction::importSavings($member_no, $date, $account_no, $amount,$bank,$method);
                }

                fclose($handle);
            }


        }

        return Redirect::to('/')->with('notice', 'Member savings successfully imported');


    }

    public function importSavings()
    {
        $validator = Validator::make([
            'file' => Input::file('file'),
            'extension' => strtolower(pathinfo(Input::file('file')->getClientOriginalName(), PATHINFO_EXTENSION))
        ], [
            'file' => 'required',
            //'extension' => 'required|in:xls,xlsx,csv'
            //upload csv files only
            'extension' => 'required|in:csv'

        ]);

        if ($validator->fails()) {
            return Redirect::back()->with('errors', $validator->messages());
        }

        $file = Input::file('file');
        $filename = str_random(16) . "." . $file->getClientOriginalExtension();
        $destination = public_path('/migrations/savings');
        $file->move($destination, $filename);

        $error_flag = false;

        Excel::filter('chunk')->selectSheetsByIndex(0)->load(public_path('migrations/savings/') . $filename)->chunk(250, function ($results) {
            foreach ($results as $result) {
                if ($result->account_number != null
                    && $result->date != null
                    && $result->amount != null
                    && $result->type != null
                    && $result->description != null) {
                    $account_number = trim(explode(':', $result->account_number)[1]);

                    $account = Savingaccount::where('account_number', $account_number)->first();
                    if ($account != null) {
                        $member_no = Member::where('id', $account->member_id)->get();
                        $account_no = Savingaccount::where('account_number', $account_number)->get();

                        Savingtransaction::importSavings($member_no, date('Y-m-d', strtotime($result->date)), $account_no, $result->amount, $result->description,$result->bank_ref,$result->transaction_method);
                    } else $error_flag = true;
                } else $error_flag = true;
            }
        });

        if ($error_flag) {
            $umessage = 'Successfully migrated. Some entries were ignored.';
        } else {
            $umessage = 'Successfully migrated.';
        }

        return Redirect::back()->with(compact('umessage'));
    }


    public function void($id)
    {

        Savingtransaction::destroy($id);

        return Redirect::back()->with('notice', ' transaction has been successfully voided');
    }


}
