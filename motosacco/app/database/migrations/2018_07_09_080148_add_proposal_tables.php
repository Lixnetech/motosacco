<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProposalTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', array('INTEREST', 'OTHER INCOME', 'EXPENDITURE'));
            $table->timestamps();
        });

        Schema::create('proposal_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proposal_category_id');
            $table->integer('year');
            $table->decimal('first_quarter');
            $table->decimal('second_quarter');
            $table->decimal('third_quarter');
            $table->decimal('fourth_quarter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proposal_categories');
        Schema::drop('proposal_entries');
    }

}
