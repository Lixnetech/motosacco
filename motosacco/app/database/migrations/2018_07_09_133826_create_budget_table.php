<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgetcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', array('INTEREST', 'OTHER INCOME', 'EXPENDITURE'));
            $table->timestamps();
        });
        Schema::create('budgetentries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budgetcategory_id');
            $table->integer('year');
            $table->decimal('amount');
            $table->decimal('first_quarter');
            $table->decimal('second_quarter');
            $table->decimal('third_quarter');
            $table->decimal('fourth_quarter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budgetcategories');
        Schema::drop('budgetentries');

    }

}
