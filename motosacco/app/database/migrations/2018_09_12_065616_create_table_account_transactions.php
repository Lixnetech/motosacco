<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccountTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_transactions', function(Blueprint $table){
			$table->increments('id');
			$table->date('transaction_date');
			$table->string('description');
			$table->integer('account_debited');
			$table->integer('account_credited');
			$table->integer('bank_transaction_id');
			$table->integer('bank_statement_id');
			$table->float('transaction_amount', 8,2);
			$table->string('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropTable('account_transactions');
	}

}
