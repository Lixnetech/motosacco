<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefinanceHistoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_refinance_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loanaccount_id');
            $table->date('date');
            $table->enum('type', array('INITIAL', 'REFINANCE'));
            $table->decimal('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('loan_refinance_history');
    }

}
