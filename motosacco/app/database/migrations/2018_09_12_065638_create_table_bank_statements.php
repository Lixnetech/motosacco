<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankStatements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_statements', function(Blueprint $table){
			$table->increments('id');
			$table->integer('bank_account_id')->unsigned();
			$table->float('bal_bd');
			$table->float('adj_bal_bd', 13,2)->nullable();
			$table->string('stmt_month');
			$table->boolean('is_reconciled')->default(false);
			$table->string('file_path')->nullable();
			$table->timestamps();

			$table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropTable('bank_statements');
	}

}
