<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStmtTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stmt_transactions', function(Blueprint $table){
			$table->increments('id');
			$table->integer('bank_statement_id')->unsigned();
			$table->date('transaction_date');
			$table->string('description');
			$table->string('ref_no')->nullable();
			$table->float('transaction_amnt');
			$table->string('check_no')->nullable();
			$table->string('status', 50);
			$table->string('type')->nullable();
			$table->timestamps();

			$table->foreign('bank_statement_id')->references('id')->on('bank_statements')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropTable('stmt_transactions');
	}

}
