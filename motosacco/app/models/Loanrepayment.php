<?php

class Loanrepayment extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];
	public function loanaccount(){

		return $this->belongsTo('Loanaccount');
	} 

	public static function getAmountPaid($loanaccount,$date=null){
		$date_disbursed=$loanaccount->date_disbursed;  
		if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
		if($date!=null){
			$paid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $date_disbursed)->where('date', '<=', $date)->where('description','=','loan repayment')->sum('amount');
		}else{
			$paid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $date_disbursed)->where('description','=','loan repayment')->sum('amount');
		}
			return $paid;
	}  
	 
	/*public static function getPrincipalPaid($loanaccount){
			$date_disbursed=$loanaccount->date_disbursed;
			if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $date_disbursed)->sum('principal_paid');
			return $paid;
	}*/
	public static function getPrincipalPaid($loanaccount,$date=null){
		$date_disbursed=$loanaccount->date_disbursed;  
		if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
		if($date!=null){
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)
                ->where('date', '>', $date_disbursed)->where('date', '<=', $date)
                ->sum('principal_paid');
		}else{
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)
                ->where('date', '>', $date_disbursed)->sum('principal_paid');
		}
		return $paid;
	}

	public static function getPrincipalPaidAt($loanaccount, $date = null){
		if($date == null)
			$date = date('Y-m-d');

		$from = date('Y-m-d', strtotime('+1 day', strtotime($loanaccount->date_disbursed)));
		if(strtotime($date) >= strtotime($loanaccount->date_disbursed)){
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->whereBetween('date', array($from, $date))->sum('principal_paid');
		}else{
			$paid = 0;
		}
		return $paid;
	}

	/*public static function getInterestPaid($loanaccount){
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->sum('interest_paid');
			return $paid;
	}*/
	public static function getInterestPaid($loanaccount,$date=null){
		$date_disbursed=$loanaccount->date_disbursed;  
		if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
		if($date!=null){
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>', $date_disbursed)->where('date', '<=', $date)->sum('interest_paid');
		}else{
			$paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>', $date_disbursed)->sum('interest_paid');
		} 
		return $paid; 
	} 
	public static function getInterestUnpaid($loanaccount){
		$supposed=Loantransaction::getAmountDueAt($loanaccount,date('Y-m-d'),'interest');
		$interestpaid = Loanrepayment::getInterestPaid($loanaccount);//DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
		$interestUnpaid=(float)$supposed-(float)$interestpaid; if($interestUnpaid<1){$interestUnpaid=0;}
		return $interestUnpaid;  
		//$interestpaid; //$supposed; //   
	}
	public static function repayLoan($data){

		$loanaccount_id = array_get($data, 'loanaccount_id');

		$loanaccount = Loanaccount::findorfail($loanaccount_id);

		$amount = array_get($data, 'amount');
		$date = array_get($data, 'date');
                $bank = array_get($data, 'bank_reference');

		$member = $loanaccount->member;


		$principal_due = Loantransaction::getPrincipalDue($loanaccount);
		$interest_due = Loantransaction::getInterestDue($loanaccount);
		$principal_bal =Loanaccount::getPrincipalBal($loanaccount);
		$total_due = $principal_due + $interest_due;

		$payamount = $amount;

        $chosen_date_date = date('Y-m-d', strtotime($date));
        $start_date = $loanaccount->repayment_start_date;
        $chosen_year = date('Y', strtotime($date));
        $start_year = date('Y', strtotime($start_date));
        $chosen_month = date('m', strtotime($date));
        $start_month = date('m', strtotime($start_date));
        $months = (($chosen_year - $start_year) * 12) + ($chosen_month - $start_month);
        $counter = Loantransaction::where('loanaccount_id', '=', $loanaccount->id)->count();
        //subtract current repayment month
        if ($loanaccount->loanguard_status == 1) {
            $counter += 1;
        }
        $balance = Loanaccount::getPrincipalBal($loanaccount);
        $rate = ($loanaccount->interest_rate) / 100;
        $principal_due = Loanaccount::getLoanAmount($loanaccount) / $loanaccount->repayment_duration;
        $category = "Cash";

        if (($counter < 3) && ($chosen_date_date > $start_date) && ($months > 0)) {
            $start_date = $loanaccount->date_disbursed;
            $dates = Loanrepayment::end_months($start_date, $months);
            foreach ($dates as $enddate) {
                $interest_supposed_to_pay = $balance * $rate;
                Loanrepayment::payPrincipal($loanaccount, $enddate, 0,$bank);
                Loanrepayment::payInterest($loanaccount, $enddate, 0,$bank);
                $total_supposed = $principal_due + $interest_supposed_to_pay;
                $amount_paid_month = 0;
                /*Record Arrears*/
                // $arrears = $total_supposed;
                // Loantransaction::repayLoan($loanaccount, $amount_paid_month, $enddate);
                /*Record Transaction for the arrears:  Debit*/
                /*$transaction = new Loantransaction;
                $transaction->loanaccount()->associate($loanaccount);
                $transaction->date = $enddate;
                $transaction->description = 'loan arrears';
                $transaction->amount = $interest_supposed_to_pay;
                $transaction->type = 'debit';*/
                // $transaction->arrears = 0;
                // $transaction->payment_via = $category;
                // $transaction->save();
                /*Looping through the days*/
                //$start_date= date('Y-m-d', strtotime($start_date.'+1 month'));


                $balance += $interest_supposed_to_pay;
            }
        } elseif ($counter > 3) {
            $trans = Loantransaction::where('loanaccount_id', '=', $loanaccount_id)->orderBy('date', 'DESC')->first();
            $last_date = $trans->date;
            $last_month = date('m', strtotime($last_date));
            $last_year = date('Y', strtotime($last_date));

            $months = (($chosen_year - $last_year) * 12) + ($chosen_month - $last_month);
            $months -= 1;
            if ($months > 0) {
                $dates = Loanrepayment::end_months($last_date, $months);
                foreach ($dates as $enddate) {
                    /*$last_date= date('Y-m-d', strtotime($last_date.'+1 month'));
                    //$last_month += 1;
                    //$last_date = $last_year. '-' . $last_month . '-' .'01';
                    //$number = date('t', strtotime($last_date));

                    $last_date = date('Y-m', strtotime($last_date)).'-'. date('t', strtotime($last_date));*/
                    $interest_supposed_to_pay = $balance * $rate;
                    Loanrepayment::payPrincipal($loanaccount, $enddate, 0,$bank);
                    Loanrepayment::payInterest($loanaccount, $enddate, 0,$bank);
                    $total_supposed = $principal_due + $interest_supposed_to_pay;
                    $amount_paid_month = 0;
                    /*Record Arrears*/
                    // $arrears = $total_supposed;
                    // Loantransaction::repayLoan($loanaccount, $amount_paid_month, $enddate);
                    /*Record Transaction for the arrears:  Debit*/
                    /*$transaction = new Loantransaction;
                    $transaction->loanaccount()->associate($loanaccount);
                    $transaction->date = $enddate;
                    $transaction->description = 'loan arrears';
                    $transaction->amount = $interest_supposed_to_pay;
                    $transaction->type = 'debit';*/
                    // $transaction->arrears = 0;
                    // $transaction->payment_via = $category;
                    // $transaction->save();
                    /*Looping through the days*/


                    $balance += $interest_supposed_to_pay;
                }


            }
        }

 	  if($payamount < $total_due){

			//pay interest first
			Loanrepayment::payInterest($loanaccount, $date, $interest_due,$bank);
			$payamount = $payamount - $interest_due;
			if($payamount > 0){
				Loanrepayment::payPrincipal($loanaccount, $date, $payamount,$bank);
			}
		}elseif($payamount >= $total_due){
			//pay interest first
			if($payamount >= $interest_due && $interest_due > 0){
				Loanrepayment::payInterest($loanaccount, $date, $interest_due,$bank);
				$payamount=$payamount-$interest_due;
			}elseif($payamount < $interest_due){
				Loanrepayment::payInterest($loanaccount, $date, $payamount,$bank);
				$payamount=0;
			}


			if ($payamount < $principal_bal && $payamount > 0){
				Loanrepayment::payPrincipal($loanaccount, $date, $payamount,$bank);
				$payamount=$payamount-$principal_bal;
			}elseif ($payamount>=$principal_bal){
				$overcharge=$payamount-$principal_bal;
				$payamount=$principal_bal;
				Loanrepayment::payPrincipal($loanaccount, $date, $payamount,$bank);
			        $data = array(
			'credit_account' =>'99' ,
			'debit_account' =>'6' ,
			'date' => $date,
			'amount' => $overcharge,
			'initiated_by' => 'system',
			'description' => 'loanovercharge',
			'bank_details' => $bank,
            'particulars_id' => '75',
            'narration' => $loanaccount->member->id

			);
                        	//$savingsaccount=Savingaccount::where('member_id',$member->id)->first();
			
                	//Savingtransaction::transact($date,$savingsaccount,$overcharge,'credit','loanovercharge', Confide::user()->username,$member);
		$journal = new Journal;
		$journal->journal_entry($data);
                   	}
	}
		/*
		do {

			if($payamount >= $principal_due ){

				Loanrepayment::payPrincipal($loanaccount, $date, $principal_due,$bank);
				$payamount = $payamount - $principal_due;


				if($payamount >= $interest_due ){

				Loanrepayment::payInterest($loanaccount, $date, $interest_due,$bank);
				$payamount = $payamount - $interest_due;

				}

				elseif($payamount > 0 && $payamount < $interest_due) {

					Loanrepayment::payInterest($loanaccount, $date, $payamount,$bank);
					$payamount = $payamount - $payamount;
				}

			}

			elseif(($payamount > 0) and ($payamount < $principal_due) ) {

				Loanrepayment::payInterest($loanaccount, $date, $interest_due,$bank);
				$payamount = $payamount - $interest_due;


				if($payamount > 0) {

					Loanrepayment::payPrincipal($loanaccount, $date, $payamount,$bank);
					$payamount = $payamount - $payamount;

				}
			}
		} while($payamount > 0);

	*/
		Loantransaction::repayLoan($loanaccount, $amount, $date,$bank);
	}
/*get loan overcharge amount for each member*/
	public static function loanOverchargemem($id){
		$member=Member::find($id);
		
		$total_loanovercharge =DB::table('journals')
				->join('narration','journals.trans_no','=','narration.trans_no')
				->where('narration.member_id','=',$member->id)
				->where('journals.account_id','=',99)
				->sum('journals.amount');
				return $total_loanovercharge;
	}

    public static function end_months($start_date, $months)
    {
        $dates = array();
        foreach (range(1, $months) as $month) {
            $start_date = date('Y', strtotime($start_date)) . "-" . date('m', strtotime($start_date)) . "-01";
            $start_date = date('Y-m-d', strtotime('+1 month', strtotime($start_date)));
            array_push($dates, date('Y-m-t', strtotime($start_date)));
        }
        return $dates;
    }

	public static function offsetLoan($data){
		$loanaccount_id = array_get($data, 'loanaccount_id');
		$loanaccount = Loanaccount::findorfail($loanaccount_id);
		$amount = array_get($data, 'amount');
		$date = array_get($data, 'date');
                $bank = array_get($data, 'bank_reference');

		$principal_bal = Loanaccount::getPrincipalBal($loanaccount);
		$interest_bal = Loanaccount::getInterestAmount($loanaccount);
		//pay principal
 		Loanrepayment::payPrincipal($loanaccount, $date, $principal_bal,$bank);
 		//pay interest
 		Loanrepayment::payInterest($loanaccount, $date, $interest_bal,$bank);
		Loantransaction::repayLoan($loanaccount, $amount, $date,$bank);
	}

	public static function payPrincipal($loanaccount, $date, $principal_due,$bank){
		$repayment = new Loanrepayment;
		$repayment->loanaccount()->associate($loanaccount);
		$repayment->date = $date;
		$repayment->principal_paid = $principal_due;
                $repayment->bank_repdetails = $bank;
		$repayment->save();
		$account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'principal_repayment');
		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $principal_due,
			'initiated_by' => 'system',
			'description' => 'principal repayment',
                         'bank_details' => $bank,

            'particulars_id' => '26',
            'narration' => $loanaccount->member->id
			);
		$journal = new Journal;
		$journal->journal_entry($data);
	}

	public static function clearLoan($loanaccount,$method=NULL){
		$principal_bal = Loanaccount::getPrincipalBal($loanaccount); 
		$interest_bal = Loanaccount::getInterestBal($loanaccount);
		$total_bal=Loantransaction::getLoanBalance($loanaccount); $date=date("Y-m-h");

		$loanbal=Loantransaction::getLoanBalance($loanaccount); 
		/*$extra=Loantransaction::getLoanExtra($loanaccount);
		if($extra=='arrears'){
			$arrears=Loantransaction::getExtraAmount($loanaccount,'arrears');
			$overpayment=$arrears;  $arrears=0; 
		}else if($extra=='over_payment'){
			$overpayments=Loantransaction::getExtraAmount($loanaccount,'overpayments');
			$arrears=$overpayments; $overpayment=0; 
		}else{$overpayment=0; $arrears=0;} */
     
		Loanrepayment::payPrincipal($loanaccount, $date, $principal_bal);
 		//pay interest
 		Loanrepayment::payInterest($loanaccount, $date, $interest_bal);   
		Loantransaction::repayLoan($loanaccount,$total_bal,$date,"none"); 
		$l_product=Loanproduct::find($loanaccount->loanproduct_id);
		if($method=="cash" || $method=="Cash"){$dbAcc=1;}else if($method=="bank"){
			$dbAcc=6; 
		}else{$dbAcc=1;}
		$data = array( 
			'credit_account' =>$l_product->account_id, 
			'debit_account' =>$dbAcc,  
			'date' => date('Y-m-d'),
			'amount' => $total_bal,
			'initiated_by' => 'system',
			'description' => 'loanRefund',
			'particulars_id' => '0',
			'narration' => $loanaccount->member_id
		);
		
		$journal = new Journal;
		$journal->journal_entry($data); 
	}

	public static function payInterest($loanaccount, $date, $interest_due,$bank){
		$repayment = new Loanrepayment;
		$repayment->loanaccount()->associate($loanaccount);
		$repayment->date = $date;
		$repayment->interest_paid = $interest_due;
		$repayment->save();
		$account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'interest_repayment');
		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $interest_due,
			'initiated_by' => 'system',
			'description' => 'interest repayment',
                         'bank_details' => $bank,

            'particulars_id' => '1',
            'narration' => $loanaccount->member->id
			);
		$journal = new Journal;
		$journal->journal_entry($data);
	}
}
