<?php 

class Tbopeningbalance extends Eloquent{

	protected $table = 'openingbalances';

	/**
	 * Link with accounts
	 */
	public function account(){
		return $this->belongsTo('accounts');
	}

	public static function getTrialBalance($account, $openingbalance, $from, $to)
    { 
      
        $balance = 0;
        
        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'credit')->whereBetween('date', array($from, $to))->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->whereBetween('date',  array($from, $to))->sum('amount');

        if ($account->category == 'ASSET') {

            $balance = $debit - $credit;
            $total=$openingbalance+$balance;


        }

        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             $total=$openingbalance+$balance;

        }

        if ($account->category == 'LIABILITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EQUITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
           $total=$openingbalance+$balance;

        }


        return $total;


    }
public static function getTrialBalanceAsAt($account, $openingbalance,$date)
    { 
      
        $balance = 0;

       $odate=date('Y-m-d',strtotime($date));
        $opendate=date('Y-m-d',strtotime('2020-01-01'));
        
        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('date', '>=',$opendate )->where('type', '=', 'credit')->where('date','<=',$odate)->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->where('date', '>=', $opendate)->where('date','<=',$odate)->sum('amount');

        if ($account->category == 'ASSET') {

            $balance = $debit - $credit;
            $total=$openingbalance+$balance;


        }

        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             $total=$openingbalance+$balance;

        }

        if ($account->category == 'LIABILITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EQUITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
           $total=$openingbalance+$balance;

        }


        return $total;


    }
public static function getTrialBalancepreviousmonth($account, $openingbalance,$date)
    { 
      
        $balance = 0;

       $odate=date('Y-m-d',strtotime($date));
         $month=date('m',strtotime($date));

          $year=date('Y',strtotime($date));

        $opendate=date('Y-m-d',strtotime('2020-01-01'));

        
        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('date', '>=',$opendate )->where('type', '=', 'credit')->whereMonth('date','<=',$month)->whereYear('date','=',$year)->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->where('date', '>=', $opendate)->whereMonth('date','<=',$month)->whereYear('date','=',$year)->sum('amount');

        if ($account->category == 'ASSET') {

            $balance = $debit - $credit;
            $total=$openingbalance+$balance;


        }

        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             $total=$openingbalance+$balance;

        }

        if ($account->category == 'LIABILITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EQUITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
           $total=$openingbalance+$balance;

        }


        return $total;


    }

public static function incomExpenseBal($account, $date)
    { 
      
        $balance = 0;
         
       $odate=date('Y',strtotime($date));
                
        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'credit')->whereYear('date','=',$odate)->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->whereYear('date','=',$odate)->sum('amount');

        
        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             
        }

        
        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
            
           
        }


        return $balance;


    }

public static function getRetainedEarnings($account, $date)
    { 
      
      $yeardate=date('Y',strtotime($date));

$income = DB::table('accounts')
                ->join('openingbalances', 'accounts.id', '=', 'openingbalances.account_id')
                ->where('accounts.category','=','INCOME')
                 ->where('opening_bal_for','=',$yeardate)
                ->select('account_balance')
                ->sum('account_balance');
$expense = DB::table('accounts')
                ->join('openingbalances', 'accounts.id', '=', 'openingbalances.account_id')
                ->where('accounts.category','=','EXPENSE')
                ->where('opening_bal_for','=',$yeardate)
                ->select('account_balance')
                ->sum('account_balance');
            $retained_amount=$income-$expense;




        $balance = 0;
              
       $odate=date('Y',strtotime($date));
                
        $amount = DB::table('openingbalances')->where('account_id', '=', $account->id)->whereYear('date','=',$odate)->pluck('account_balance');
        
        
        
            $balance = $amount+$retained_amount;
             
        

        return $balance;


    }


}
