<?php

class Loantransaction extends \Eloquent {

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = [];

    public function loanaccount(){

        return $this->belongsTo('Loanaccount');
    }

    public static function getLoanBalance($loanaccount){
        $principal_bal = Loanaccount::getPrincipalBal($loanaccount);
        if(!empty($principal_bal)){
            $rate = $loanaccount->interest_rate/100;
            $interest_due = $principal_bal * $rate;
            $balance = $principal_bal + $interest_due;
            return $balance;
        }else{
            return 0;
        }
    }
    public static function getLoanBalanceAt($loanaccount, $date){
        $loanAmount = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;
        $principal_paid = Loanrepayment::getPrincipalPaidAt($loanaccount, $date);
        $rate = $loanaccount->interest_rate/100;
        $principal_bal = $loanAmount - $principal_paid;
        $interest_due = $principal_bal * $rate;
        $balance = $principal_bal + $interest_due;

        $arrearDate = date('Y-m-d', strtotime('+1 day', strtotime($loanaccount->date_disbursed)));
        $arrears = Loantransaction::where('loanaccount_id', $loanaccount->id)
            ->where('type', 'debit')
            ->whereBetween('date', array($arrearDate, $date))
            ->sum('amount');

        return $principal_bal;//+$arrears;
    }

    public static function getMemberLoanBalance($member_id,$what=NULL){
        $loanaccounts = DB::table('loanaccounts')->where('member_id', '=', $member_id)->where('is_disbursed','=',1)->get();
        $loanBalances=0; $withGuaranteeLoanBal=0;
        foreach($loanaccounts as $loanaccount){
            $loanBalance=Loantransaction::getLoanBalance($loanaccount);
            $guaranteed=Loanaccount::guaranteedAmount($loanaccount);
            if($loanBalance<1){$loanBalance=0;}
            $withGuaranteeBal=(float)$loanBalance-(float)$guaranteed; if($withGuaranteeBal<1){$withGuaranteeBal=0;}
            $loanBalances+=$loanBalance; $withGuaranteeLoanBal+=$withGuaranteeBal;
        }
        if($what=='withGuaranteeBal'){return $withGuaranteeLoanBal;}else{return $loanBalances;}
    }
    public static function getLoanBalanceNoInterest($loanaccount){
        $amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
        $amountgiven=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
        $totaltobepaid=(float)$amountgiven;
        return (float)$totaltobepaid-(float)$amountpaid;
    }

    public static function getRemainingPeriod($loanaccount){

        $paid_periods = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)
            ->where('description', '=', 'loan repayment')
            ->where('date', '>', $loanaccount->date_disbursed)
            ->count();

        $remaining_period = $loanaccount->repayment_duration - $paid_periods;

        return $remaining_period;
    }

    public static function getPrincipalDue($loanaccount){
        /*$remaining_period = Loantransaction::getRemainingPeriod($loanaccount);
        $principal_paid = Loanrepayment::getPrincipalPaid($loanaccount);
        $principal_balance = $loanaccount->amount_disbursed - $principal_paid;
        if($principal_balance > 0 && $remaining_period > 0){
            $principal_due = $principal_balance/$remaining_period;
        }else{
            $principal_due = Loanaccount::getPrincipalBal($loanaccount);
        }*/
        $period=$loanaccount->period;
        $principal_due = ($loanaccount->amount_disbursed+$loanaccount->top_up_amount) / $period;
        return $principal_due;
    }

    public static function getInterestDue($loanaccount){
        $principal_bal = Loanaccount::getPrincipalBal($loanaccount);
        $rate = $loanaccount->interest_rate/100;
        $interest_due = $principal_bal * $rate;
        return $interest_due;
    }

    public static function getAmountDueAt($loanaccount,$ldate,$what){#changeo
        $fdate=$loanaccount->date_disbursed;
        if(isset($fdate)){
            $months=Loantransaction::monthsDiff($fdate,$ldate); $interest=0;
            $balance =$loanaccount->amount_disbursed+$loanaccount->top_up_amount; $disbursed=$balance; #Loanaccount::getLoanBalNoInterest($loanaccount);
            $installment=Loantransaction::getInstallment($loanaccount); $rate=Loantransaction::getrate($loanaccount);
            $principal_due=Loantransaction::getPrincipalDue($loanaccount); 	$interest_amount= Loanaccount::getTotalInterest($loanaccount);
            $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
            $formula=$loanproduct->formula;  $period=$loanaccount->period; $interest=0;
            if((int)$months<(int)$period){
                for($i = 1; $i <=(int)$months; $i++){
                    if ($loanaccount->loanproduct->formula == 'SL') {
                        $interest_due = $interest_amount / $period;
                        $interest+=$interest_due;
                    }
                    if ($loanaccount->loanproduct->formula == 'RB') {
                        $interest_due = ($disbursed - (($i - 1) * $principal_due)) * ($loanaccount->interest_rate / 100);
                        $interest+=$interest_due;
                    }
                }
                $installments=(float)$installment*(float)$months;
            }else{
                $interest= Loanaccount::getTotalInterest($loanaccount);
                $installments=(int)$disbursed+(float)$interest;
            }
        }else{
            $interest=0;$installments=0;
        }
        if($what=='installments'){return $installments;}else if($what=='interest'){return $interest;}
    }

    public static function getAmountUnpaid($loanaccount){
        $supposed=Loantransaction::getAmountDueAt($loanaccount,date('Y-m-d'),'installments');
        $amountpaid = Loanrepayment::getAmountPaid($loanaccount);//DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
        $amountUnpaid=(float)$supposed-(float)$amountpaid; if($amountUnpaid<1){$amountUnpaid=0;}
        return $amountUnpaid;
        //$amountpaid; //$supposed;
    }

    public static function monthsDiff($fdate,$ldate){
        $fdate_split=explode('-',$fdate); $ldate_split=explode('-',$ldate);
        $fdate_year=(int)$fdate_split[0]; $fdate_month=(int)$fdate_split[1]; $fdate_days=(int)$fdate_split[2];
        $ldate_year=(int)$ldate_split[0]; $ldate_month=(int)$ldate_split[1]; $ldate_days=(int)$ldate_split[2];
        if($fdate_year===$ldate_year){
            $months=$ldate_month-$fdate_month;
        }else{
            $yrDiff=$ldate_year-$fdate_year; $otherMonths=($yrDiff-1)*12;
            $previous_months=12-$fdate_month; $months2=$previous_months+$ldate_month;
            $months=$months2+$otherMonths;
        }
        $days=$ldate_days-$fdate_days;
        if($days<0){$months_diff=$months-1;}else{$months_diff=$months;}
        return (int)$months_diff;
    }
    //BIGCHANGE
    public static function tomonthlyRate($rate){
        $rate=$rate/100; $rate=$rate/12;
        return $rate;
    }
    public static function toannualRate($rate){
        $rate=$rate/100; $rate=$rate*12;
        return $rate;
    }
    public static function justRate($rate){
        $rate=$rate/100;
        return $rate;
    }
//match rate with frequency..frequency=the duration after which an installment is paid	
    public static function getrate($loanaccount){
        //getaccountdetails
        $frequency=$loanaccount->frequency; $rate=$loanaccount->interest_rate;
        $rate_type=$loanaccount->rate_type;
        $balance =Loanaccount::getPrincipalBal($loanaccount);
        //endgetaccountdetails
        if($frequency=='annually' && $rate_type!=='annually'){
            $rate=Loantransaction::toannualRate($rate);
        }else if($frequency=='monthly' && $rate_type!=='monthly'){
            $rate=Loantransaction::tomonthlyRate($rate);
        }else{$rate=Loantransaction::justRate($rate);}
        return (float)$rate;
    }


    public static function getInstallment($loanaccount,$what=null){
        //getaccountdetails
        $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
        $formula=$loanproduct->formula;
        $months=$loanaccount->period;
        $amount=$loanaccount->amount_disbursed+$loanaccount->top_up_amount; $interest=Loantransaction::getInterestDue($loanaccount);
        $amortization=$loanproduct->amortization; $total_amount=$amount+$interest;
        //endgetaccountdetails
        if($amortization=="EI" && $formula=="SL"){
            $period=$months; #no. of installments
            $eachpay=(float)$total_amount/(float)$months;
        }else if($amortization=="EP" && $formula=="SL"){
            $principal=Loantransaction::getPrincipalDue($loanaccount);
            $period=$months; #no. of installments
            $eachpay=(float)$principal/(float)$interest;
        }else{
            $rate=Loantransaction::getrate($loanaccount);
            $r1=$rate+1; $r2=pow($r1,-$months); $r3=1-$r2;
            $r4=$r3/$rate; $period=$r4; #no. of installments
            $eachpay=$amount/$r4; #installment amount
        }
        if($what=='period'){return $period;}else{return $eachpay;}
    }
    /*public static function getInterestDue($loanaccount){
        //getaccountdetails
        $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
        $formula=$loanproduct->formula; $months=$loanaccount->period;
        $rate=Loantransaction::getrate($loanaccount);
        $balance =Loanaccount::getLoanBalNoInterest($loanaccount); //$int_bal=Loanaccount::getInterestBal($loanaccount);
        //endgetaccountdetails
        //if($int_bal>0){$interest=$balance*$rate;}else{
        //	$interest=0;
        //}
        $interest=$balance*$rate;
        return $interest;
    }

    public static function getPrincipalDue($loanaccount){
        $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
        $formula=$loanproduct->formula; $months=(int)$loanaccount->period;
        $rate=Loantransaction::getrate($loanaccount);
        $amortization=$loanproduct->amortization;
        $amount_given=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
        if($amortization=="EP" && $formula=="SL"){
            $principal=$amount_given/$months;
        }else{
            $installment=Loantransaction::getInstallment($loanaccount,'0');
            $interest=Loantransaction::getInterestDue($loanaccount);
            //check if a loan is fully paid
            if($interest>0){$principal=$installment-$interest;}else{
                $principal=0;
            }
        }
        return $principal;
    }*/

    //ENDBIGCHANGE
    public static function repayLoan($loanaccount, $amount, $date,$bank){
        $transaction = new Loantransaction;
        $transaction->loanaccount()->associate($loanaccount);
        $transaction->date = $date;
        $transaction->description = 'loan repayment';
        $transaction->amount = $amount;
        $transaction->bank_ldetails = $bank;
        $transaction->type = 'credit';
        $transaction->save();
        Audit::logAudit($date, Confide::user()->username, 'loan repayment', 'Loans', $amount);
    }

    public static function disburseLoan($loanaccount, $amount, $date,$bank){
        $transaction = new Loantransaction;
        $transaction->loanaccount()->associate($loanaccount);
        $transaction->date = $date;
        $transaction->description = 'loan disbursement';
        $transaction->amount = $amount;
        $transaction->bank_ldetails = $bank;
        $transaction->type = 'debit';
        $transaction->save();


        $account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

        $data = array(
            'credit_account' =>$account['credit'] ,
            'debit_account' =>$account['debit'] ,
            'date' => $date,
            'amount' => $loanaccount->amount_disbursed,
            'initiated_by' => 'system',
            'description' => 'loan disbursement',
            'bank_details'=>$bank,

            'particulars_id' => '26',
            'narration' => $loanaccount->member->id
        );


        $journal = new Journal;


        $journal->journal_entry($data);

        Audit::logAudit($date, Confide::user()->username, 'loan disbursement', 'Loans', $amount);

    }

    public static function refinanceLoan($loanaccount, $amount, $date,$bank) {
        $transaction = new Loantransaction;
        $transaction->loanaccount()->associate($loanaccount);
        $transaction->date = $date;
        $transaction->description = 'loan refinance';
        $transaction->amount = $amount;
        $transaction->bank_ldetails = $bank;
        $transaction->type = 'debit';
        $transaction->save();


        $account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

        $data = array(
            'credit_account' =>$account['credit'] ,
            'debit_account' =>$account['debit'] ,
            'date' => $date,
            'amount' => $loanaccount->amount_disbursed,
            'initiated_by' => 'system',
            'description' => 'loan refinance',
            'bank_details'=>$bank,
            'particulars_id' => '26',
            'narration' => $loanaccount->member->id
        );


        $journal = new Journal;


        $journal->journal_entry($data);

        Audit::logAudit($date, Confide::user()->username, 'loan refinance', 'Loans', $amount);
    }

    public static function topupLoan($loanaccount, $amount,$date,$bank){

        $transaction = new Loantransaction;

        $transaction->loanaccount()->associate($loanaccount);
        $transaction->date = $date;
        $transaction->description = 'loan top up';
        $transaction->amount = $amount;
        $transaction->bank_ldetails = $bank;
        $transaction->type = 'debit';
        $transaction->save();


        $account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

        $data = array(
            'credit_account' =>$account['credit'] ,
            'debit_account' =>$account['debit'] ,
            'date' => $date,
            'amount' => $loanaccount->top_up_amount,
            'initiated_by' => 'system',
            'description' => 'loan top up',
            'bank_details'=>$bank,
            'particulars_id' => 18,
            'narration' => $loanaccount->member->id
        );


        $journal = new Journal;


        $journal->journal_entry($data);

        Audit::logAudit($date, Confide::user()->username, 'loan to up', 'Loans', $amount);



    }




}
