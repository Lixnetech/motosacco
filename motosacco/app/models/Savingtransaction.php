<?php

class Savingtransaction extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $guarded = [];


	public function savingaccount(){

		return $this->belongsTo('Savingaccount');
	}


	public static function getWithdrawalCharge($savingaccount){
		$chargeamount = 0;

		foreach ($savingaccount->savingproduct->charges as $charge) {

			if($charge->payment_method == 'withdrawal'){

				$chargeamount = $chargeamount + $charge->amount;

			}
			
		}

		return $chargeamount;
	}



	public static function withdrawalCharges($savingaccount, $date, $transAmount){

		foreach($savingaccount->savingproduct->charges as $charge){

			if($charge->payment_method == 'withdrawal'){


					if($charge->calculation_method == 'percent'){
						$amount = ($charge->amount/ 100) * $transAmount;
					}

					if($charge->calculation_method == 'flat'){
						$amount = $charge->amount;
					}



					$savingtransaction = new Savingtransaction;

					$savingtransaction->date = $date;
					$savingtransaction->savingaccount()->associate($savingaccount);
					$savingtransaction->amount = $amount;
					$savingtransaction->type = 'debit';
					$savingtransaction->description = 'withdrawal charge';
					$savingtransaction->save();


				foreach($savingaccount->savingproduct->savingpostings as $posting){

					if($posting->transaction == 'charge'){

						$debit_account = $posting->debit_account;
						$credit_account = $posting->credit_account;
						$data = array(
						'credit_account' => $credit_account,
						'debit_account' => $debit_account,
						'date' => $date,
						'amount' => $amount,
						'initiated_by' => 'system',
						'description' => 'cash withdrawal'
					);
					$journal = new Journal;
					$journal->journal_entry($data);
					}
				}
			}
		}
	}



	public static function importSavings($member, $date, $savingaccount, $amount, $description,$bank,$method){

		
		
		
		$member = Member::find($member[0]->id);
		$savingaccount = Savingaccount::find($savingaccount[0]->id);
		


		
		//check if account and member exists



		$savingtransaction = new Savingtransaction;

		$savingtransaction->date = $date;
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->amount = $amount;
		$savingtransaction->type = 'credit';
		$savingtransaction->description = $description;
		$savingtransaction->payment_method =$method;
                 $savingtransaction->bank_sadetails = $bank;
		$savingtransaction->transacted_by = $member->name;
		$savingtransaction->save();


		foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($method=='cash' && $posting->transaction == 'deposit_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
                             elseif( $method =='bank' && $posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}

			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $amount,
				'initiated_by' => 'system',
				'description' => $description,
                                 'bank_details' => $bank,

                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, 'Savings imported', 'Savings', $amount);

			

	}



	public static function creditAccounts($data){





		$savingaccount = Savingaccount::findOrFail(array_get($data, 'account_id'));

		$savingtransaction = new Savingtransaction;

		$savingtransaction->date = array_get($data,'date');
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->amount = array_get($data,'amount');
		$savingtransaction->type = array_get($data,'type');
		$savingtransaction->description = 'savings deposit';
		$savingtransaction->payment_method = array_get($data,'pay_method');
        $savingtransaction->bank_sadetails = array_get($data,'bank_details');
		
		$savingtransaction->save();


	
		

		


		// deposit
		if((array_get($data,'type') == 'credit')&&(array_get($data,'pay_method') == 'cash')){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}
                      }
                      elseif((array_get($data,'type') == 'credit')&&(array_get($data,'pay_method') == 'bank')){
                              foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
 
                               }
                               }
                          


			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => array_get($data, 'date'),
				'amount' => array_get($data,'amount'),
				'initiated_by' => 'system',
				'description' => 'cash deposit',
                                 'bank_details' => array_get($data,'bank_details')				
                                    );


			$journal = new Journal;


			$journal->journal_entry($data);

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, 'savings deposit', 'Savings', array_get($data,'amount'));
			
		}

	





	public static function transact($date, $savingaccount, $amount, $type, $description, $transacted_by, $member,$bank,$method){

		$savingtransaction = new Savingtransaction;
		if($bank==NULL){$bank="none";}	
		$savingtransaction->date = $date;
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->amount = $amount;
		$savingtransaction->type = $type;  
		$savingtransaction->description = $description; 
		if($method=='cash'){
         $savingtransaction->payment_method= 'cash';
				} 
	   else{
         $savingtransaction->payment_method= 'bank';
				}

                $savingtransaction->bank_sadetails = $bank;
 
		$savingtransaction->transacted_by = $transacted_by;
		$savingtransaction->save();


	
		// bank withdrawal 

		if($type == 'debit' && $method=='bank'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'withdrawal'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}

				
			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $amount,
				'initiated_by' => 'system',
				'description' => $description,
                                'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have withdrawn ksh ".$amount." from saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";


			Savingtransaction::withdrawalCharges($savingaccount, $date, $amount);

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, $description, 'Savings', $amount);

		}
		//cash withdrawal

		if($type == 'debit' && $method=='cash'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'withdrawal_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}

				
			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $amount,
				'initiated_by' => 'system',
				'description' => $description,
                                'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have withdrawn ksh ".$amount." from saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";


			Savingtransaction::withdrawalCharges($savingaccount, $date, $amount);

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, $description, 'Savings', $amount);

		}


		// bank deposit
		if($type == 'credit' && $method=='bank'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $amount,
				'initiated_by' => 'system',
				'description' => $description,
                               'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have deposited ksh ".$amount." to saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, $description, 'Savings', $amount);
			
		}
       
       // cash deposit
		if($type == 'credit' && $method=='cash' ){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $amount,
				'initiated_by' => 'system',
				'description' => $description,
                               'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have deposited ksh ".$amount." to saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";

			Audit::logAudit(date('Y-m-d'), Confide::user()->username, $description, 'Savings', $amount);
			
		}
       // include(app_path() . '/views/AfricasTalkingGateway.php');
        // Specify your authentication credentials
        $username   = "lixnet";
        $apikey     = "a8d19ab5cfe8409bf737a4ef53852ab515560e31fcac077c3e6bb579cc2681e6";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $member->phone;
        // And of course we want our recipients to know what we really do
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message);
            $thisMonth=date('Y-m',time());
            $smsLogs=Smslog::where('date','like',$thisMonth.'%')
            		->where('user',$member->id)->first();
            if(sizeof($smsLogs) >=1){
            	//update sms logs
            	$smsLogs->monthlySmsCount +=1;
            	$smsLogs->update();
            }else{
            	//insert to sms logs
            	$newSms=new Smslog();
            	$newSms->user=$member->id;
            	$newSms->monthlySmsCount=1;
            	$newSms->date=date('Y-m-d',time());
            	$newSms->charged=0;
            	$newSms->save();
            }
            /*foreach($results as $result) {
                // status is either "Success" or "error message"
                echo " Number: " .$result->number;
                echo " Status: " .$result->status;
                //echo " StatusCode: " .$result->statusCode;
                echo " MessageId: " .$result->messageId;
                echo " Cost: "   .$result->cost."\n";
            }*/
        }
        catch ( AfricasTalkingGatewayException $e )
        {
            echo "Encountered an error while sending: ".$e->getMessage();
        }

	}




	public static function trasactionExists($date, $savingaccount){

		$count = DB::table('savingtransactions')->where('date', '=', $date)->where('savingaccount_id', '=', $savingaccount->id)->count();

		if($count >= 1){

			return true;
		} else {

			return false;
		}
	}




	
}