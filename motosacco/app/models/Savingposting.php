<?php

class Savingposting extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];



	public function savingproduct(){

		return $this->belongsTo('Savingproduct');
	}

	public function create_post_rules($product, $fee_income_acc, $saving_control_acc, $cash_account,$bank_account){

			//create posting rule for bank deposit transaction
                        if(!empty($bank_account))
                        {
			$posting = Savingposting::where('transaction', 'deposit')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}



			$posting->transaction = 'deposit';
			$posting->debit_account = $bank_account;
			$posting->credit_account = $saving_control_acc;
			$posting->savingproduct()->associate($product);
			$posting->save();


			//create posting rule for withdrawal transaction

			$posting = Savingposting::where('transaction', 'withdrawal')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}


			$posting->transaction = 'withdrawal';
			$posting->debit_account = $saving_control_acc;
			$posting->credit_account = $bank_account;
			$posting->savingproduct()->associate($product);
			$posting->save();



			//create posting rule for charge transaction
			$posting = Savingposting::where('transaction', 'charge')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}


			$posting->transaction = 'charge';
			$posting->debit_account = $bank_account;
			$posting->credit_account = $fee_income_acc;
			$posting->savingproduct()->associate($product);
			$posting->save();





	       }

//create posting rule for cash deposit transaction
                        if(!empty($cash_account))
                        {
			$posting = Savingposting::where('transaction', 'deposit_cash')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}



			$posting->transaction = 'deposit_cash';
			$posting->debit_account = $cash_account;
			$posting->credit_account = $saving_control_acc;
			$posting->savingproduct()->associate($product);
			$posting->save();


			//create posting rule for withdrawal transaction

			$posting = Savingposting::where('transaction', 'withdrawal_cash')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}


			$posting->transaction = 'withdrawal_cash';
			$posting->debit_account = $saving_control_acc;
			$posting->credit_account = $cash_account;
			$posting->savingproduct()->associate($product);
			$posting->save();



			//create posting rule for charge transaction
			$posting = Savingposting::where('transaction', 'charge_cash')->where('savingproduct_id', '=', $product->id)->first();

			if(empty($posting)){
				$posting = new Savingposting;
			}


			$posting->transaction = 'charge_cash';
			$posting->debit_account = $cash_account;
			$posting->credit_account = $fee_income_acc;
			$posting->savingproduct()->associate($product);
			$posting->save();





	       }
}
}
