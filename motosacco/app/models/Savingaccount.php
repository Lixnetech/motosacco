<?php

class Savingaccount extends \Eloquent
{

    // Add your validation rules here
    public static $rules = [
        'account_number' => 'unique:account_number'
    ]; 

    // Don't forget to fill this array
    protected $fillable = [];


    public function member()
    {

        return $this->belongsTo('Member');
    }


    public function savingproduct()
    {

        return $this->belongsTo('Savingproduct');
    }


    public function transactions()
    {

        return $this->hasMany('Savingtransaction');
    }


    public static function getLastAmount($savingaccount)
    {

        $saving = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->OrderBy('date', 'desc')->pluck('amount');


        if ($saving) {
            return $saving;
        } else {
            return 0;
        }


    }


    public static function getAccountBalance($savingaccount)
    {
        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->sum('amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $deposits - $withdrawals; if($balance<0){$balance=0;}

        return $balance; 
    }

    public static function getSavingsBetween($savingaccount, $from, $to)
    {

        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->whereBetween('date', array($from, $to))->sum('amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->whereBetween('date', array($from, $to))->sum('amount');

        $balance = $deposits - $withdrawals;

        return $balance;
    }

    public static function getSavingsAsAt($savingaccount, $date)
    {

        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->where('date', '<=', $date)->sum('amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->where('date', '<=', $date)->sum('amount');

        $balance = $deposits - $withdrawals;

        return $balance;
    }

    public static function getUserSavingsBalance($member)
    {
        $savingaccounts=DB::table('savingaccounts')->where('member_id', '=', $member)->get();
        $scount=DB::table('savingaccounts')->where('member_id', '=', $member)->count();
        if($scount>0){
                $samount=0;
                foreach($savingaccounts as $savingaccount){
                    $accountbal=Savingaccount::getAccountBalance($savingaccount);
                    $samount+=(int)$accountbal;
                }
                return $samount;
        }else{return 0;}
    }
    public static function withdrawSavings($member_id,$amount,$method=NULL)
    {   $date=date('Y-m-d'); 
        $userSavings=Savingaccount::getUserSavingsBalance($member_id);
        if($userSavings>0){
            $savingaccounts=DB::table('savingaccounts')->where('member_id', '=', $member_id)->get();
            $member=Member::findOrFail($member_id);
            $samount=0; $type="debit"; $description="Loan offset"; $transacted_by="xara"; $remamount=$amount;
            $scount=DB::table('savingaccounts')->where('member_id', '=', $member_id)->count(); $bank=NULL;
            if($scount>0){ 
                foreach($savingaccounts as $savingaccount){
                    $savinaccount=Savingaccount::findOrFail($savingaccount->id);
                    $accountbal=Savingaccount::getAccountBalance($savingaccount); 
                    if($savingaccount->savingproduct_id==4 & $accountbal>0){
                        if($accountbal>=$amount){$wdamount=$amount; $remamount=0;}else{$wdamount=$accountbal; $remamount-=$accountbal;}
                        Savingtransaction::transact($date, $savinaccount, $wdamount, $type, $description, $transacted_by, $member,$bank,$method);
                    }
                    if($remamount!=0){
                        if($accountbal>=$amount){$wdamount=$remamount; $remamount=0;}else{$wdamount=$accountbal; $remamount-=$accountbal;}
                        Savingtransaction::transact($date, $savinaccount, $wdamount, $type, $description, $transacted_by, $member,$bank,$method);
                    }  
                }  
            }  
        }
    }

    public static function getFinalSavingsBalance($member)
    {	$scount=DB::table('savingaccounts')->where('member_id', '=', $member)->count();
        if($scount>0){
            $rawsamount= Savingaccount::getUserSavingsBalance($member);
            $guaranteeamount=Loanaccount::amountGuarantee($member);
            $loanBalance=Loantransaction::getMemberLoanBalance($member);
            $finalsamount=(float)$rawsamount-((float)$guaranteeamount+(float)$loanBalance); 
            if($finalsamount<0){$finalamount=0;}
        }else{$finalsamount=0;}   
        return round($finalsamount,2);
    }

    public static function getDeductionAmount($account, $date)
    {

        $transactions = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->get();

        $amount = 0;
        foreach ($transactions as $transaction) {

            $period = date('m-Y', strtotime($transaction->date));

            if ($date == $period) {

                if ($transaction->type == 'credit') {
                    $amount = $transaction->amount;
                }

            }

        }


        return $amount;
    }


    public static function getAccountNumbersString()
    {
        $savingsAccounts = DB::table('savingaccounts')->limit(10)->get();
        $savingsAccountsArray = array();
        foreach ($savingsAccounts as $savingsAccount) {
            array_push($savingsAccountsArray, $savingsAccount->account_number);
        }
        return strtolower(trim(implode(",", $savingsAccountsArray)));
    }


}
