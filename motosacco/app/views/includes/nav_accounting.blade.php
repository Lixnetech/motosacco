 <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ URL::to('accounts') }}"><i class="fa fa-random"></i> Chart of Accounts</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('journals') }}"><i class="fa fa-barcode fa-fw"></i> Journal Entries</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('journals/create') }}"><i class="fa fa-pencil"></i> Add Journal Entry</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('particulars') }}"><i class="fa fa-money fa-fw"></i> Particulars</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('budget/expenses') }}"><i class="fa fa-money fa-fw"></i> Expenses</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('budget/incomes') }}"><i class="fa fa-money fa-fw"></i> Income</a>
                    </li>


                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bar-chart-o"></i> Budget
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="nav collapse" id="side-menu">
                            <li>
                                <a href="{{ URL::to('budget/projections') }}">
                                    Projections
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('budget/interests') }}">
                                    Interests
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('budget/income') }}">
                                    Other Income
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('budget/expenditure') }}">
                                    Expenditure
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="{{ URL::to('petty_cash') }}"><i class="fa fa-money fa-fw"></i>Petty Cash</a></li>

                    <li>
                          <a href="#"><i class="fa fa-money fa-fw"></i> Banking<i class="fa fa-caret-down fa-fw"></i></a>
                          <ul class="nav">
                              <li><a href="{{ URL::to('bankAccounts') }}"><i class="fa fa-archive fa-fw"></i> Bank Accounts</a></li>
                              <li><a href="{{ URL::to('bank/bank_deposit') }}"><i class="fa fa-book fa-fw"></i> Bank Deposit </a></li>
                              <!--<li><a href="{{ URL::to('bankReconciliation/report') }}"><i class="fa fa-file fa-fw"></i> Reconciliation Report</a></li>-->
                              <li><a href="{{ URL::to('bankReconciliation/receipt') }}"><i class="fa fa-file fa-fw"></i> Add Receipt</a></li>
                               <li><a href="{{ URL::to('bankReconciliation/transact') }}"><i class="fa fa-file fa-fw"></i>Disbursals and Payments</a></li>
                              <li><a href="{{ URL::to('transactions/'.date('m-Y')) }}"><i class="fa fa-book fa-fw"></i> Transactions </a></li>
                          </ul>  
                      </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->
