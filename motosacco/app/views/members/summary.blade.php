@extends('layouts.member')
@section('content')
    <br/>
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-info btn-sm " href="{{ URL::to('members/edit/'.$member->id)}}">update details</a>
            <a class="btn btn-success btn-sm" href="{{ URL::to('members/show/'.$member->id)}}">Manage</a>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <img src="{{  asset('public/uploads/photos/'.$member->photo)}}" width="150px" height="130px" alt="no photo"><br>
            <br>
            <img src="{{  asset('public/uploads/photos/'.$member->signature)}}" width="120px" height="50px"
                 alt="no signature">
        </div>
        <div class="col-lg-10">
            <table class="table table-bordered table-hover">
                <tr>
                    <td>Member Name</td>
                    <td>{{ $member->name}}</td>
                </tr>
                <tr>
                    <td>Membership Number</td>
                    <td>{{ $member->membership_no}}</td>
                </tr>
                @if($member->branch != null)
                    <tr>
                        <td>Branch</td>
                        <td>{{ $member->branch->name}}</td>
                    </tr>
                @endif
                @if($member->group != null)
                    <tr>
                        <td>Group</td>
                        <td>{{ $member->group->name}}</td>
                    </tr>
                @endif
                <tr>
                    <td>ID Number</td>
                    <td>{{ $member->id_number}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Summary</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover">
                <tr>
                    <td>Deposits</td>
                    <td>
                        @foreach($member->savingaccounts as $savingaccount)
                            <strong>{{ $savingaccount->savingproduct->name }}</strong>
                            : {{ asMoney(Savingaccount::getAccountBalance($savingaccount)) }}<br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Loans</td>
                    <td>
                        @foreach($member->loanaccounts as $loanaccount)
                          @if($loanaccount->is_disbursed == 1)
                            <strong>{{ $loanaccount->account_number }}
                                - {{ $loanaccount->loanproduct->name }}</strong>
                            : {{ asMoney(Loanaccount::getPrincipalBal($loanaccount)) }}<br>
                          @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Shares</td>
                    <td>{{ Shareaccount::getShares($member->shareaccount) }}</td>
                </tr>
            </table>
        </div>
    </div>
@stop
