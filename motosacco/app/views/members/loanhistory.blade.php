@extends('layouts.member')
@section('content')
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>
    <div class="container">
        <h3>{{ $member->name}}</h3>
        <small>Loan History</small><br><br>
        <a class="btn btn-success" target="_blank" href="{{ URL::to('members/loanhistory/'.$member->id.'/report') }}">
            <span class="fa fa-file-word-o" aria-hidden="true"></span> Report
        </a>
        <h1></h1>

        <div class="row">
            @foreach($member->loanaccounts as $loanaccount)
                <h4>{{ $loanaccount->account_number }}</h4>
                <div class="row">
                    <div class="col-lg-10">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td>Loan Type</td>
                                <td>{{ $loanaccount->loanproduct->name}}</td>
                            </tr>
                            <tr>
                                <td>Date Disbursed</td>
                                <td>{{ $loanaccount->date_disbursed}}</td>
                            </tr>
                            <tr>
                                <td>Amount Disbursed</td>
                                <td>{{ asMoney($loanaccount->amount_disbursed)}}</td>
                            </tr>
                            @if($loanaccount->is_top_up)
                                <tr>
                                    <td>Top Up Amount</td>
                                    <td>{{ asMoney($loanaccount->top_up_amount)}}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Cr</th>
                                <th>Dr</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($loanaccount->loantransactions as $loantransaction)
                                <tr>
                                    <td>{{ $loantransaction->date }}</td>
                                    <td>{{ $loantransaction->description }}</td>
                                    @if($loantransaction->type == 'credit')
                                        <td>{{ asMoney($loantransaction->amount) }}</td>
                                        <td>0.00</td>
                                    @else
                                        <td>0.00</td>
                                        <td>{{ asMoney($loantransaction->amount) }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
@endsection