@extends('layouts.member')
@section('content')
<style>
    .tophead{text-align:center;}
    .contentDiv{display:flex; flex-direction:row; justify-content:space-between; 
            width:100%; background-color:beige;
    }
    .savingsDiv,.loansDiv,.guaranteeDiv{width:33%; display:flex; flex-direction:column; justify-content:space-between; text-align:center;}
    .refundableDiv{width:100%; background-color:beige; text-align:center; margin-top:1px; padding:4px 0px;}
</style>
   
    <div class="row tophead">
            <h3>Money refund</h3>
    </div>
    @if (session::get('refundStatus'))
        <div class="alert alert-success">
            {{ session::get('refundStatus') }}
        </div>
    @endif
    @if (session::get('error'))
        <div class="alert alert-danger">
            {{ session::get('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            @if(Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
            <div class='contentDiv'>
                <div class='savingsDiv'>
                    <section>
                        <h4>Member Savings</h4>
                            <?php $total_sbal=0; ?>
                        @foreach($member_savings as $saving)
                            <?php $s_product=Savingproduct::findorfail($saving->savingproduct_id); 
                                $savings=Savingaccount::where("member_id",$member->id)->where("savingproduct_id",$saving->savingproduct_id)->get();
                                $savingBal=0; 
                                foreach($savings as $saving){
                                    $savBal=Savingaccount::getAccountBalance($saving); $savingBal+=$savBal;
                                }
                                $total_sbal+=$savingBal;
                            ?>
                            <div>
                                <span><u>{{$s_product->name}}</u></span><br>
                                <span>Balance :: {{$savingBal}}</span>
                            </div><br>
                        @endforeach
                    </section><br><br>
                    <section>
                        <div> Total savings :: {{$total_sbal}}  </div><br>
                    </section>
                </div>

                <div class='guaranteeDiv'>
                    <section>
                        <h4>Amount Guaranteed</h4>
                            <?php $total_gbal=0; ?>
                        @foreach($guarantees as $guarantee)
                            <?php 
                                $g_account=Loanaccount::findorfail($guarantee->loanaccount_id); $g_balance=$guarantee->amount;
                                $member=Member::findorfail($g_account->member_id); $l_product=Loanproduct::findorfail($g_account->loanproduct_id);
                                $total_gbal+=$g_balance;
                            ?>  
                            <div>
                                <span><u>{{$member->name}} || {{$l_product->name}}</u></span><br>
                                <span>Balance :: {{$g_balance}}</span>
                            </div><br>
                        @endforeach
                    </section><br><br>
                    <section>
                        <div> Total guaranteed amount :: {{$total_gbal}}  </div><br>
                    </section>
                </div>
                <div class='loansDiv'>
                    <section>
                        <h4>Member loans</h4>
                            <?php $total_lbal=0; $loansBal=0; ?>
                        @foreach($member_loans as $loan)
                            <?php $l_product=Loanproduct::findorfail($loan->loanproduct_id); 
                                $allloans=Loanaccount::where("member_id",$member->id)->where("loanproduct_id",$loan->loanproduct_id)->get();
                                foreach($allloans as $oneloan){
                                    $loanBal=Loantransaction::getLoanBalance($oneloan); 
                                    $loansBal+=$loanBal;
                                }
                            ?>
                            <div>
                                <span><u>{{$l_product->name}}</u></span><br>
                                <span>Balance :: {{$loansBal}}</span>
                            </div><br>
                        @endforeach 
                    </section><br><br>
                    <section>
                            <?php  $total_lbal+=$loansBal; ?>
                        <div> Total loan balance :: {{$total_lbal}}  </div><br>
                    </section>
                </div>
            </div>
                <div class='refundableDiv'>
                    <?php 
                        $refundable1=$total_sbal-($total_gbal+$total_lbal); 
                        if($refundable1<0){$refundable=0;}else{$refundable=$refundable1;}
                    ?> 
                    Refundable :: {{$refundable}}
                </div>
            
            <form method="POST" action="{{{ URL::to('savings/refund') }}}" accept-charset="UTF-8">
   
                <fieldset>
                    <div class="form-group">
                        <label for="username">Amount refundable <span style="color:red"></span> </label>
                        <input class="form-control" placeholder="" readonly type="text" name="refundAmount" id="name" value="{{$refundable}}">
                    </div>

                    <div class="form-group">
                        <label for="username">Form of payment <span style="color:red">*</span> </label>
                        <select class="form-control" name="form">
                                <option>Cash</option>
                                <option>Cheque</option>
                                <option>Mpesa</option>
                        </select>
                        <input type="hidden" name="refundable" value="{{$refundable1}}">
                        <input type="hidden" name="member_id" value="{{$member->id}}">
                    </div>
                    
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Refund Member</button>
                    </div>
                </fieldset>

            </form>

        </div>
    </div>
@stop