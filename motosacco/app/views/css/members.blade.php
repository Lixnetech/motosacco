@extends('layouts.member')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>Members</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
            @if(Session::get('error'))
                <div class="alert alert-failure">{{ Session::get('error') }}</div>
            @endif
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#active-members" aria-controls="remittance" role="tab"
                           data-toggle="tab">Active Members</a>
                    </li>
                    <li role="presentation">
                        <a href="#inactive-members" aria-controls="profile" role="tab" data-toggle="tab">
                            Inactive Members</a>
                    </li>
                    <li role="presentation">
                        <a href="#left_members" aria-controls="profile" role="tab" data-toggle="tab">
                            Left Members</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="active-members">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="btn btn-info btn-sm" href="{{ URL::to('members/create') }}">New Member</a>
                            </div>
                            <div class="panel-body">
                                <table id="users"
                                       class="table table-condensed table-bordered table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        @if($member->is_active)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <?php /* @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                            @endif
                                                            @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                </li>
                                                            @endif */?>
                                                            <li>
                                                                <a data-toggle="modal" data-target="#deactModal" class='deact_link' lang='{{$member->id}}' href="#">Deactivate</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="inactive-members">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="btn btn-info btn-sm" href="{{ URL::to('members/create') }}">New Member</a>
                            </div>
                            <div class="panel-body">
                                <table id="users"
                                       class="table table-condensed table-bordered table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th>Member Branch</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        @if(!$member->is_active && $member->gone!=1)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td>
                                                <td>{{ $member->branch->name }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                           <?php /*@if($member->is_css_active == false)
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                            @endif
                                                            @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                </li>
                                                            @endif */ ?>
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{URL::to('portal/leave/'.$member->id)}}">Leave</a>
                                                                </li>
                                                                <!--<li><a href="{{URL::to('css/reset/'.$member->id)}}">Reset
                                                                    Password</a>
                                                                </li>-->
                                                                <li>
                                                                    <a data-toggle="modal" data-target="#deactdetsModal" class='deactdets_link' method='{{$member->deactivation_date}}' lang='{{$member->deactivation_reason}}' href="#">Details</a>
                                                                </li>    
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="left_members">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="btn btn-info btn-sm" href="{{ URL::to('members/create') }}">New Member</a>
                                <a class="btn btn-info btn-sm align-right"  href="{{ URL::to('member_balanceReport') }}">Report</a>
                            </div>
                            <div class="panel-body">
                                <table id="users"
                                       class="table table-condensed table-bordered table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th>Member Branch</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        @if($member->gone==1)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td>
                                                <td>{{ $member->branch->name }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                           <?php /*@if($member->is_css_active == false)
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                            @endif
                                                            @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                </li>
                                                            @endif */ ?>
                                                                <li>
                                                                    <a href="{{URL::to('portal/refund/'.$member->id)}}">Refunds</a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="modal" data-target="#deactdetsModal" class='deactdets_link' method='{{$member->deactivation_date}}' lang='{{$member->deactivation_reason}}' href="#">Details</a>
                                                                </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="deactModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Member deactivation</h4>
                </div>
                <form method="POST" action="{{{ URL::to('portal/deactivate') }}}" accept-charset="UTF-8" class="modal-body">
                    <fieldset>
                        <div class="form-group">
                            <label for="username">Reason for deactivation</label>
                            <input class="form-control" placeholder="" type="text" name="reason" id="reason" value="{{{ Input::old('reason') }}}">
                            <input class="form-control member_inpu" placeholder="" type="hidden" name="member_id" id="member_id" value="">
                        </div> 
                        <div class="form-group">
                            <input class="" placeholder="" type="checkbox" name="leaving" id="leaving">
                            <label for="leaving">Leaving?</label>
                        </div>
                        <div class="form-actions form-group">
                            <button type="submit" class="btn btn-primary btn-sm">Submit</button> 
                        </div>
                    </fieldset>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div id="deactdetsModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Member deactivation details</h4>
                </div>
                <div  class="modal-body">  
                    <div class=reasonDiv>
                        <span><u>Reason</u></span><br><span class='reasonSpan'></span><br><br>
                    </div>
                    <div class=rdateDiv>
                        <span>Date :: </span><span class='rdateSpan'></span><br><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.deact_link').click(function(){
                var member_id=$(this).attr('lang');
                $('.member_inpu').val(member_id);
            });

            $('.deactdets_link').click(function(){
                var reason=$(this).attr('lang'); var date=$(this).attr('method');
                $('.reasonSpan').html(reason); $('.rdateSpan').html(date); 
            });
        });
    </script>
@stop