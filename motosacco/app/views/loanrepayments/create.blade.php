@extends('layouts.member')
@section('content')
<br/>
<?php
function asMoney($value) {
  return number_format($value, 2);
}
?>
<div class="row">
  <div class="col-lg-5">
  @if(Session::has('none'))
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>{{{ Session::get('none') }}}</strong>
      </div>
   @endif
   </div>
	<div class="col-lg-12">
  <h3>Loan Application</h3>
  <hr>
</div>
</div>
<div class="row">
	<div class="col-lg-5">
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif
       <table class="table table-condensed table-bordered">
        <tr>
          <td>Member</td><td>{{$loanaccount->member->name}}</td>
        </tr>
        <tr>
          <td>Loan Account</td><td>{{$loanaccount->account_number}}</td>
        </tr>
        <tr>
          <td>Loan Amount</td><td>{{ asMoney($loanaccount->amount_disbursed) }}</td>
        </tr>
        <tr>
          <td>Loan Balance</td><td>{{ asMoney($loanbalance) }}</td>
        </tr>
       </table>
		 <form method="POST" action="{{{ URL::to('loanrepayments') }}}" accept-charset="UTF-8">
    <fieldset>
       <table class="table table-condensed table-bordered">
       @if($principal_due>$loanbalance)
        <tr>
          <td>Principal Due</td><td>{{ asMoney($loanbalance) }}</td>
        </tr>
         @else
         <tr>
          <td>Principal Due</td><td>{{ asMoney($principal_due) }}</td>
        </tr>
        @endif
        <tr>
          <td>Interest Due</td><td>{{ asMoney($interest_due) }}</td>
        </tr>
      @if($principal_due>$loanbalance)

          <td>Amount Due</td><td>{{ asMoney($loanbalance + $interest_due)}}</td>
        @else
        <td>Amount Due</td><td>{{ asMoney($principal_due + $interest_due)}}</td>

        @endif

        </tr>
        </table>
        <input class="form-control" placeholder="" type="hidden" name="loanaccount_id" id="loanaccount_id" value="{{ $loanaccount->id }}">
         <div class="form-group">
            <label for="username">Repayment Date <span style="color:red">*</span></label>
            <div class="right-inner-addon ">
            <i class="fa fa-calendar"></i>
            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="date" id="date" value="{{{ date('Y-m-d') }}}">
        </div>
       </div>
       <!--BEGIN VERBOTEN -->
       <input type="hidden" name="principal" value="{{$principal_due}}">
       <input type="hidden" name="interest" value="{{$interest_due}}">
       <!--END VERBOTEN-->
        <div class="form-group">
            <label for="username">Amount</label>
            <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount"
            value="{{{ Input::old('date') }}}">
        </div>
          <div class="form-group">
            <label for="username">Bank Reference</label>
            <textarea name="bank_reference" id="bank_reference" class="form-control"> </textarea>
        </div>

        <div class="form-actions form-group">
          <button type="submit" class="btn btn-primary btn-sm">Submit Payment</button>
        </div>
    </fieldset>
</form>
</div>
</div>
@stop
