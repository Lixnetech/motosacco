@extends('layouts.accounting')
@section('content')
    <br/>

    <div class="row">
        <div class="col-lg-12">
            <h3>New Journal Entry</h3>

            <hr>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-5">


            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            <form method="POST" action="{{{ URL::to('journals') }}}" accept-charset="UTF-8">

                <fieldset>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <div class="right-inner-addon ">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <input class="form-control datepicker" readonly placeholder="Date" type="text" name="date"
                                   id="date" value="{{{ Input::old('date') }}}" required>
                        </div>

                    <div class="form-group">
                        <label for="username">Particular</label>
                        <select class="form-control selectable" name="particular" id="particular" required>
                            <option> --Select Particular--</option>
                            @foreach($particulars as $particular)
                             
                                <option value="{{ $particular->id }}">{{ $particular->name }}</option>
                             
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="username">Description</label>
                        <textarea name="description" id="description" class="form-control"> </textarea>
                    </div>

                    <div class="form-group">
                        <label for="username">Amount</label>
                        <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount"
                               value="{{{ Input::old('amount') }}}">
                    </div>

                    {{--  <div class="form-group">
                          <label for="username">Debit Account</label>
                          <select class="form-control" name="debit_account">

                              <option></option>
                              @foreach($accounts as $account)
                              <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                              @endforeach


                          </select>
                      </div>
              --}}

                    {{-- <div class="form-group">
                         <label for="username">Credit Account</label>
                         <select class="form-control" name="credit_account">

                             <option></option>
                             @foreach($accounts as $account)
                             <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                             @endforeach


                         </select>
                     </div>--}}

                    <div class="form-group">
                        <label for="narration">Narration</label>
                        <select class="form-control selectable" name="narration" id="narration" required>
                            <option value="0">Moto Sacco</option>
                            @foreach($members as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                    </div>

                 <div class="form-group">
            <label for="username"> Transaction Reference</label>
            <textarea class="form-control" name="bank_reference">{{{ Input::old('bank_reference') }}}</textarea>

        </div>


                    <input type="hidden" name="user" value="{{ Confide::user()->username }}">


                    <div class="form-actions form-group">

                        <button type="submit" class="btn btn-primary btn-sm">Submit Entry</button>
                    </div>

                </fieldset>
            </form>


        </div>

    </div>
























@stop