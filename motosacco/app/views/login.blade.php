@include('includes.head')
<?php $organization = Organization::find(1);?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-body">
                    <div style="height: auto; overflow: hidden; text-align: center;">
                        <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo"
                             style="height: 100px; margin: 10px 0;">
                    </div>
                    {{ Confide::makeLoginForm()->render() }}
                </div>
            </div>
        </div>
    </div>
</div>