<?php
function asMoney($value)
{
    return number_format($value, 2);
}
?>

@extends('layouts.accounting')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>Income</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a class="btn btn-info btn-sm" href="{{ URL::to('budget/incomes/create')}}">New</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-info btn-sm" href="{{ URL::to('import_income')}}">Import Income</a>

                </div>
                <div class="panel-body">
                    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Amount</th>
                              <th>Description</th>
                              <th>Month</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($incomes as $income)
                          if(isset($income->particular))
                           {
                                                            <tr>
                                <td> {{ $i }}</td>
                                <td>{{ $income->particular->name }}</td>
                                <td>{{ asMoney($income->amount) }}</td>
                                <td>{{ $income->description }}</td>
                                <td>{{ $income->date }}</td>
                            </tr>
                             }
                            <?php $i++; ?>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop