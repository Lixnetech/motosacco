<html>
<head>
    <style>
        @page {
            margin: 170px 30px;
        }

        .header {
            position: fixed;
            left: 0px;
            top: -150px;
            right: 0px;
            height: 150px;
            text-align: center;
        }

        .footer {
            position: fixed;
            left: 0px;
            bottom: -180px;
            right: 0px;
            height: 50px;
        }

        .footer .page:after {
            content: counter(page, upper-roman);
        }

        .content table {
            width: 100%;
            border-collapse: collapse;
        }
    </style>
<body>

<div class="header">
    <table>
        <tr>
            <td>
                <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}"
                     style="height: 50px;"/>
            </td>
            <td>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}} |
                {{ $organization->email}} |
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </td>
        </tr>
        <tr>
            <hr>
        </tr>
    </table>
</div>
<div class="footer">
    <p class="page">Page <?php $PAGE_NUM ?></p>
</div>
<div class="content">

    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>
    <div>
        <h3>Loan History</h3>
        <h4>{{ $member->name}}</h4>
        <div>
            @foreach($member->loanaccounts as $loanaccount)
                <h5>{{ $loanaccount->account_number }}</h5>
                <div>
                    <div>
                        <table border="1">
                            <tr>
                                <td>Loan Type</td>
                                <td>{{ $loanaccount->loanproduct->name}}</td>
                            </tr>
                            <tr>
                                <td>Date Disbursed</td>
                                <td>{{ $loanaccount->date_disbursed}}</td>
                            </tr>
                            <tr>
                                <td>Amount Disbursed</td>
                                <td>{{ asMoney($loanaccount->amount_disbursed)}}</td>
                            </tr>
                            @if($loanaccount->is_top_up)
                                <tr>
                                    <td>Top Up Amount</td>
                                    <td>{{ asMoney($loanaccount->top_up_amount)}}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>

                <div>
                    <div>
                        <table border="1">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Cr</th>
                                <th>Dr</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($loanaccount->loantransactions as $loantransaction)
                                <tr>
                                    <td>{{ $loantransaction->date }}</td>
                                    <td>{{ $loantransaction->description }}</td>
                                    @if($loantransaction->type == 'credit')
                                        <td>{{ asMoney($loantransaction->amount) }}</td>
                                        <td>0.00</td>
                                    @else
                                        <td>0.00</td>
                                        <td>{{ asMoney($loantransaction->amount) }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>

    <p style="page-break-before: always;"></p>
</div>
</body>
</html>