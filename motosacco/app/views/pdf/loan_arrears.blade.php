@extends('layouts.ports')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> Loan Arrears Report</h3>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
          @if($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
              <a href="#"  class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            </div>
            @endif
            <form target="_blank" method="POST" action="{{URL::to('reports/loan_arrears_pdf')}}">

                <div class="form-group">
                    <label for="username">Loan product</label>
                    <select class="form-control" name="product" id="period" required>
                        <?php foreach($loans as $loan){
                                $loanproduct=Loanproduct::find($loan['loanproduct_id']);
                            ?>
                            <option value="{{$loanproduct->id}}">{{$loanproduct->name}}</option>
                        <?php } ?> 
                    </select>
                </div>

            <div class="form-group">
                <label for="username">Format</label>
                <select class="form-control" name="format" id="format" required>
                    <option value="">Select format</option>
                    <option value="pdf">PDF</option> 
                    <!--<option value="pdf">Excel</option>-->
                </select>
            </div>
    


            <div class="form-actions form-group">


                <button type="submit" class="btn btn-primary btn-sm">Generate Report</button>
            </div>


        </form>

    </div>

</div>

@stop