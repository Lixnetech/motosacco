<?php
function asMoney($value)
{
    return number_format($value, 2);
}
?>
        <!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Comprehensive Report</title>

    <style type="text/css">
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th {
            font-weight: bold;
            text-align: left;
        }

        tr.total {
            outline: thin solid;
        }

        .title {
            text-transform: uppercase;
            text-align: center;
            font-weight: bold;
        }
    </style>
</head><body>
<h3 class="title">
    MOTO STAFF SACCO LTD
    <br>
    proposed budget for the year {{ $set_year }}
</h3>

<table class="table table-condensed table-bordered table-responsive table-hover">
    <thead>
    <tr>
        <th></th>
        <th>1<sup>st</sup> Quarter</th>
        <th>2<sup>nd</sup> Quarter</th>
        <th>3<sup>rd</sup> Quarter</th>
        <th>4<sup>th</sup> Quarter</th>
        <th>Proposed {{ $set_year }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($projections as $title => $values)
        @if(count($values)>0)
            <tr>
                <td style="font-weight: bold; text-transform: uppercase;">{{ $title }}</td>
            </tr>
            <?php
            $first_q = 0;
            $second_q = 0;
            $third_q = 0;
            $fourth_q = 0;
            $total = 0;
            ?>
            @foreach($values as $projection)
                <tr>
                    <td>{{ $projection->name }}</td>
                    <td>{{ asMoney((double)$projection->first_quarter) }}</td>
                    <td>{{ asMoney((double)$projection->second_quarter) }}</td>
                    <td>{{ asMoney((double)$projection->third_quarter) }}</td>
                    <td>{{ asMoney((double)$projection->fourth_quarter) }}</td>
                    <td>{{ asMoney((int)$projection->first_quarter + (int)$projection->second_quarter + (int)$projection->third_quarter + (int)$projection->fourth_quarter) }}</td>
                    <?php
                    $first_q += (int)$projection->first_quarter;
                    $second_q += (int)$projection->second_quarter;
                    $third_q += (int)$projection->third_quarter;
                    $fourth_q += (int)$projection->fourth_quarter;
                    $total += (int)$projection->first_quarter + (int)$projection->second_quarter + (int)$projection->third_quarter + (int)$projection->fourth_quarter;
                    ?>
                </tr>
            @endforeach
            <tr class="total">
                <td></td>
                <td><strong>{{ asMoney($first_q) }}</strong></td>
                <td><strong>{{ asMoney($second_q) }}</strong></td>
                <td><strong>{{ asMoney($third_q) }}</strong></td>
                <td><strong>{{ asMoney($fourth_q) }}</strong></td>
                <td><strong>{{ asMoney($total) }}</strong></td>
            </tr>
        @endif
    @endforeach
    </tbody></table></body></html>