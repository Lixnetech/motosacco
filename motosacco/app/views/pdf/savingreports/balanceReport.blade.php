<html><head>
<style>
    table{width:100% important;}
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px; text-align:center;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
</style></head><body style="font-size:;">
<?php
function asMoney($value)
{
    return number_format($value, 2);
}

?>


<div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
<div class="body">
    <table class="table table-bordered" >
        <tr>
            <td>#</td>
            <td>Member</td>
            <td>Total savings balance</td>
            <td>Total guaranteed amount</td>
            <td>Total loan balance</td>
        </tr>  <?php $i=0; ?>
        @foreach($members as $member) <?php $i++; ?>
            <?php 
                $loanBal=Loantransaction::getMemberLoanBalance($member->id);
                $savingsBal=Savingaccount::getUserSavingsBalance($member->id);
                $finalSavings=Savingaccount::getFinalSavingsBalance($member->id);
                $guaranteeAmount=Loanaccount::amountGuarantee($member->id);
            ?>
            <tr>
                <td>{{$i}}</td>
                <td>{{$member->name}}</td>
                <td>{{$savingsBal}}</td>
                <td>{{$guaranteeAmount}}</td>
                <td>{{$loanBal}}</td>
            </tr> 
        @endforeach
    </table>
</div>
</body></html>
