<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {

    $count = count(User::all());

    if ($count == 0) {

        return View::make('signup');
    }


    if (Confide::user()) {
        return Redirect::to('/dashboard');
    } else {
        return View::make('login');
    }
});

//Links outside filter
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');

Route::group(['before' => 'loggedin'], function () {
    Route::get('/dashboard', function () {
        if (Confide::user()) {
            $organization = Organization::find(1);
            $installationdate = date('Y-m-d', strtotime($organization->installation_date));

            $splitdate = explode('-', $installationdate);
            //split to obtain month and day from the installation date
            $day = $splitdate[2];
            $month = $splitdate[1];
            $year = date('Y');
            // The due date for annual subscription fee.
            $date = date('d-F-Y', strtotime($day . '-' . $month . '-' . $year));
            $notificationperiod = date('d-F-Y', strtotime($date . '-20 days'));
            //set the dates parameters for comparison purposes to avoid incorrct output which occurs from unformatted date comparison
            $todaydate = date('d-F-Y');
            $not_compare = strtotime($notificationperiod);
            $todaydate_compare = strtotime($todaydate);
            $date_compare = strtotime($date);
// $notificationperiod =date('Y-m-d',strtotime($notification->created_at));

            /*today's date in y-m-d format*/
            $dateformat = date('Y-m-d');
//obtain the two digit number of today's date for the purpose of using modulus operator to facilitate skipping a day during notifications period.
            $splittodaydate = explode('-', $dateformat);
            $dayofthemonth = $splittodaydate[2];
            $notification = DB::table('notifications')->where('created_at', '>=', $dateformat)->orderBy('created_at', 'DESC')->first();

            if ($todaydate_compare >= $not_compare && $todaydate_compare <= $date_compare) //if today's notification does not exist and the day is even(skip odd day)
            {
                if (empty($notification) && $dayofthemonth % 2 === 0) {
                    $userid = Confide::user()->id;
                    $key = md5(uniqid());
                    Notification::notifyUser($userid, "Hello, Please make payments for annual suscription fees before " . $date, "payment reminder", "paymentnotification/invoiceshow/" . $key . "/" . $userid, $key);

                }
            }


            if (Confide::user()->user_type == 'admin') {

                $members = Member::all();
                //Grab all available loans
                $loanaccounts = Loanaccount::all();
                foreach ($loanaccounts as $loanaccount) {
                    $period = $loanaccount->period;
                    $endpoint = 30 * $period;
                    $end = '+$endpoint';
                    //Take member ID
                    $id = $loanaccount->member_id;
                    //Get all the member details
                    $member = Member::find($id);
                    //Take the repayment period
                    $startdate = date('Y-m-d', strtotime($loanaccount->repayment_start_date));
                    for ($date = $startdate; $date < date('Y-m-d', strtotime($date . "+$endpoint days")); $date = date('Y-m-d', strtotime($date . "+28 days"))) {
                        //Reset execution limit
                        set_time_limit(60);
                        //Get current month
                        $now = date('Y-m-d');
                        $today = date('Y-m-d', strtotime($now));
                        $month = date('m');
                        //Get payment date
                        $paydate = date('Y-m-d', strtotime($date . "+30 days"));
                        $defaultdate = date('Y-m-d', strtotime($date . "+31 days"));
                        //Get Payment month
                        $pay_month = date('m', strtotime($paydate));
                        //When payment month equals current month send an email
                        if ($month == $pay_month && $today <= $paydate) {
                            /*Mail::send( 'emails.notification', array('name'=>$member->name,
                            'pay_date'=>date('d-F-Y',strtotime($date."+30 days"))), function( $message )
                             use ($member){
                                 $message->to($member->email)->subject( 'Loan Repayment Notification' );
                                });  */
                            break;
                        } else if ($month == $pay_month && $today == $defaultdate) {
                            $loanaccount->is_defaulted = TRUE;
                            $loanaccount->save();
                            $defaultrepay = new Loanrepayment;
                            $defaultrepay->loanaccount_id = $loanaccount->id;
                            $defaultrepay->date = $defaultdate;
                            $defaultrepay->principal_paid = 0.00;
                            $defaultrepay->interest_paid = 0.00;
                            $defaultrepay->default_period = date('m-Y', strtotime($date . "+31 days"));
                            $defaultrepay->save();
                        }
                        break;
                    }
                }
                return View::make('dashboard', compact('members'));

            }

            if (Confide::user()->user_type == 'teller') {

                $members = Member::all();

                return View::make('tellers.dashboard', compact('members'));

            }

            if (Confide::user()->user_type == 'credit') {

                $loanaccounts = Loanaccount::all();

                return View::make('creditS.dashboard', compact('loanaccounts'));

            }


            if (Confide::user()->user_type == 'member') {

                $loans = Loanproduct::all();
                $member = Member::where('email', Confide::user()->email)->first();
                $products = Product::all();
                //$rproducts = Product::getRemoteProducts();

                return View::make('css.memberindex', compact('loans', 'member', 'products'));

            }


        } else {
            return View::make('login');
        }
    });
//
    Route::get('notifications/index', 'NotificationController@index');
    Route::get('notifications/markasread/{id}', 'NotificationController@markasread');
    Route::get('notifications/markallasread', 'NotificationController@markallasread');
    Route::get('users/logout', 'UsersController@logout');

    Route::post('license/activate_annual', 'OrganizationsController@annual_subscription_license');
    Route::post('license/key', 'OrganizationsController@generate_license_key');
    Route::post('license/activate', 'OrganizationsController@activate_license');
    Route::get('license/activate_annual/{id}', 'OrganizationsController@activate_annual_license_form');
    Route::get('license/activate/{id}', 'OrganizationsController@activate_license_form');


    Route::get('license', function () {


        $organization = Organization::find(1);

        return View::make('system.license', compact('organization'));
    });


// Confide routes
    Route::get('users/create', 'UsersController@create');
    Route::get('users/edit/{user}', 'UsersController@edit');
    Route::post('users/update/{user}', 'UsersController@update');
    Route::post('users', 'UsersController@store');

    Route::resource('users', 'UsersController');
    Route::get('users/activate/{user}', 'UsersController@activate');
    Route::get('users/deactivate/{user}', 'UsersController@deactivate');
    Route::get('users/destroy/{user}', 'UsersController@destroy');
    Route::get('users/password/{user}', 'UsersController@Password');
    Route::post('users/password/{user}', 'UsersController@changePassword');
    Route::get('users/profile/{user}', 'UsersController@profile');
    Route::get('users/add', 'UsersController@add');
    Route::post('users/newuser', 'UsersController@newuser');

    Route::get('tellers', 'UsersController@tellers');
    Route::get('tellers/create/{id}', 'UsersController@createteller');
    Route::get('tellers/activate/{id}', 'UsersController@activateteller');
    Route::get('tellers/deactivate/{id}', 'UsersController@deactivateteller');

    Route::get('credits', 'UsersController@credits');
    Route::get('credits/create/{id}', 'UsersController@createcredit');
    Route::get('credits/activate/{id}', 'UsersController@activatecredit');
    Route::get('credits/deactivate/{id}', 'UsersController@deactivatecredit');

    Route::get('members/profile', 'UsersController@password2');
    Route::post('users/pass', 'UsersController@changePassword2');


    Route::resource('roles', 'RolesController');
    Route::get('roles/create', 'RolesController@create');
    Route::get('roles/edit/{id}', 'RolesController@edit');
    Route::get('roles/show/{id}', 'RolesController@show');
    Route::post('roles/update/{id}', 'RolesController@update');
    Route::get('roles/delete/{id}', 'RolesController@destroy');


//    Route::group(['before' => 'payment_access_restriction'], function () {

        Route::get('member', function () {


            $member = Member::where('membership_no', Confide::user()->username)->first();

            return View::make('css.memberindex', compact('member'));

        });


        Route::get('memberloanrepayments', function () {
            $m = Member::where('membership_no', Confide::user()->username)->first();
            $member = Member::findOrFail($m->id);
            /*$loanaccounts = DB::table('loanaccounts')
                               ->join('loanproducts', 'loanaccounts.loanproduct_id', '=', 'loanproducts.id')
                               ->join('members', 'loanaccounts.member_id', '=', 'members.id')
                               ->where('loanaccounts.member_id',$member->id)
                               ->where('loanaccounts.is_approved',1)
                               ->select('loanaccounts.id as id','members.name as mname','members.id as mid','loanproducts.name as pname','phone','application_date','amount_applied','repayment_duration','loanaccounts.interest_rate')
                               ->get();*/

            return View::make('css.loanrepayment', compact('member'));

        });

        Route::post('loanpayment/{id}', function () {
            $name = Input::get('mname');
            $date = Input::get('date');
            $phone = Input::get('phone');
            $mid = Input::get('mid');
            $loanaccount_id = Input::get('loanaccount_id');
            $amount = Input::get('amount');
            View::addLocation(app_path() . '/views/pesapal-php-master');
            View::addNamespace('theme', app_path() . '/views/pesapal-php-master');

            return View::make('pesapal-iframe', compact('name', 'date', 'phone', 'amount', 'mid', 'loanaccount_id'));

        });


        Route::post('memberloanrepayments/offsetloan', function () {
            $name = Input::get('mname');
            $date = Input::get('date');
            $phone = Input::get('phone');
            $mid = Input::get('mid');
            $loanaccount_id = Input::get('loanaccount_id');
            $amount = Input::get('amount');
            View::addLocation(app_path() . '/views/pesapal-php-master');
            View::addNamespace('theme', app_path() . '/views/pesapal-php-master');

            return View::make('pesapal-iframe-offset', compact('name', 'date', 'phone', 'amount', 'mid', 'loanaccount_id'));

        });

        Route::get('/pesapal_callback', function () {
            $validator = Validator::make($data = Input::all(), Loanrepayment::$rules);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $loanaccount = Input::get('loanaccount_id');
            Loanrepayment::repayLoan($data);
            return Redirect::to('/memberloanrepayments')->withFlashMessage('You have successfully paid this month`s loan instalment!');

        });

        Route::get('/pesapal_callback_offset', function () {
            $validator = Validator::make($data = Input::all(), Loanrepayment::$rules);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $loanaccount = Input::get('loanaccount_id');
            Loanrepayment::offsetLoan($data);
            return Redirect::to('/memberloanrepayments')->withFlashMessage('You have successfully completed paying your loan!');

        });


        Route::post('membersavingtransactions/{id}', function () {
            if (Input::get('type') == 'credit') {
                $transacted_by = Input::get('transacted_by');
                $date = Input::get('date');
                $ttype = Input::get('type');
                $phone = Input::get('phone');
                $mid = Input::get('mid');
                $description = Input::get('description');
                $account_id = Input::get('account_id');
                $amount = Input::get('amount');
                View::addLocation(app_path() . '/views/pesapal-php-master');
                View::addNamespace('theme', app_path() . '/views/pesapal-php-master');

                return View::make('pesapal-iframe-savings', compact('transacted_by', 'description', 'date', 'phone', 'amount', 'mid', 'account_id', 'ttype'));
            } else {
                $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);

                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator)->withInput();
                }

                $date = Input::get('date');
                $transAmount = Input::get('amount');
                $currency = Currency::find(1);

                $savingaccount = Savingaccount::findOrFail(Input::get('account_id'));
                $date = Input::get('date');
                $amount = Input::get('amount');
                $type = Input::get('type');
                $description = Input::get('description');
                $transacted_by = Input::get('transacted_by');


                Savingtransaction::transact($date, $savingaccount, $amount, $type, $description, $transacted_by);

                return Redirect::to('memtransactions/' . $savingaccount->id)->withFlashMessage('You have successfully withdrawn ' . $currency->shortname . '. ' . number_format($transAmount, 2) . ' from your savings account!');
            }
        });

        Route::get('/pesapal_callback_saving', function () {
            $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $date = Input::get('date');
            $transAmount = Input::get('amount');
            $currency = Currency::find(1);

            $savingaccount = Savingaccount::findOrFail(Input::get('account_id'));
            $date = Input::get('date');
            $amount = Input::get('amount');
            $type = Input::get('type');
            $description = Input::get('description');
            $transacted_by = Input::get('transacted_by');


            Savingtransaction::transact($date, $savingaccount, $amount, $type, $description, $transacted_by);

            return Redirect::to('memtransactions/' . $savingaccount->id)->withFlashMessage('You have successfully deposited ' . $currency->shortname . '. ' . number_format($amount, 2) . ' to your savings account!');

        });


        Route::get('transaudits', function () {
            $transactions = Loantransaction::all();
            return View::make('transaud', compact('transactions'));
        });


        Route::post('transaudits', function () {
            $date = Input::get('date');
            $type = Input::get('type');
            if ($type == 'loan') {
                $transactions = DB::table('loantransactions')->where('date', '=', $date)->get();
                return View::make('transaudit', compact('transactions', 'type', 'date'));
            }

            if ($type == 'savings') {
                $transactions = DB::table('savingtransactions')->where('date', '=', $date)->get();
                return View::make('transaudit', compact('transactions', 'type', 'date'));
            }
        });


        Route::get('import', function () {

            return View::make('import');
        });


        Route::get('automated/loans', function () {


            $loanproducts = Loanproduct::all();

            return View::make('autoloans', compact('loanproducts'));
        });

        Route::get('automated/savings', function () {


            $savingproducts = Savingproduct::all();

            return View::make('automated', compact('savingproducts'));
        });


        Route::post('automated', function () {

            $members = DB::table('members')->where('is_active', '=', true)->get();


            $category = Input::get('category');


            if ($category == 'savings') {

                $savingproduct_id = Input::get('savingproduct');

                $savingproduct = Savingproduct::findOrFail($savingproduct_id);


                foreach ($savingproduct->savingaccounts as $savingaccount) {

                    if (($savingaccount->member->is_active) && (Savingaccount::getLastAmount($savingaccount) > 0)) {


                        $data = array(
                            'account_id' => $savingaccount->id,
                            'amount' => Savingaccount::getLastAmount($savingaccount),
                            'date' => date('Y-m-d'),
                            'type' => 'credit'
                        );

                        Savingtransaction::creditAccounts($data);


                    }


                }
                Autoprocess::record(date('Y-m-d'), 'saving', $savingproduct);
            } else {
                $loanproduct_id = Input::get('loanproduct');
                $loanproduct = Loanproduct::findOrFail($loanproduct_id);
                foreach ($loanproduct->loanaccounts as $loanaccount) {
                    if (($loanaccount->member->is_active) && (Loanaccount::getEMP($loanaccount) > 0)) {
                        $data = array(
                            'loanaccount_id' => $loanaccount->id,
                            'amount' => Loanaccount::getEMP($loanaccount),
                            'date' => date('Y-m-d')
                        );
                        Loanrepayment::repayLoan($data);
                    }
                }
                Autoprocess::record(date('Y-m-d'), 'loan', $loanproduct);
            }
            return Redirect::back()->with('notice', 'successfully processed');
        });


        Route::get('system', function () {


            $organization = Organization::find(1);

            return View::make('system.index', compact('organization'));
        });


        /**Route::get('license', function () {
         *
         *
         * $organization = Organization::find(1);
         *
         * return View::make('system.license', compact('organization'));
         * });**/


        /**
         * Organization routes
         */
        Route::resource('organizations', 'OrganizationsController');
        Route::post('organizations/update/{id}', 'OrganizationsController@update');
        Route::post('organizations/logo/{id}', 'OrganizationsController@logo');

        Route::get('language/{lang}',
            array(
                'as' => 'language.select',
                'uses' => 'OrganizationsController@language'
            )
        );


        Route::resource('currencies', 'CurrenciesController');
        Route::get('currencies/edit/{id}', 'CurrenciesController@edit');
        Route::post('currencies/update/{id}', 'CurrenciesController@update');
        Route::get('currencies/delete/{id}', 'CurrenciesController@destroy');
        Route::get('currencies/create', 'CurrenciesController@create');


        Route::get('loanrepayments/offprint/{id}', 'LoanrepaymentsController@offprint');


        /*
        * apartments routes
        */

        Route::resource('apartments', 'ApartmentsController');
        Route::get('apartments/list', 'ApartmentsController@list');
        Route::get('apartments/create', 'ApartmentsController@create');
        Route::get('apartments/edit/{id}', 'ApartmentsController@edit');
        Route::post('apartments/update/{id}', 'ApartmentsController@update');
        Route::get('apartments/show/{id}', 'ApartmentsController@show');
        Route::get('apartments/delete/{id}', 'ApartmentsController@destroy');


        /*
        * branches routes
        */


        Route::resource('branches', 'BranchesController');
        Route::post('branches/update/{id}', 'BranchesController@update');
        Route::get('branches/delete/{id}', 'BranchesController@destroy');
        Route::get('branches/edit/{id}', 'BranchesController@edit');


        Route::resource('groups', 'GroupsController');
        Route::post('groups/update/{id}', 'GroupsController@update');
        Route::get('groups/delete/{id}', 'GroupsController@destroy');
        Route::get('groups/edit/{id}', 'GroupsController@edit');


        Route::resource('members', 'MembersController');
        Route::post('members/update/{id}', 'MembersController@update');
        Route::post('member/update/{id}', 'MembersController@update');
        Route::get('members/delete/{id}', 'MembersController@destroy');
        Route::get('members/edit/{id}', 'MembersController@edit');
        Route::get('member/edit/{id}', 'MembersController@edit');
        Route::get('members/show/{id}', 'MembersController@show');
        Route::get('members/summary/{id}', 'MembersController@summary');
        Route::get('member/show/{id}', 'MembersController@show');
        Route::post('deldoc', 'MembersController@deletedoc');
        Route::get('members/loanaccounts/{id}', 'MembersController@loanaccounts');
        Route::get('memberloans', 'MembersController@loanaccounts2');
        Route::group(['before' => 'limit'], function () {

            Route::get('members/create', 'MembersController@create');
        });
        Route::get('members/loanhistory/{id}', 'MembersController@loanHistory');
        Route::get('members/loanhistory/{id}/report', 'MembersController@loanHistoryReport');
        Route::get('member_balanceReport', function () {
            $organization = Organization::find(1);
            $members = Member::where("gone", 1)->get();

            $pdf = PDF::loadView('pdf.savingreports.balanceReport', compact('organization', 'members'))->setPaper('a4')->setOrientation('portrait');
            return $pdf->stream('Balances.pdf');
        });

        Route::resource('kins', 'KinsController');
        Route::post('kins/update/{id}', 'KinsController@update');
        Route::get('kins/delete/{id}', 'KinsController@destroy');
        Route::get('kins/edit/{id}', 'KinsController@edit');
        Route::get('kins/show/{id}', 'KinsController@show');
        Route::get('kins/create/{id}', 'KinsController@create');


        Route::resource('accounts', 'AccountsController');
        Route::post('accounts/update/{id}', 'AccountsController@update');
        Route::get('accounts/delete/{id}', 'AccountsController@destroy');
        Route::get('accounts/edit/{id}', 'AccountsController@edit');
        Route::get('accounts/show/{id}', 'AccountsController@show');
        Route::get('accounts/create/{id}', 'AccountsController@create');


        Route::resource('journals', 'JournalsController');
        Route::post('journals/update/{id}', 'JournalsController@update');
        Route::get('journals/delete/{id}', 'JournalsController@destroy');
        Route::get('journals/edit/{id}', 'JournalsController@edit');
        Route::get('journals/show/{id}', 'JournalsController@show');


        Route::resource('charges', 'ChargesController');
        Route::post('charges/update/{id}', 'ChargesController@update');
        Route::get('charges/delete/{id}', 'ChargesController@destroy');
        Route::get('charges/edit/{id}', 'ChargesController@edit');
        Route::get('charges/show/{id}', 'ChargesController@show');
        Route::get('charges/disable/{id}', 'ChargesController@disable');
        Route::get('charges/enable/{id}', 'ChargesController@enable');

        Route::resource('savingproducts', 'SavingproductsController');
        Route::get('savingproducts/update/{id}', 'SavingproductsController@selectproduct');
        Route::post('savingproducts/update', 'SavingproductsController@update');
        Route::get('savingproducts/delete/{id}', 'SavingproductsController@destroy');
        Route::get('savingproducts/edit/{id}', 'SavingproductsController@edit');
        Route::get('savingproducts/show/{id}', 'SavingproductsController@show');


        Route::resource('monthlyremittances', 'RemittancesController');
        Route::post('monthlyremittances/update/{id}', 'RemittancesController@update');
        Route::get('monthlyremittances/delete/{id}', 'RemittancesController@destroy');
        Route::get('monthlyremittances/edit/{id}', 'RemittancesController@edit');
        Route::get('monthlyremittances/show/{id}', 'RemittancesController@show');

        Route::resource('savingaccounts', 'SavingaccountsController');
        Route::get('savingaccounts/create/{id}', 'SavingaccountsController@create');
        Route::get('member/savingaccounts/{id}', 'SavingaccountsController@memberaccounts');
        Route::post('savings/refund', 'SavingaccountsController@refund');

        Route::get('savingtransactions/show/{id}', 'SavingtransactionsController@show');
        Route::resource('savingtransactions', 'SavingtransactionsController');
        Route::get('savingtransactions/create/{id}', 'SavingtransactionsController@create');
        Route::get('membersavingtransactions/create/{id}', 'SavingtransactionsController@create');
        Route::get('savingtransactions/receipt/{id}', 'SavingtransactionsController@receipt');
        Route::get('savingtransactions/statement/{id}', 'SavingtransactionsController@statement');
        Route::get('savingtransactions/void/{id}', 'SavingtransactionsController@void');

        Route::post('savingtransactions/import', 'SavingtransactionsController@import');

        Route::get('templates/savings', function () {
            return Excel::create('SavingsTransactions', function ($excel) {

                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


                $excel->sheet('SavingsTransactions', function ($sheet) {


                    $sheet->row(1, array(
                        'DATE', 'ACCOUNT NUMBER', 'AMOUNT', 'TYPE', 'DESCRIPTION', 'BANK REF', 'TRANSACTION METHOD',
                    ));

                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 60,
                        'C' => 30,
                        'D' => 30,
                        'E' => 30,
                        'F' => 30,
                        'G' => 30,
                    ));

                    /*$sheet->getStyle('A2:A100')
                        ->getNumberFormat()
                        ->setFormatCode('yyyy-mm-dd');*/

                    $sheet->setColumnFormat(array(
                        "A" => "yyyy-mm-dd",
                    ));

                    $row = 2;
                    $r = 2;
                    $savingsAccounts = Savingaccount::all();
                    if (ob_get_level() > 0) {
                        ob_end_clean();
                    }

                    for ($i = 0; $i < count($savingsAccounts); $i++) {
                        $member = Member::find($savingsAccounts[$i]->member_id);
                        $sheet->SetCellValue("Y" . $row, $member->name . ": " . $savingsAccounts[$i]->account_number);
                        $row++;
                    }

                    $sheet->_parent->addNamedRange(
                        new \PHPExcel_NamedRange(
                            'accounts', $sheet, 'Y2:Y' . (count($savingsAccounts) + 1)
                        )
                    );


                    for ($i = 2; $i <= 1000; $i++) {

                        $objValidation = $sheet->getCell('B' . $i)->getDataValidation();
                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                        $objValidation->setAllowBlank(false);
                        $objValidation->setShowInputMessage(true);
                        $objValidation->setShowErrorMessage(true);
                        $objValidation->setShowDropDown(true);
                        $objValidation->setErrorTitle('Input error');
                        $objValidation->setError('Value is not in list.');
                        $objValidation->setPromptTitle('Pick from list');
                        $objValidation->setPrompt('Please pick a value from the drop-down list.');
                        $objValidation->setFormula1('accounts'); //note this!

                        $objValidation = $sheet->getCell('D' . $i)->getDataValidation();
                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                        $objValidation->setAllowBlank(false);
                        $objValidation->setShowInputMessage(true);
                        $objValidation->setShowErrorMessage(true);
                        $objValidation->setShowDropDown(true);
                        $objValidation->setErrorTitle('Input error');
                        $objValidation->setError('Value is not in list.');
                        $objValidation->setPromptTitle('Pick from list');
                        $objValidation->setPrompt('Please pick a value from the drop-down list.');
                        $objValidation->setFormula1('"credit, debit"'); //note this!

                        $objValidation = $sheet->getCell('G' . $i)->getDataValidation();
                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                        $objValidation->setAllowBlank(false);
                        $objValidation->setShowInputMessage(true);
                        $objValidation->setShowErrorMessage(true);
                        $objValidation->setShowDropDown(true);
                        $objValidation->setErrorTitle('Input error');
                        $objValidation->setError('Value is not in list.');
                        $objValidation->setPromptTitle('Pick from list');
                        $objValidation->setPrompt('Please pick a value from the drop-down list.');
                        $objValidation->setFormula1('"bank, cash"'); //note this!
                    }

                });

            })->export('xlsx');
        });
        Route::get('banking/import/statement', function () {
            return View::make('banking.importbankitems');
        });

        Route::get('templates/bankstatement', function () {
            return Excel::create('BankStatement', function ($excel) {

                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


                $excel->sheet('BankStatement', function ($sheet) {


                    $sheet->row(1, array(
                        'Sr.No', 'Date', 'Value Date', 'Bank Reference No', 'Customer Reference No', 'Description', 'Debit Amount', 'Credit Amount', 'Running Balance',
                    ));

                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 30,
                        'C' => 30,
                        'D' => 60,
                        'E' => 60,
                        'F' => 30,
                        'G' => 30,
                        'H' => 30,
                        'I' => 30

                    ));

                    /*$sheet->getStyle('A2:A100')
                        ->getNumberFormat()
                        ->setFormatCode('yyyy-mm-dd');*/

                    $sheet->setColumnFormat(array(
                        "B" => "yyyy-mm-dd",
                    ));

                    $row = 2;
                    $r = 2;
                    $savingsAccounts = Savingaccount::all();
                    if (ob_get_level() > 0) {
                        ob_end_clean();
                    }
                    /**
                     * for ($i = 0; $i < count($savingsAccounts); $i++) {
                     * $member = Member::find($savingsAccounts[$i]->member_id);
                     * $sheet->SetCellValue("Y" . $row, $member->name . ": " . $savingsAccounts[$i]->account_number);
                     * $row++;
                     * }
                     *
                     * $sheet->_parent->addNamedRange(
                     * new \PHPExcel_NamedRange(
                     * 'accounts', $sheet, 'Y2:Y' . (count($savingsAccounts) + 1)
                     * )
                     * );
                     *
                     *
                     * for ($i = 2; $i <= 1000; $i++) {
                     *
                     * $objValidation = $sheet->getCell('B' . $i)->getDataValidation();
                     * $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                     * $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                     * $objValidation->setAllowBlank(false);
                     * $objValidation->setShowInputMessage(true);
                     * $objValidation->setShowErrorMessage(true);
                     * $objValidation->setShowDropDown(true);
                     * $objValidation->setErrorTitle('Input error');
                     * $objValidation->setError('Value is not in list.');
                     * $objValidation->setPromptTitle('Pick from list');
                     * $objValidation->setPrompt('Please pick a value from the drop-down list.');
                     * $objValidation->setFormula1('accounts'); //note this!
                     *
                     * $objValidation = $sheet->getCell('D' . $i)->getDataValidation();
                     * $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                     * $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                     * $objValidation->setAllowBlank(false);
                     * $objValidation->setShowInputMessage(true);
                     * $objValidation->setShowErrorMessage(true);
                     * $objValidation->setShowDropDown(true);
                     * $objValidation->setErrorTitle('Input error');
                     * $objValidation->setError('Value is not in list.');
                     * $objValidation->setPromptTitle('Pick from list');
                     * $objValidation->setPrompt('Please pick a value from the drop-down list.');
                     * $objValidation->setFormula1('"credit, debit"'); //note this!
                     * }**/

                });

            })->export('xlsx');
        });

        Route::post('migration/import/savings', 'SavingtransactionsController@importSavings');
        Route::get('migration/import/savings', function () {
            return View::make('savingtransactions.import');
        });

        Route::get('templates/savingss', function () {
            return Savingaccount::getAccountNumbersString();
        });

//Route::resource('savingpostings', 'SavingpostingsController');

        Route::get('transfershares', function () {
            $id = Input::get('member');
            $members = Member::all();


            return View::make('members.transfershares', compact('members'));
        });

        Route::resource('shares', 'SharesController');
        Route::post('shares/update/{id}', 'SharesController@update');
        Route::get('shares/delete/{id}', 'SharesController@destroy');
        Route::get('shares/edit/{id}', 'SharesController@edit');
        Route::get('shares/show/{id}', 'SharesController@show');
        Route::get('sharecapital/contribution', 'SharesController@contribution');
        Route::get('sharecapital/dividend', 'SharesController@dividend');
        Route::get('sharecapital/parameters', 'SharesController@parameters');
        Route::post('sharecapital/parameters', 'SharesController@parameterize');
        Route::get('sharecapital/editparameters', 'SharesController@editparameters');
        Route::post('sharecapital/editparameters', 'SharesController@doparameterize');


        Route::get('sharetransactions/show/{id}', 'SharetransactionsController@show');
        Route::post('shares/transfer', 'SharetransactionsController@sharetransfer');
        Route::resource('sharetransactions', 'SharetransactionsController');
        Route::get('sharetransactions/create/{id}', 'SharetransactionsController@create');


        Route::resource('sharereports', 'sharereportsController');
        Route::get('sharereports/individualcontribution', 'sharereportsController@show');
        Route::post('sharereports/individualcontribution', 'sharereportsController@individual');


        /**Route::post('license/key', 'OrganizationsController@generate_license_key');
         * Route::post('license/activate', 'OrganizationsController@activate_license');
         * Route::get('license/activate/{id}', 'OrganizationsController@activate_license_form');*
         */


        Route::resource('loanproducts', 'LoanproductsController');
        Route::post('loanproducts/update/{id}', 'LoanproductsController@update');
        Route::get('loanproducts/delete/{id}', 'LoanproductsController@destroy');
        Route::get('loanproducts/edit/{id}', 'LoanproductsController@edit');
        Route::get('loanproducts/show/{id}', 'LoanproductsController@show');
        Route::get('memberloanshow/{id}', 'LoanproductsController@memberloanshow');

        Route::resource('disbursements', 'DisbursementController');
        Route::get('disbursements/create', 'DisbursementController@create');
        Route::post('disbursements/create', 'DisbursementController@docreate');
        Route::get('disbursements/update/{id}', 'DisbursementController@update');
        Route::post('disbursements/update', 'DisbursementController@doupdate');
        Route::get('disbursements/delete/{id}', 'DisbursementController@destroy');

        Route::resource('investmentscats', 'InvestmentcategoriesController');
        Route::get('investmentscats/create', 'InvestmentcategoriesController@create');
        Route::get('investmentscats/update/{id}', 'InvestmentcategoriesController@update');
        Route::post('investmentscats/update', 'InvestmentcategoriesController@doupdate');
        Route::get('investmentscats/delete/{id}', 'InvestmentcategoriesController@destroy');

        Route::resource('loanguarantors', 'LoanguarantorsController');
        Route::post('loanguarantors/update/{id}', 'LoanguarantorsController@update');
        Route::get('loanguarantors/delete/{id}', 'LoanguarantorsController@destroy');
        Route::get('loanguarantors/edit/{id}', 'LoanguarantorsController@edit');
        Route::get('loanguarantors/create/{id}', 'LoanguarantorsController@create');
        Route::get('memberloanguarantors/create/{id}', 'LoanguarantorsController@create');
        Route::get('loanguarantors/css/{id}', 'LoanguarantorsController@create');

        Route::post('loanguarantors/cssupdate/{id}', 'LoanguarantorsController@cssupdate');
        Route::get('loanguarantors/cssdelete/{id}', 'LoanguarantorsController@cssdestroy');
        Route::get('loanguarantors/cssedit/{id}', 'LoanguarantorsController@cssedit');


        Route::resource('loans', 'LoanaccountsController');
        Route::get('loans/apply/{id}', 'LoanaccountsController@apply');
        Route::get('guarantorapproval', 'LoanaccountsController@guarantor');
        Route::post('loans/apply', 'LoanaccountsController@doapply');
        Route::post('loans/application', 'LoanaccountsController@doapply2');
        Route::get('loanduplicates', 'LoanaccountsController@management');


        Route::get('loantransactions/statement/{id}', 'LoantransactionsController@statement');
        Route::get('loantransactions/certificate/{id}', 'LoantransactionsController@certificate');
        Route::get('loantransactions/overpayments/{id}', 'LoantransactionsController@overpayments');
        Route::get('loantransactions/receipt/{id}', 'LoantransactionsController@receipt');
        Route::get('loanapplication/member', 'LoanaccountsController@member');
        Route::get('loanapplication/form/{id}', 'LoanaccountsController@application');
        Route::post('loanapplication/form', 'LoanaccountsController@application22');

        Route::get('loans/application/{id}', 'LoanaccountsController@apply2');
        Route::post('shopapplication', 'LoanaccountsController@shopapplication');

        Route::get('loans/edit/{id}', 'LoanaccountsController@edit');
        Route::get('loanmanagement', 'LoanaccountsController@index');
        Route::post('loans/update/{id}', 'LoanaccountsController@update');

        Route::get('loans/approve/{id}', 'LoanaccountsController@approve');
        Route::post('loans/approve/{id}', 'LoanaccountsController@doapprove');
        Route::post('gurantorapprove/{id}', 'LoanaccountsController@guarantorapprove');
        Route::post('gurantorreject/{id}', 'LoanaccountsController@guarantorreject');

        Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
        Route::post('rejectapplication', 'LoanaccountsController@rejectapplication');

        Route::get('loans/disburse/{id}', 'LoanaccountsController@disburse');
        Route::post('loans/disburse/{id}', 'LoanaccountsController@dodisburse');

        Route::get('loans/show/{id}', 'LoanaccountsController@show');
        Route::post('loans/update-repayment-period/{id}', 'LoanaccountsController@updateRepaymentPeriod');
        Route::get('memberloan/show/{id}', 'LoanaccountsController@show');

        Route::post('loans/amend/{id}', 'LoanaccountsController@amend');

        Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
        Route::post('loans/reject/{id}', 'LoanaccountsController@rejectapplication');


        Route::get('loanaccounts/topup/{id}', 'LoanaccountsController@gettopup');
        Route::post('loanaccounts/topup/{id}', 'LoanaccountsController@topup');

        Route::get('memloans/{id}', 'LoanaccountsController@show2');
        Route::post('import_income', 'AccountsController@importincome');
        Route::get('import_income', 'AccountsController@importincomeView');
        Route::get('income_template', 'AccountsController@createIncomeTemplate');
        Route::post('import_repayments', 'LoanrepaymentsController@importRepayment');
        Route::get('import_expenses', 'AccountsController@importexpenseView');

        Route::post('import_expenses', 'AccountsController@importExpense');
        Route::get('expense_template', 'AccountsController@createTemplate');


        Route::resource('loanrepayments', 'LoanrepaymentsController');

        Route::get('loanrepayments/create/{id}', 'LoanrepaymentsController@create');
        Route::get('memberloanrepayments/create/{id}', 'LoanrepaymentsController@create');
        Route::get('loanrepayments/offset/{id}', 'LoanrepaymentsController@offset');
        Route::get('repayments_template', 'LoanrepaymentsController@createTemplate');
        Route::get('import_repayments', 'LoanrepaymentsController@importView');
        Route::post('import_repayments', 'LoanrepaymentsController@importRepayment');
//Converting and recovering loans routes
        Route::get('loanrepayments/recover/{id}', 'LoanrepaymentsController@recoverloan');
        Route::get('loanrepayments/convert/{id}', 'LoanrepaymentsController@convert');
        Route::get('loanrepayments/refinance/{id}', 'LoanrepaymentsController@refinanceCreate');
        Route::post('loanrepayments/refinance/{id}', 'LoanrepaymentsController@refinance');
        Route::post('loanrepayments/recover/complete', 'LoanrepaymentsController@doRecover');
        Route::post('loanrepayments/convert/commit', 'LoanrepaymentsController@doConvert');
//Guarantor Liabilities
        Route::resource('loanliabilities', 'LoanliabilitiesController');

        Route::resource('matrices', 'MatrixController');
        Route::get('matrices/create', 'MatrixController@create');
        Route::post('matrices/create', 'MatrixController@docreate');
        Route::get('matrices/update/{id}', 'MatrixController@update');
        Route::post('matrices/update', 'MatrixController@doupdate');
        Route::get('matrices/delete/{id}', 'MatrixController@destroy');

        Route::get('memberloanrepayments/offset/{id}', 'LoanrepaymentsController@offset');
        Route::post('loanrepayments/offsetloan', 'LoanrepaymentsController@offsetloan');
        Route::get('reports', function () {

            return View::make('members.reports');
        });
        Route::get('reports/combined', function () {

            $members = Member::all();

            return View::make('members.combined', compact('members'));
        });
        /*Route::post('reports/combinedstatement', function () {
            $id = Input::get('member');

            $member = Member::where('id', '=', $id)->first();
           //End Prerequisites and Start Saving Transactions
            $account = Savingaccount::where('member_id', '=', $id)->first();
            if (!empty($account)) {
                $transactions = $account->transactions;
                $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
                $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');
                $balance = $credit - $debit;
                //End Saving Transactions and Start Loans Transactions Summary
                $loans = Loanaccount::where('member_id', '=', $id)->get();
                //End Loan Transactions Summary
                $organization = Organization::find(1);
                $pdf = PDF::loadView('pdf.comprehensive', compact('member', 'transactions', 'organization', 'balance', 'account', 'loans'))
                    ->setPaper('a4')->setOrientation('potrait');
                return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Stetement.pdf');
            } else {
               //End Saving Transactions and Start Loans Transactions Summary
                $loans = Loanaccount::where('member_id', '=', $id)->get();
               //End Loan Transactions Summary
                $organization = Organization::find(1);
                $pdf = PDF::loadView('pdf.comprehensive', compact('member', 'organization', 'loans'))->setPaper('a4')->setOrientation('potrait');
                return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Stetement.pdf');
            }
        });*/
        Route::get('bankReconciliation/transact', function () {

            return View::make('banking.bankTransactionEntries');
        });


        Route::post('reports/combinedstatement', function () {
            $id = Input::get('member');

            $member = Member::where('id', '=', $id)->first();
            $account = Savingaccount::where('member_id', '=', $member->id)->where('savingproduct_id', '=', 4)->first();
            $saving = Savingaccount::where('member_id', '=', $member->id)->where('savingproduct_id', '=', 5)->first();
            $interest = DB::table('savingproducts')->where('id', '=', 5)->pluck('Interest_Rate');

            if (!empty($account)) {

                $date = date("d-M-Y");

                /*End Prerequisites and Start Saving Transactions*/
                $savingaccounts = Savingaccount::where('member_id', '=', $member->id)->get();

                if (!empty($account)) {

                    $transactions = $account->transactions;

                    $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
                    $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');
                    $balance = $credit - $debit;
                    /*End Saving Transactions and Start Loans Transactions Summary*/
                    $loans = Loanaccount::where('member_id', '=', $id)->get();
                    /*End Loan Transactions Summary*/
                    $organization = Organization::find(1);
                    $pdf = PDF::loadView('pdf.comprehensive', compact('member', 'saving', 'interest', 'transactions', 'savingaccounts', 'organization', 'account', 'balance', 'loans'))
                        ->setPaper('a4')->setOrientation('potrait');
                    return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Stetement.pdf');
                } else {
                    /*End Saving Transactions and Start Loans Transactions Summary*/
                    $loans = Loanaccount::where('member_id', '=', $id)->get();
                    /*End Loan Transactions Summary*/
                    $organization = Organization::find(1);
                    $pdf = PDF::loadView('pdf.comprehensive', compact('member', 'organization', 'loans'))->setPaper('a4')->setOrientation('potrait');
                    return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Stetement.pdf');
                }
            } else {
                return Redirect::back()->withErrors('Saving or loan account for the selected member does not exist');
            }
        });
        Route::get('loanreports', function () {

            $loanproducts = Loanproduct::all();

            return View::make('loanaccounts.reports', compact('loanproducts'));
        });
        Route::get('loanrepayments', function () {

            $loanproducts = Loanproduct::all();

            return View::make('loanaccounts.repayments', compact('loanproducts'));
        });


        Route::get('reports/monthlyrepayments', function () {
            $loans = Loanaccount::where('is_disbursed', '=', TRUE)
                ->groupBy('member_id')
                ->get();
            return View::make('loanaccounts.monthlyrepayments', compact('loans'));
        });

        Route::get('savingreports', function () {

            $savingproducts = Savingproduct::all();

            return View::make('savingaccounts.reports', compact('savingproducts'));
        });


        Route::get('financialreports', function () {
            $incomeAccounts = Account::select('id')->where('category', 'INCOME')->get()->toArray();
            $incomeParticulars = Particular::whereIn('creditaccount_id', $incomeAccounts)->get();

            $expenseAccounts = Account::select('id')->where('category', 'EXPENSE')->get()->toArray();
            $expenseParticulars = Particular::whereIn('debitaccount_id', $expenseAccounts)->get();

            return View::make('pdf.financials.reports', compact('incomeParticulars', 'expenseParticulars'));
        });


        Route::post('cashbook', function () {
            $organization = Organization::find(1);
            $from = Input::get('from');
            $to = Input::get('to');

            $creditbf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))->where('created_at', '<', date('Y-m-d H:i:s', strtotime($from)))->where('type', 'credit')->sum('amount');
            $debitbf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))->where('created_at', '<', date('Y-m-d H:i:s', strtotime($from)))->where('type', 'debit')->sum('amount');
            $balbf = $creditbf - $debitbf;
            $creditaf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))->where('created_at', '<', date('Y-m-d H:i:s', strtotime($to)))->where('type', 'credit')->sum('amount');
            $debitaf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))->where('created_at', '<', date('Y-m-d H:i:s', strtotime($to)))->where('type', 'debit')->sum('amount');
            $balcf = $creditaf - $debitaf;
            /*
            $credits = DB::table('journals')->join('particulars', 'journals.particulars_id', '=', 'particulars.id')->whereIn('account_id', array(1,9))->whereBetween('created_at' ,array($from, $to))->where('type', 'credit')->select(DB::raw('journals.date as date, journals.trans_no as transno, particulars.name as particulars, journals.amount as amount'))->first();
            $debits = DB::table('journals')->whereIn('account_id', array(1,9))->whereBetween('created_at' ,array($from, $to))->where('type', 'debit')->get();
            */


            $account = Account::where('name', 'like', '%' . 'Bank Account' . '%')->where('category', '=', 'ASSET')->first();
            $dateop = date('Y', strtotime($to));
            $openingbalance = Tbopeningbalance::where('account_id', '=', $account->id)->where('opening_bal_for', '=', $dateop)->pluck('account_balance');


            $openingbal = Tbopeningbalance::getTrialBalancepreviousmonth($account, $openingbalance, $to);
            $entries = DB::table('journals')
                ->whereIn('account_id', array(1, 6, 44))
                ->whereBetween('date', array($from, $to))
                ->where('amount', '!=', '0')
                ->get();
            // return var_dump($entries);
            $dates = array();
            foreach ($entries as $entry) {
                if (!array_key_exists($entry->date, $dates))
                    $dates[$entry->date] = array();

                array_push($dates[$entry->date], $entry);
            }

            return Excel::create('CashBook', function ($excel) use ($dates, $openingbal) {

                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


                $excel->sheet('CashBook', function ($sheet) use ($dates, $openingbal) {

                    $sheet->setAllBorders('thin');

                    $sheet->setWidth(array(
                        'A' => 15,
                        'B' => 60,
                        'C' => 15,
                        'D' => 15,
                        'E' => 1,
                        'F' => 60,
                        'G' => 15,
                        'H' => 15
                    ));

                    $sheet->mergeCells('A1:H1');

                    $sheet->row(1, array("MOTO STAFF SACCO LTD"));

                    $sheet->cells('A1:H1', function ($cells) {
                        $cells->setAlignment('center');
                        // $cells->setBackground('#777777');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->mergeCells('A2:D2');
                    $sheet->mergeCells('F2:H2');

                    $sheet->cell('A2', function ($cell) {
                        $cell->setValue('Dr');
                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->cell('F2', function ($cell) {
                        $cell->setValue('Cr');
                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->row(3, array(
                        "Date", "Description", "Trans. Ref.", "Amount", "", "Description", "Trans. Ref.", "Amount"
                    ));

                    $sheet->cells('A3:H3', function ($cells) {
                        $cells->setFont(array(
                            'bold' => true
                        ));
                    });

                    if (ob_get_level() > 0) {
                        ob_end_clean();
                    }

                    $debit_row = 3;
                    $credit_row = 3;
                    $debit_total = 0;
                    $credit_total = 0;
                    foreach ($dates as $date => $entries) {
                        $max = max($debit_row, $credit_row);
                        $max++;

                        $debit_row = $max;
                        $credit_row = $max;

                        $sheet->cell('A' . $max, function ($cell) use ($date) {
                            $cell->setValue($date);
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });

                        foreach ($entries as $entry) {
                            $sheet->cell('E' . $debit_row, function ($cell) {

                                $cell->setBackground('#860086');
                            });
                            if ($entry->amount > 0) {

                                $narration = DB::table('narration')->where('trans_no', $entry->trans_no)->first();
                                if ($narration != null) {
                                    if ($narration->member_id == '0') {
                                        $narration = "Moto Sacco";
                                    } else {
                                        $narration = Member::find($narration->member_id)->name;
                                    }
                                } else {
                                    $narration = "";
                                }


                                if ($entry->type == "debit") {
                                    $ref = strtoupper($entry->bank_details);
                                    $debitamount = number_format($entry->amount, 2);
                                    $sheet->cell('B' . $debit_row, function ($cell) use ($entry, $narration) {
                                        $cell->setValue($entry->description . " " . $narration);
                                    });
                                    $sheet->cell('C' . $debit_row, function ($cell) use ($entry, $ref) {
                                        $cell->setValue($ref);
                                    });
                                    $sheet->cell('D' . $debit_row, function ($cell) use ($debitamount) {
                                        $cell->setAlignment('left');
                                        $cell->setValue($debitamount);
                                    });

                                    $debit_row++;
                                    $debit_total += $entry->amount;
                                } elseif ($entry->type == "credit") {
                                    $ref = strtoupper($entry->bank_details);
                                    $creditamount = number_format($entry->amount, 2);
                                    $sheet->cell('F' . $credit_row, function ($cell) use ($entry, $narration) {
                                        $cell->setValue($entry->description . " " . $narration);
                                    });
                                    $sheet->cell('G' . $credit_row, function ($cell) use ($entry, $ref) {
                                        $cell->setValue($ref);
                                    });
                                    $sheet->cell('H' . $credit_row, function ($cell) use ($creditamount) {
                                        $cell->setAlignment('left');
                                        $cell->setValue($creditamount);
                                    });

                                    $credit_row++;
                                    $credit_total += $entry->amount;
                                }
                            }
                        }
                    }

                    $max = max($debit_row, $credit_row);
                    $max++;

                    $balance = $debit_total - $credit_total;
                    $total_balance = $balance + $openingbal;
                    $pos_balance = $total_balance * (-1);
                    $credit_total = number_format($credit_total, 2);
                    $debit_total = number_format($debit_total, 2);
                    $balance = number_format($balance, 2);
                    $pos_balance = number_format($pos_balance, 2);
                    $sheet->cell('A' . $max, function ($cell) {
                        $cell->setValue("TOTAL");
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $sheet->cell('D' . $max, function ($cell) use ($debit_total) {
                        $cell->setValue($debit_total);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $sheet->cell('H' . $max, function ($cell) use ($credit_total) {
                        $cell->setValue($credit_total);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $max = $max + 3;
                    $sheet->cell('B' . $max, function ($cell) use ($debit_total) {
                        $cell->setValue("Opening Balance");
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $sheet->cell('D' . $max, function ($cell) use ($openingbal) {
                        $cell->setValue($openingbal);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });


                    $max = $max + 4;


                    if ($total_balance < 0) {

                        $sheet->cell('F' . $max, function ($cell) {
                            $cell->setAlignment('left');
                            $cell->setValue("BALANCE B/D");
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });

                        $sheet->cell('H' . $max, function ($cell) use ($pos_balance) {
                            $cell->setValue($pos_balance);
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });
                    } else {

                        $sheet->cell('B' . $max, function ($cell) {
                            $cell->setAlignment('left');
                            $cell->setValue("BALANCE B/D");
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });

                        $sheet->cell('D' . $max, function ($cell) use ($total_balance) {
                            $cell->setValue($total_balance);
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });
                    }

                });


            })->export('xlsx');

            /*$debits=array();
            $credits=array();
            $debits_sum=0;
            $credts_sum=0;
            foreach ($entries as $entry) {
              if ($entry->amount > 0){
              if($entry->particulars_id == null || $entry->particulars_id == '0'){
                $particular=" ";
              }else{
                $particular = Particular::find($entry->particulars_id)->name;
              }

              if(!array_key_exists($entry->date, $credits)){
                 $credits[$entry->date]=array();
              }

              if(!array_key_exists($entry->date, $debits)){
                 $debits[$entry->date]=array();
              }

              if($entry->type=="credit"){
                if(!array_key_exists($particular, $credits[$entry->date])){
                  $credits[$entry->date][$particular]=array();
                }

                 $credits[$entry->date][$particular][$entry->trans_no] = $entry->amount;

                $credts_sum+=$entry->amount;
              } elseif ($entry->type=="debit") {
                if(!array_key_exists($particular, $debits[$entry->date])){
                  $debits[$entry->date][$particular]=array();
                }

                 $debits[$entry->date][$particular][$entry->trans_no] = $entry->amount;
                $debits_sum+=$entry->amount;
              }
            }
            }*/

//return var_dump($debits);
//return $credits;
// return View::make('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization','debits_sum','credts_sum','credits','debits','balcf','balbf'));

            //return $pdf = PDFF::html('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization', 'debits_sum', 'credts_sum', 'credits', 'debits', 'balcf', 'balbf'))->setOrientation('landscape');
            /*$pdf->setOptions(array(
                'orientation' => 'landscape'
            ));*/
            //return $pdf->stream('Cash Book.pdf');
            /*$pdf = PDF::loadView('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization','debits_sum','credts_sum','credits','debits','balcf','balbf'))->setPaper('a4')
                ->setOrientation('landscape');
            return $pdf->stream('Cash Book.pdf');*/
        });


        Route::post('cashbookstatement', function () {
            $organization = Organization::find(1);
            $from = Input::get('from');
            $to = Input::get('to');

            /**$entries = DB::table('journals')
             * ->whereIn('account_id', array(1, 6, 44))
             * ->whereBetween('date', array($from, $to))
             * ->where('amount', '!=', '0')
             * ->get();**/
            $entries = DB::table('account_transactions')
                ->whereIn('account_debited', array(1, 6, 44))
                ->whereBetween('transaction_date', array($from, $to))
                ->where('transaction_amount', '!=', '0')
                ->get();
            // return var_dump($entries);
            $dates = array();
            foreach ($entries as $entry) {
                if (!array_key_exists($entry->transaction_date, $dates))
                    $dates[$entry->transaction_date] = array();

                array_push($dates[$entry->transaction_date], $entry);
            }

            return Excel::create('CashBook', function ($excel) use ($dates) {

                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
                require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


                $excel->sheet('CashBook', function ($sheet) use ($dates) {

                    $sheet->setAllBorders('thin');

                    $sheet->setWidth(array(
                        'A' => 15,
                        'B' => 60,
                        'C' => 15,
                        'D' => 15,
                        'E' => 1,
                        'F' => 60,
                        'G' => 15,
                        'H' => 15
                    ));

                    $sheet->mergeCells('A1:H1');

                    $sheet->row(1, array("MOTO STAFF SACCO LTD"));

                    $sheet->cells('A1:H1', function ($cells) {
                        $cells->setAlignment('center');
                        // $cells->setBackground('#777777');
                        $cells->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->mergeCells('A2:D2');
                    $sheet->mergeCells('F2:H2');

                    $sheet->cell('A2', function ($cell) {
                        $cell->setValue('Dr');
                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->cell('F2', function ($cell) {
                        $cell->setValue('Cr');
                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'bold' => true
                        ));
                    });

                    $sheet->row(3, array(
                        "Date", "Description", "Ref. No.", "Amount", "", "Description", "Ref. No.", "Amount"
                    ));

                    $sheet->cells('A3:H3', function ($cells) {
                        $cells->setFont(array(
                            'bold' => true
                        ));
                    });

                    if (ob_get_level() > 0) {
                        ob_end_clean();
                    }

                    $debit_row = 3;
                    $credit_row = 3;
                    $debit_total = 0;
                    $credit_total = 0;
                    foreach ($dates as $date => $entries) {
                        $max = max($debit_row, $credit_row);
                        $max++;

                        $debit_row = $max;
                        $credit_row = $max;

                        $sheet->cell('A' . $max, function ($cell) use ($date) {
                            $cell->setValue($date);
                            $cell->setFont(array(
                                'bold' => true
                            ));
                        });

                        foreach ($entries as $entry) {
                            if ($entry->transaction_amount > 0) {

                                $narration = DB::table('narration')->where('reference', $entry->Ref_no)->first();
                                if ($narration != null) {
                                    if ($narration->member_id == '0') {
                                        $narration = "Moto Sacco";
                                    } else {
                                        $narration = Member::find($narration->member_id)->name;
                                    }
                                } else {
                                    $narration = "";
                                }

                                if ($entry->type == "debit") {
                                    $sheet->cell('B' . $debit_row, function ($cell) use ($entry, $narration) {
                                        $cell->setValue($entry->description);
                                    });
                                    $sheet->cell('C' . $debit_row, function ($cell) use ($entry) {
                                        $cell->setValue($entry->Ref_no);
                                    });
                                    $sheet->cell('D' . $debit_row, function ($cell) use ($entry) {
                                        $cell->setValue($entry->transaction_amount);
                                    });

                                    $debit_row++;
                                    $debit_total += $entry->transaction_amount;
                                } elseif ($entry->type == "credit") {
                                    $sheet->cell('F' . $credit_row, function ($cell) use ($entry, $narration) {
                                        $cell->setValue($entry->description);
                                    });
                                    $sheet->cell('G' . $credit_row, function ($cell) use ($entry) {
                                        $cell->setValue($entry->Ref_no);
                                    });
                                    $sheet->cell('H' . $credit_row, function ($cell) use ($entry) {
                                        $cell->setValue($entry->transaction_amount);
                                    });

                                    $credit_row++;
                                    $credit_total += $entry->transaction_amount;
                                }
                            }
                        }
                    }

                    $max = max($debit_row, $credit_row);
                    $max++;

                    $sheet->cell('A' . $max, function ($cell) {
                        $cell->setValue("TOTAL");
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $sheet->cell('D' . $max, function ($cell) use ($debit_total) {
                        $cell->setValue($debit_total);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    $sheet->cell('H' . $max, function ($cell) use ($credit_total) {
                        $cell->setValue($credit_total);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });
                });

            })->export('xlsx');
        });

        Route::get('cashbookselectperiod1', function () {


            return View::make('pdf.cashbookselectperiod1');
        });


        Route::get('reports/listing', 'ReportsController@members');
        Route::post('reports/listing', 'ReportsController@membersListing');
        Route::get('reports/remittance', 'ReportsController@remittance');
        Route::get('reports/blank', 'ReportsController@template');
        Route::get('reports/loanlisting', 'ReportsController@allListing');

        Route::get('reports/loanproduct/{id}', 'ReportsController@loanproduct');
        Route::get('reports/interest/{id}', 'ReportsController@interest');
        Route::get('reports/totalinterests', 'ReportsController@totalInterest');
        Route::get('reports/loanrepayments', 'ReportsController@loanrepayments');
        Route::get('reports/creditreport/{id}/{loan}', 'ReportsController@creditappraisal');
        Route::get('reports/savinglisting', 'ReportsController@savinglisting');

        Route::get('reports/savingproduct/{id}', 'ReportsController@savingproduct');

        Route::get('reports/selecttransactionPeriod', 'ReportsController@selecttransactionPeriod');
        Route::post('reports/transaction', 'ReportsController@transaction');


        Route::post('reports/financials', 'ReportsController@financials');
        Route::get('reports/redirect', 'ReportsController@intercept');

        Route::post('report/monthlyrepayments', 'ReportsController@monthlyrepayments');
        Route::get('reports/loan_arrears', 'ReportsController@loan_arrears');
        Route::post('reports/loan_arrears_pdf', 'ReportsController@loan_arrears_pdf');

        Route::get('portal', function () {

            $members = Member::all();
            return View::make('css.members', compact('members'));
        });

        Route::get('saccoinvestments', function () {
            $investment = Investment::all();
            //Calculate the valuationda
            foreach ($investment as $invest) {
                Valuation::calculateValuation($invest->id);
            }
            $organization = Organization::find(1);
            return View::make('css.saccoinvestments', compact('investment', 'organization'));
        });

        Route::get('saccoinvestments/create', function () {
            $vendor = Vendor::all();
            $organization = Organization::find(1);
            $cats = Investmentcategory::all();
            return View::make('css.createinvestment', compact('vendor', 'organization', 'cats'));
        });

        Route::post('saccoinvestments/create', function () {
            $data = Input::all();
            $invest = new Investment;
            $invest->name = array_get($data, 'investment');
            $invest->vendor_id = array_get($data, 'vendor');
            $invest->valuation = array_get($data, 'valuation');
            $invest->growth_type = array_get($data, 'growth_type');
            $invest->growth_rate = array_get($data, 'growth_rate');
            $invest->description = array_get($data, 'desc');
            $invest->category_id = array_get($data, 'category');
            $invest->date = date('Y-m-d');
            $invest->save();
            $out = "The sacco Investment successfully created!!!";
            $investment = Investment::all();
            $organization = Organization::find(1);
            return View::make('css.saccoinvestments', compact('out', 'investment', 'organization'));
        });

        Route::get('saccoinvestments/edit/{id}', 'InvestmentController@update');
        Route::post('saccoinvestments/edit', 'InvestmentController@doupdate');
        Route::get('saccoinvestments/delete/{id}', 'InvestmentController@destroy');

        Route::get('portal/activate/{id}', 'MembersController@activateportal');
        Route::post('portal/deactivate', 'MembersController@deactivateportal');
        Route::get('portal/refund/{id}', 'MembersController@refundportal');
        Route::get('portal/leave/{id}', 'MembersController@leaveportal');

        /*
        * Vendor controllers
        */
        Route::resource('vendors', 'VendorsController');
        Route::get('vendors/create', 'VendorsController@create');
        Route::post('vendors/update/{id}', 'VendorsController@update');
        Route::get('vendors/edit/{id}', 'VendorsController@edit');
        Route::get('vendors/delete/{id}', 'VendorsController@destroy');
        Route::get('vendors/products/{id}', 'VendorsController@products');
        Route::get('vendors/orders/{id}', 'VendorsController@orders');
        /*
        * products controllers
        */
        Route::resource('products', 'ProductsController');
        Route::post('products/update/{id}', 'ProductsController@update');
        Route::get('products/edit/{id}', 'ProductsController@edit');
        Route::get('products/create', 'ProductsController@create');
        Route::get('products/delete/{id}', 'ProductsController@destroy');
        Route::get('products/orders/{id}', 'ProductsController@orders');
        Route::get('shop', 'ProductsController@shop');
        /*
        * orders controllers
        */
        Route::resource('orders', 'OrdersController');
        Route::post('orders/update/{id}', 'OrdersControler@update');
        Route::get('orders/edit/{id}', 'OrdersControler@edit');
        Route::get('orders/delete/{id}', 'OrdersControler@destroy');
        /*
        * Projects controllers
        */
        Route::resource('projects', 'ProjectsController');
        Route::get('projects/create', 'ProjectsController@create');
        Route::post('projects/update', 'ProjectsController@doupdate');
        Route::get('projects/update/{id}', 'ProjectsController@update');
        Route::get('projects/delete/{id}', 'ProjectsController@destroy');
        Route::get('projects/orders/show', 'ProjectsController@show');
        Route::get('projects/orders/create/{id}', 'ProjectsController@createOrder');
        Route::post('projects/orders/create', 'ProjectsController@docreateOrder');
        Route::get('projects/orders/update/{id}', 'ProjectsController@updateOrder');
        Route::post('projects/orders/update', 'ProjectsController@doupdateOrder');
        Route::get('projects/orders/delete/{id}', 'ProjectsController@diminishOrder');
        Route::get('projects/orders/approve/{id}', 'ProjectsController@approve');
        Route::post('projects/orders/approve', 'ProjectsController@doapprove');
        Route::get('projects/orders/reject/{id}', 'ProjectsController@reject');
        Route::post('projects/orders/reject', 'ProjectsController@doreject');
        /*
        * particular controller
        */
        Route::resource('particulars', 'ParticularController');


        Route::get('savings', function () {

            $mem = Confide::user()->username;


            $memb = DB::table('members')->where('membership_no', '=', $mem)->pluck('id');

            $member = Member::find($memb);


            return View::make('css.savingaccounts', compact('member'));
        });

///The post loanguarantors:: Just checking if the selected guarantor has existing loan that they are servicing
        Route::post('loanguarantors', function () {
            $mem_id = Input::get('member_id');
            $member = Member::where('id', $mem_id)->get();
            if (count($member) < 1) {
                return Redirect::back()->withNull('You have not selected any guarantor! Select one to continue...');
            }
            $loanaccount = Loanaccount::findOrFail(Input::get('loanaccount_id'));
            $check_member = Loanaccount::where('member_id', $mem_id)->get();
            if (count($check_member) > 1) {
                return Redirect::back()->withBang('The selected member already services a loan, hence cannot guarantee the loan.');
            } else {
                $guarantor = new Loanguarantor;
                $guarantor->member()->associate($member);
                $guarantor->loanaccount()->associate($loanaccount);
                $guarantor->save();
                return Redirect::to('memloans/' . $loanaccount->id);
            }
        });
        Route::resource('audits', 'AuditsController');

        Route::get('backups', function () {
            //$backups = Backup::getRestorationFiles('../app/storage/backup/');
            return View::make('backup');
        });

        Route::get('backups/create', function () {

            echo '<pre>';

            $instance = Backup::getBackupEngineInstance();

            print_r($instance);

            //Backup::setPath(public_path().'/backups/');

            //Backup::export();
            //$backups = Backup::getRestorationFiles('../app/storage/backup/');

            //return View::make('backup');

        });


        Route::get('memtransactions/{id}', 'MembersController@savingtransactions');

        Route::get('convert', function () {


// get the name of the organization from the database
//$org_id = Confide::user()->organization_id;

            $organization = Organization::findorfail(1);


            $string = $organization->name;

            echo "Organization: " . $string . "<br>";


            $organization = new Organization;


            $license_code = $organization->encode($string);

            echo "License Code: " . $license_code . "<br>";


            $name2 = $organization->decode($license_code, 7);

            echo "Decoded L code: " . $name2 . "<br>";


            $license_key = $organization->license_key_generator($license_code);

            echo "License Key: " . $license_key . "<br>";

            echo "__________________________________________________<br>";

            $name4 = $organization->license_key_validator($license_key, $license_code, $string);

            echo "Decoded L code: " . $name4 . "<br>";


        });


        Route::get('perms', function () {

            $perm = new Permission;

            $perm->name = 'edit_loan_product';
            $perm->display_name = 'edit loan products';
            $perm->category = 'Loanproduct';
            $perm->save();


            $perm = new Permission;

            $perm->name = 'view_loan_product';
            $perm->display_name = 'view loan products';
            $perm->category = 'Loanproduct';
            $perm->save();

            $perm = new Permission;

            $perm->name = 'delete_loan_product';
            $perm->display_name = 'delete loan products';
            $perm->category = 'Loanproduct';
            $perm->save();


            $perm = new Permission;

            $perm->name = 'create_loan_account';
            $perm->display_name = 'create loan account';
            $perm->category = 'Loanaccount';
            $perm->save();


            $perm = new Permission;

            $perm->name = 'view_loan_account';
            $perm->display_name = 'view loan account';
            $perm->category = 'Loanaccount';
            $perm->save();


            $perm = new Permission;

            $perm->name = 'approve_loan_account';
            $perm->display_name = 'approve loan';
            $perm->category = 'Loanaccount';
            $perm->save();


            $perm = new Permission;

            $perm->name = 'disburse_loan';
            $perm->display_name = 'disburse loan';
            $perm->category = 'Loanaccount';
            $perm->save();


        });


        Route::get('rproducts', function () {

            Product::getRemoteProducts();


        });


        Route::get('reports/deduction', function () {

            return View::make('deduction');

        });


        Route::post('deductions', function () {

            $date = Input::get('date');

            $members = Member::all();

            $loanproducts = Loanproduct::all();

            $savingproducts = Savingproduct::all();

            return View::make('dedreport', compact('members', 'loanproducts', 'savingproducts', 'date'));
        });

        Route::get('reports/projects', function () {
            $members = DB::table('members')
                ->join('project_orders', 'members.id', '=', 'project_orders.member_id')
                ->where('project_orders.is_approved', '=', 1)
                ->select('members.name as name', 'members.membership_no as membership_no',
                    'members.id as id')
                ->groupBy('members.name')
                ->get();
            return View::make('project', compact('members'));

        });

        Route::post('projects/statement', function () {
            $memid = Input::get('memberid');
            $member = Member::where('id', '=', $memid)->get()->first();
            $projects = Projectorder::where('member_id', '=', $memid)
                ->where('is_approved', '=', 1)
                ->get();
            $organization = Organization::findOrFail(1);
            $pdf = PDF::loadView('pdf.investments.statement', compact('organization', 'member', 'projects'))
                ->setPaper('a4')->setOrientation('potrait');
            return $pdf->stream('Member Project Statement.pdf');
        });

        Route::get('reports/allotment', function () {
            $members = DB::table('members')
                ->join('project_orders', 'members.id', '=', 'project_orders.member_id')
                ->where('project_orders.is_approved', '=', 1)
                ->select('members.name as name', 'members.membership_no as membership_no',
                    'members.id as id')
                ->groupBy('members.name')
                ->get();
            return View::make('allotment', compact('members'));

        });

        Route::post('allotments', function () {
            $memid = Input::get('memberid');
            $membername = Member::where('id', '=', $memid)->pluck('name');
            $prid = Input::get('loanaccount_id');
            $projs = Projectorder::where('id', '=', $prid)->get()->first();
            $units = $projs->units;
            $proj_id = $projs->project_id;
            $projectname = Project::where('id', '=', $proj_id)->pluck('name');
            $price = Project::where('id', '=', $proj_id)->pluck('unit_price');
            $totalamount = $price * $units;

            $organization = Organization::findOrFail(1);
            $pdf = PDF::loadView('pdf.investments.certificate', compact('organization', 'membername', 'totalamount', 'units', 'projectname'))
                ->setPaper('a5')->setOrientation('landscape');
            return $pdf->stream('Member Allotment Certificate.pdf');
        });
        Route::get('api/orders', function () {
            $id = Input::get('option');
            $projects = DB::table('project_orders')
                ->join('members', 'project_orders.member_id', '=', 'members.id')
                ->join('projects', 'project_orders.project_id', '=', 'projects.id')
                ->where('member_id', $id)
                ->select('project_orders.id as id', 'projects.name as name')
                ->lists('name', 'id');

            return $projects;
        });


        Route::post('import/savings', function () {

            if (Input::hasFile('savings')) {

                $destination = storage_path() . '/backup/';

                $filename = str_random(12);

                $ext = Input::file('savings')->getClientOriginalExtension();
                $file = $filename . '.' . $ext;

                Input::file('savings')->move($destination, $file);


                Excel::load(storage_path() . '/backup/' . $file, function ($reader) {

                    $results = $reader->get();

                    // Getting all results
                    foreach ($results as $result) {

                        $date = date('Y-m-d', strtotime($result->date));
                        $savingaccount = Member::getMemberAccount($result->id);

                        if (Savingtransaction::trasactionExists($date, $savingaccount) == false) {


                            $amount = $result->amount;
                            if ($amount >= 0) {
                                $type = 'credit';
                                $description = $result->description;
                            } else {
                                $type = 'debit';
                                $description = $result->description;
                                $amount = preg_replace('/[^0-9]+/', '', $amount);
                            }
                            $transacted_by = $result->member;


                            Savingtransaction::transact($date, $savingaccount, $amount, $type, $description, $transacted_by);


                        }


                    }

                });


                return Redirect::back()->with('notice', 'savings have been imported');

            } else {

                return Redirect::back()->with('error', 'You have not uploaded any file');

            }


        });

        Route::get('api/dropdown', function () {
            $id = Input::get('option');
            $bbranch = Bank::find($id)->bankbranch;
            return $bbranch->lists('bank_branch_name', 'id');
        });


        Route::post('import/loans', function () {


            if (Input::hasFile('loans')) {

                $destination = storage_path() . '/backup/';

                $filename = str_random(12);

                $ext = Input::file('loans')->getClientOriginalExtension();
                $file = $filename . '.' . $ext;

                Input::file('loans')->move($destination, $file);

                Excel::load(storage_path() . '/backup/' . $file, function ($reader) {

                    $results = $reader->get();

                    // Getting all results
                    foreach ($results as $result) {


                        $date = date('Y-m-d', strtotime($result->date));

                        $member_id = $result->id;
                        $loanproduct_id = $result->product;

                        $amount = $result->amount;

                        $member = Member::findorfail($member_id);

                        $loanproduct = Loanproduct::findorfail($loanproduct_id);

                        $loanaccount = new Loanaccount;
                        $loanaccount->member()->associate($member);
                        $loanaccount->loanproduct()->associate($loanproduct);
                        $loanaccount->application_date = $date;
                        $loanaccount->amount_applied = $amount;
                        $loanaccount->repayment_duration = $result->period;


                        $loanaccount->date_approved = $date;
                        $loanaccount->amount_approved = $amount;
                        $loanaccount->interest_rate = $result->rate;
                        $loanaccount->period = $result->period;
                        $loanaccount->is_approved = TRUE;
                        $loanaccount->is_new_application = FALSE;

                        $loanaccount->date_disbursed = $date;
                        $loanaccount->amount_disbursed = $amount;
                        $loanaccount->repayment_start_date = $date;
                        $loanaccount->account_number = Loanaccount::loanAccountNumber($loanaccount);
                        $loanaccount->is_disbursed = TRUE;


                        $loanaccount->save();

                        $loanamount = $amount + Loanaccount::getInterestAmount($loanaccount);
                        Loantransaction::disburseLoan($loanaccount, $loanamount, $date);

                    }

                });

            }


        });

        Route::get('mpesa', function () {

        });

        Route::get('api/label', function () {
            $id = Input::get('option');
            $currency = Currency::find(1);
            $loanproduct = Loanproduct::find($id);
            return $currency->shortname . '. ' . number_format($loanproduct->auto_loan_limit, 2);
        });


        /*
        * banks routes
        */

        Route::resource('banks', 'BanksController');
        Route::post('banks/update/{id}', 'BanksController@update');
        Route::get('banks/delete/{id}', 'BanksController@destroy');
        Route::get('banks/edit/{id}', 'BanksController@edit');
        Route::get('banksimport', function () {
            return View::make('banks.import');
        });

        /*
        * bank branch routes
        */
        Route::get('bankReconcile/reconciliation/{month}', 'BankAccountController@reconcileTransactions');

        Route::resource('bankbranches', 'BankBranchController');
        Route::post('bankbranches/update/{id}', 'BankBranchController@update');
        Route::get('bankbranches/delete/{id}', 'BankBranchController@destroy');
        Route::get('bankbranches/edit/{id}', 'BankBranchController@edit');
        Route::get('bankbranchesimport', function () {
            return View::make('bank_branch.import');
        });


        /* #################### IMPORT BANK BRANCHES ################################## */

        Route::post('import/bankBranches', function () {


            if (Input::hasFile('bbranches')) {

                $destination = public_path() . '/migrations/';

                $filename = str_random(12);

                $ext = Input::file('bbranches')->getClientOriginalExtension();
                $file = $filename . '.' . $ext;

                Input::file('bbranches')->move($destination, $file);
                Excel::selectSheetsByIndex(0)->load(public_path() . '/migrations/' . $file, function ($reader) {

                    $results = $reader->get();

                    foreach ($results as $result) {


                        $bbranch = new BBranch;

                        $bbranch->branch_code = $result->branch_code;

                        $bbranch->bank_branch_name = $result->branch_name;

                        $bbranch->bank_id = $result->bank_id;

                        $bbranch->organization_id = $result->organization_id;

                        $bbranch->save();

                    }

                });

            }


            return Redirect::route('bankbranches.index')->with('notice', 'bank branches have been succefully imported');


        });

        /* #################### IMPORT BANKS ################################## */

        Route::post('import/banks', function () {
            if (Input::hasFile('banks')) {
                $destination = public_path() . '/migrations/';
                $filename = str_random(12);
                $ext = Input::file('banks')->getClientOriginalExtension();
                $file = $filename . '.' . $ext;
                Input::file('banks')->move($destination, $file);
                Excel::selectSheetsByIndex(0)->load(public_path() . '/migrations/' . $file, function ($reader) {
                    $results = $reader->get();
                    foreach ($results as $result) {
                        $bank = new Bank;
                        $bank->bank_name = $result->bank_name;
                        $bank->bank_code = $result->bank_code;
                        $bank->organization_id = $result->organization_id;
                        $bank->save();
                    }
                });
            }
            return Redirect::route('banks.index')->with('notice', 'banks have been succefully imported');
        });


        Route::get('api/loans', function () {
            $id = Input::get('option');
            $loans = DB::table('loanaccounts')
                ->join('loanproducts', 'loanaccounts.loanproduct_id', '=', 'loanproducts.id')
                ->where('member_id', $id)
                ->select('loanaccounts.id as id', 'loanproducts.name as name')
                ->lists('name', 'id');

            return $loans;
        });

        Route::get('budget/projections', 'AccountsController@projections');
        Route::get('budget/projections/create', 'AccountsController@createProjection');
        Route::post('budget/projections/create', 'AccountsController@storeProjection');
        Route::get('budget/interests', 'AccountsController@proposalInterests');
        Route::get('budget/income', 'AccountsController@proposalOtherIncome');
        Route::get('budget/expenditure', 'AccountsController@proposalExpenditure');
        Route::get('budget/proposal/create', 'AccountsController@createProposal');
        Route::post('budget/proposal/create', 'AccountsController@storeProposal');
        Route::get('budget/expenses', 'AccountsController@showExpenses');
        Route::get('budget/incomes', 'AccountsController@showIncomes');
        Route::get('budget/incomes/create', 'AccountsController@createIncomes');
        Route::get('budget/expenses/create', 'AccountsController@createExpenses');
        Route::post('budget/expenses/create', 'AccountsController@storeExpenses');


        /**
         * Bank Account Routes &
         * Bank Reconciliation Routes
         */
        Route::resource('bankAccounts', 'BankAccountController');
        Route::get('bank/bank_deposit', function () {
            $bankAccs = BankAccount::all();
            $transactions = AccountTransaction::where("is_bank", 1);
            return View::make('banking.bankDeposit', compact('bankAccs', 'transactions'));
            //return View::make('banking.receipt');
        });
        Route::get('bankAccounts/reconcile/{id}', 'BankAccountController@showReconcile');
        Route::post('bankAccounts/uploadStatement', 'BankAccountController@uploadBankStatement');
        Route::post('bankAccounts/convertstatement', 'BankAccountController@convertStatement');

        Route::post('bankAccount/reconcile', 'BankAccountController@reconcileStatement');

        Route::get('bankAccount/reconcile/add/{id}/{id2}/{id3}/{id4}', 'BankAccountController@addStatementTransaction');
        Route::post('bankAccount/reconcile/add', 'BankAccountController@saveStatementTransaction');
        Route::get('bankReconciliation/receipt', function () {
            return View::make('banking.receipt');
        });
        Route::get('transactions/{month}', 'BankAccountController@transactions');
        Route::post('bankReconciliation/receipt', 'AccountsController@savereceipt');
        Route::post('bankReconciliation/payment', 'AccountsController@addBankTransaction');
        Route::get('bankReconciliation/report', 'ReportsController@displayRecOptions');
        Route::post('bankReconciliartion/generateReport', 'ReportsController@showRecReport');
        Route::post('BankReconciliation/edit', 'BankAccountController@edittrans');
        Route::get('bankReconciliation/reconcilestatement/{bid}/{aid}', 'BankAccountController@reconcile');
        Route::get('bankReconciliation/showReconciling/{bid}/{aid}', 'BankAccountController@shownonReconciled');
        Route::get('deletezeros', function () {
            $journals = Journal::where('amount', 0)->get();
            foreach ($journals as $journal) {
                // code...
                $journal->delete();
            }
        });
        Route::get('updatedisbursed', function () {
            $loans = Loanaccount::where('is_disbursed', true)->get();
            foreach ($loans as $loan) {
                $date = $loan->date_disbursed;
                $loan->repayment_start_date = date('Y-m-d', strtotime('+1 month', strtotime($date)));
                $loan->update();
            }

            return "Loans update successful";
        });
        Route::get('test', function () {
            $loan = Loanaccount::find(226);
            return Loantransaction::getLoanBalanceAt($loan, date('Y-m-d'));
        });


        /* PETTY CASH ROUTES */
        Route::get('petty_cash/delete/{id}', 'PettyCashController@deletePetty');
        Route::resource('petty_cash', 'PettyCashController');
        Route::post('petty_cash/addMoney', 'PettyCashController@addMoney');
        Route::post('petty_cash/addContribution', 'PettyCashController@addContribution');
        Route::post('petty_cash/newTransaction', 'PettyCashController@newTransaction');
        Route::get('newpettycash', function () {
            // Session::put('newTransaction', [
            // 	'transactTo'=>Input::get('transact_to'),
            // 	'trDate'=>Input::get('tr_date'),
            // 	'description'=>Input::get('description'),
            // 	'expense_ac'=>Input::get('expense_ac'),
            // 	'credit_ac'=>Input::get('credit_ac')
            // ]);
            //
            // $newTr = Session::get('newTransaction');
            //return Input::get();

            $account = Account::where('name', 'like', '%' . 'Petty Cash' . '%')->first();
            if (Input::get('item') != NULL) {

                Session::push('trItems', array(
                    'item_name' => Particular::find(Input::get('item'))->name,
                    'description' => Input::get('desc'),
                    'quantity' => Input::get('qty'),
                    'date' => Input::get('date'),
                    'unit_price' => Input::get('unit_price'),
                    'particulars_id' => Input::get('item'),
                    'receipt' => Input::get('receipt'),
                    'credit_ac' => $account->id,
                    'totalA' => Input::get('qty') * Input::get('unit_price')
                ));
            }

            $trItems = Session::get('trItems');

            $particulars = Particular::whereNotNull('debitaccount_id')->whereNotNull('creditaccount_id')->get();
            foreach ($particulars as $key => $particular) {
                if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32' || $particular->debitAccount->category != 'EXPENSE') {
                    unset($particulars[$key]);
                }
            }


            return View::make('petty_cash.transactionItems', compact('particulars', 'trItems'));
        });
        Route::post('petty_cash/commitTransaction', 'PettyCashController@commitTransaction');
        Route::get('petty_cash/transaction/{id}', 'PettyCashController@receiptTransactions');

// Edit and delete petty cash items
        Route::get('petty_cash/newTransaction/remove/{count}', 'PettyCashController@removeTransactionItem');

//    });


});
